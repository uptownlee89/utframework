//
//  UTSideMenuViewController.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTSideMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UTUtils.h"
#import "UTDefine.h"
#import "UTUILibrary.h"


NSString *const UTSideMenuViewUnderRightWillAppear    = @"UTSideMenuViewUnderRightWillAppear";
NSString *const UTSideMenuViewUnderLeftWillAppear     = @"UTSideMenuViewUnderLeftWillAppear";
NSString *const UTSideMenuViewUnderLeftWillDisappear  = @"UTSideMenuViewUnderLeftWillDisappear";
NSString *const UTSideMenuViewUnderRightWillDisappear = @"UTSideMenuViewUnderRightWillDisappear";
NSString *const UTSideMenuViewTopDidAnchorLeft        = @"UTSideMenuViewTopDidAnchorLeft";
NSString *const UTSideMenuViewTopDidAnchorRight       = @"UTSideMenuViewTopDidAnchorRight";
NSString *const UTSideMenuViewTopWillReset            = @"UTSideMenuViewTopWillReset";
NSString *const UTSideMenuViewTopDidReset             = @"UTSideMenuViewTopDidReset";

@interface UTSideMenuViewController()

@property (nonatomic, strong) UIView *topViewSnapshot;
@property (nonatomic, assign) CGFloat initialTouchPositionX;
@property (nonatomic, assign) CGFloat initialHoizontalCenter;
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic, strong) UITapGestureRecognizer *resetTapGesture;
@property (nonatomic, strong) UIPanGestureRecognizer *topViewSnapshotPanGesture;
@property (nonatomic, assign) BOOL underLeftShowing;
@property (nonatomic, assign) BOOL underRightShowing;
@property (nonatomic, assign) BOOL topViewIsOffScreen;
@property (nonatomic, assign) BOOL isTopRettseing;

- (UIView *)topView;
- (UIView *)underLeftView;
- (UIView *)underRightView;
- (void)adjustLayout;
- (void)updateTopViewHorizontalCenterWithRecognizer:(UIPanGestureRecognizer *)recognizer;
- (void)updateTopViewHorizontalCenter:(CGFloat)newHorizontalCenter;
- (void)topViewHorizontalCenterWillChange:(CGFloat)newHorizontalCenter;
- (void)topViewHorizontalCenterDidChange:(CGFloat)newHorizontalCenter;
- (void)addTopViewSnapshot;
- (void)removeTopViewSnapshot;
- (CGFloat)anchorRightTopViewCenter;
- (CGFloat)anchorLeftTopViewCenter;
- (CGFloat)resettedCenter;
- (void)underLeftWillAppear;
- (void)underRightWillAppear;
- (void)topDidReset;
- (BOOL)topViewHasFocus;
- (void)updateUnderLeftLayout;
- (void)updateUnderRightLayout;

@end


@implementation UTSideMenuViewController

// public properties
@synthesize underLeftViewController  = _underLeftViewController;
@synthesize underRightViewController = _underRightViewController;
@synthesize topViewController        = _topViewController;
@synthesize anchorLeftPeekAmount;
@synthesize anchorRightPeekAmount;
@synthesize anchorLeftRevealAmount;
@synthesize anchorRightRevealAmount;
@synthesize underRightWidthLayout = _underRightWidthLayout;
@synthesize underLeftWidthLayout  = _underLeftWidthLayout;
@synthesize shouldAllowPanningPastAnchor;
@synthesize shouldAllowUserInteractionsWhenAnchored;
@synthesize shouldAddPanGestureRecognizerToTopViewSnapshot;
@synthesize resetStrategy = _resetStrategy;

// category properties
@synthesize topViewSnapshot;
@synthesize initialTouchPositionX;
@synthesize initialHoizontalCenter;
@synthesize panGesture = _panGesture;
@synthesize resetTapGesture;
@synthesize underLeftShowing   = _underLeftShowing;
@synthesize underRightShowing  = _underRightShowing;
@synthesize topViewIsOffScreen = _topViewIsOffScreen;
@synthesize topViewSnapshotPanGesture = _topViewSnapshotPanGesture;

- (void)setTopViewController:(UIViewController *)theTopViewController
{
    CGRect topViewFrame = _topViewController ? _topViewController.view.frame : self.view.bounds;
    [self removeTopViewSnapshot];
    [_topViewController.view removeFromSuperview];
    [_topViewController willMoveToParentViewController:nil];
    [_topViewController removeFromParentViewController];
    
    _topViewController = theTopViewController;
    
    [self addChildViewController:self.topViewController];
    [self.topViewController didMoveToParentViewController:self];
    
    [_topViewController.view setAutoresizingMask:UT_AUTORESIZINGFULL];
    [_topViewController.view setFrame:topViewFrame];
    _topViewController.view.layer.shadowOffset = CGSizeZero;
    _topViewController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
    
    CALayer *layer = _topViewController.view.layer;
    [layer setShadowColor:[UIColor blackColor].CGColor];
    [layer setShadowRadius:10.0];
    [layer setShadowOpacity:0.3];
    [layer setShadowOffset:CGSizeMake(0, 0)];
    [self.view addSubview:_topViewController.view];
}

- (void)setUnderLeftViewController:(UIViewController *)theUnderLeftViewController
{
    [_underLeftViewController.view removeFromSuperview];
    [_underLeftViewController willMoveToParentViewController:nil];
    [_underLeftViewController removeFromParentViewController];
    
    _underLeftViewController = theUnderLeftViewController;
    
    if (_underLeftViewController) {
        [self addChildViewController:self.underLeftViewController];
        [self.underLeftViewController didMoveToParentViewController:self];
        
        [self updateUnderLeftLayout];
    }
}

- (void)setUnderRightViewController:(UIViewController *)theUnderRightViewController
{
    [_underRightViewController.view removeFromSuperview];
    [_underRightViewController willMoveToParentViewController:nil];
    [_underRightViewController removeFromParentViewController];
    
    _underRightViewController = theUnderRightViewController;
    
    if (_underRightViewController) {
        [self addChildViewController:self.underRightViewController];
        [self.underRightViewController didMoveToParentViewController:self];
        
        [self updateUnderRightLayout];
    }
}

- (void)setUnderLeftWidthLayout:(UTViewWidthLayout)underLeftWidthLayout
{
    if (underLeftWidthLayout == UTVariableRevealWidth && self.anchorRightPeekAmount <= 0) {
        [NSException raise:@"Invalid Width Layout" format:@"anchorRightPeekAmount must be set"];
    } else if (underLeftWidthLayout == UTFixedRevealWidth && self.anchorRightRevealAmount <= 0) {
        [NSException raise:@"Invalid Width Layout" format:@"anchorRightRevealAmount must be set"];
    }
    
    _underLeftWidthLayout = underLeftWidthLayout;
}

- (void)setUnderRightWidthLayout:(UTViewWidthLayout)underRightWidthLayout
{
    if (underRightWidthLayout == UTVariableRevealWidth && self.anchorLeftPeekAmount <= 0) {
        [NSException raise:@"Invalid Width Layout" format:@"anchorLeftPeekAmount must be set"];
    } else if (underRightWidthLayout == UTFixedRevealWidth && self.anchorLeftRevealAmount <= 0) {
        [NSException raise:@"Invalid Width Layout" format:@"anchorLeftRevealAmount must be set"];
    }
    
    _underRightWidthLayout = underRightWidthLayout;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.useRight = YES;
    self.useLeft = YES;
    self.shouldAllowPanningPastAnchor = YES;
    self.shouldAllowUserInteractionsWhenAnchored = NO;
    self.shouldAddPanGestureRecognizerToTopViewSnapshot = NO;
    self.resetTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetTopView)];
    _panGesture          = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(updateTopViewHorizontalCenterWithRecognizer:)];
    self.resetTapGesture.enabled = NO;
    self.resetStrategy = UTTapping | UTPanning;
    
    self.topViewSnapshot = [[UIView alloc] initWithFrame:self.topView.bounds];
    [self.topViewSnapshot setAutoresizingMask:UT_AUTORESIZINGFULL];
    [self.topViewSnapshot addGestureRecognizer:self.resetTapGesture];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.topView.layer.shadowOffset = CGSizeZero;
    self.topView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
    [self adjustLayout];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    self.topView.layer.shadowPath = nil;
    self.topView.layer.shouldRasterize = YES;
    
    if(![self topViewHasFocus]){
        [self removeTopViewSnapshot];
    }
    
    [self adjustLayout];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    self.topView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
    self.topView.layer.shouldRasterize = NO;
    
    if(![self topViewHasFocus]){
        [self addTopViewSnapshot];
    }
}

- (void)setResetStrategy:(UTResetStrategy)theResetStrategy
{
    _resetStrategy = theResetStrategy;
    if (_resetStrategy & UTTapping) {
        self.resetTapGesture.enabled = YES;
    } else {
        self.resetTapGesture.enabled = NO;
    }
}

- (void)adjustLayout
{
    self.topViewSnapshot.frame = self.topView.bounds;
    
    if ([self underRightShowing] && ![self topViewIsOffScreen]) {
        [self updateUnderRightLayout];
        [self updateTopViewHorizontalCenter:self.anchorLeftTopViewCenter];
    } else if ([self underRightShowing] && [self topViewIsOffScreen]) {
        [self updateUnderRightLayout];
        [self updateTopViewHorizontalCenter:-self.resettedCenter];
    } else if ([self underLeftShowing] && ![self topViewIsOffScreen]) {
        [self updateUnderLeftLayout];
        [self updateTopViewHorizontalCenter:self.anchorRightTopViewCenter];
    } else if ([self underLeftShowing] && [self topViewIsOffScreen]) {
        [self updateUnderLeftLayout];
        [self updateTopViewHorizontalCenter:self.view.bounds.size.width + self.resettedCenter];
    }
}

- (void)updateTopViewHorizontalCenterWithRecognizer:(UIPanGestureRecognizer *)recognizer
{
    CGPoint currentTouchPoint     = [recognizer locationInView:self.view];
    CGFloat currentTouchPositionX = currentTouchPoint.x;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.initialTouchPositionX = currentTouchPositionX;
        self.initialHoizontalCenter = self.topView.center.x;
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat panAmount = self.initialTouchPositionX - currentTouchPositionX;
        BOOL isPanAmountPositive = (panAmount > 0);
        panAmount = isPanAmountPositive ? (panAmount - 50) : (panAmount + 50);
        if(isPanAmountPositive != (panAmount > 0)) panAmount = 0;
        
        CGFloat newCenterPosition = self.initialHoizontalCenter - panAmount;
        BOOL topGoRight = newCenterPosition > self.resettedCenter;
        if(topGoRight && !self.useLeft){
            return;
        }
        else if(!topGoRight && !self.useRight){
            return;
        }
        if(topGoRight){
            if(_underLeftWidthLayout == UTFixedRevealWidth && self.anchorRightRevealAmount > 0 && newCenterPosition > self.resettedCenter+ self.anchorRightRevealAmount){
                newCenterPosition = self.resettedCenter+ self.anchorRightRevealAmount;
            }
            if(_underLeftWidthLayout == UTVariableRevealWidth && self.anchorRightPeekAmount > 0 && newCenterPosition > self.resettedCenter * 2 -self.anchorRightPeekAmount){
                newCenterPosition = self.resettedCenter * 2 -self.anchorRightPeekAmount;
            }
        }
        else{
        
            if(_underRightWidthLayout == UTFixedRevealWidth && self.anchorLeftRevealAmount > 0 && newCenterPosition < self.resettedCenter - self.anchorLeftRevealAmount){
                newCenterPosition = self.resettedCenter - self.anchorLeftRevealAmount;
            }
            if(_underRightWidthLayout == UTVariableRevealWidth && self.anchorLeftPeekAmount > 0 && newCenterPosition > self.resettedCenter * 2 -self.anchorLeftPeekAmount){
                newCenterPosition = self.resettedCenter * 2 -self.anchorLeftPeekAmount;
            }

        }
        //if(fabs(panAmount) < 50) panAmount = 0;
        if ((!topGoRight && (self.anchorLeftTopViewCenter == NSNotFound || self.underRightViewController == nil)) ||
            (topGoRight && (self.anchorRightTopViewCenter == NSNotFound || self.underLeftViewController == nil))
            ) {
            newCenterPosition = self.resettedCenter;
        }
        
        BOOL newCenterPositionIsOutsideAnchor = newCenterPosition < self.anchorLeftTopViewCenter || self.anchorRightTopViewCenter < newCenterPosition;
        
        if ((newCenterPositionIsOutsideAnchor && self.shouldAllowPanningPastAnchor) || !newCenterPositionIsOutsideAnchor) {
            [self topViewHorizontalCenterWillChange:newCenterPosition];
            [self updateTopViewHorizontalCenter:newCenterPosition];
            [self topViewHorizontalCenterDidChange:newCenterPosition];
        }
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        CGPoint currentVelocityPoint = [recognizer velocityInView:self.view];
        CGFloat currentVelocityX     = currentVelocityPoint.x;
        
        if ([self underLeftShowing] && currentVelocityX > 100) {
            [self anchorTopViewTo:UTRight];
        } else if ([self underRightShowing] && currentVelocityX < 100) {
            [self anchorTopViewTo:UTLeft];
        } else {
            [self resetTopView];
        }
    }
}

- (UIPanGestureRecognizer *)panGesture
{
    return _panGesture;
}

- (void)anchorTopViewTo:(UTSide)side
{
    [self anchorTopViewTo:side animations:nil onComplete:nil];
}

- (void)anchorTopViewTo:(UTSide)side animations:(void (^)())animations onComplete:(void (^)())complete
{
    CGFloat newCenter = self.topView.center.x;
    
    if (side == UTLeft) {
        newCenter = self.anchorLeftTopViewCenter;
    } else if (side == UTRight) {
        newCenter = self.anchorRightTopViewCenter;
    }
    
    [self topViewHorizontalCenterWillChange:newCenter];
    
    [UIView animateWithDuration:0.25f animations:^{
        if (animations) {
            animations();
        }
        [self updateTopViewHorizontalCenter:newCenter];
    } completion:^(BOOL finished){
        if (_resetStrategy & UTPanning) {
            self.panGesture.enabled = YES;
        } else {
            self.panGesture.enabled = NO;
        }
        if (complete) {
            complete();
        }
        _topViewIsOffScreen = NO;
        [self addTopViewSnapshot];
        if(side == UTRight && !self.underLeftView.superview){
            [self.view insertSubview:self.underLeftView belowSubview:self.topView];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *key = (side == UTLeft) ? UTSideMenuViewTopDidAnchorLeft : UTSideMenuViewTopDidAnchorRight;
            [[NSNotificationCenter defaultCenter] postNotificationName:key object:self userInfo:nil];
        });
    }];
}

- (void)anchorTopViewOffScreenTo:(UTSide)side
{
    [self anchorTopViewOffScreenTo:side animations:nil onComplete:nil];
}

- (void)anchorTopViewOffScreenTo:(UTSide)side animations:(void(^)())animations onComplete:(void(^)())complete
{
    if(self.isTopRettseing){
//        complete();
        return;
    }
    CGFloat newCenter = self.topView.center.x;
    
    if (side == UTLeft) {
        newCenter = -self.resettedCenter;
    } else if (side == UTRight) {
        newCenter = self.view.bounds.size.width + self.resettedCenter;
    }
    
    [self topViewHorizontalCenterWillChange:newCenter];
    
    [UIView animateWithDuration:0.25f animations:^{
        if (animations) {
            animations();
        }
        [self updateTopViewHorizontalCenter:newCenter];
    } completion:^(BOOL finished){
        if (complete) {
            complete();
        }
        _topViewIsOffScreen = YES;
        [self addTopViewSnapshot];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *key = (side == UTLeft) ? UTSideMenuViewTopDidAnchorLeft : UTSideMenuViewTopDidAnchorRight;
            [[NSNotificationCenter defaultCenter] postNotificationName:key object:self userInfo:nil];
        });
    }];
}

- (void)resetTopView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewTopWillReset object:self userInfo:nil];
    });
    self.isTopRettseing = YES;
    [self resetTopViewWithAnimations:nil onComplete:nil];
}

- (void)resetTopViewWithAnimations:(void(^)())animations onComplete:(void(^)())complete
{
    [self topViewHorizontalCenterWillChange:self.resettedCenter];
    
    [UIView animateWithDuration:0.25f animations:^{
        if (animations) {
            animations();
        }
        [self updateTopViewHorizontalCenter:self.resettedCenter];
    } completion:^(BOOL finished) {
        if (complete) {
            complete();
        }
        [self topViewHorizontalCenterDidChange:self.resettedCenter];
    }];
}


- (UIView *)topView
{
    return self.topViewController.view;
}

- (UIView *)underLeftView
{
    return self.underLeftViewController.view;
}

- (UIView *)underRightView
{
    return self.underRightViewController.view;
}

- (void)updateTopViewHorizontalCenter:(CGFloat)newHorizontalCenter
{
    CGPoint center = self.topView.center;
    center.x = newHorizontalCenter;
    self.topView.layer.position = center;
}

- (void)topViewHorizontalCenterWillChange:(CGFloat)newHorizontalCenter
{
    CGPoint center = self.topView.center;
    
	if (center.x >= self.resettedCenter && newHorizontalCenter == self.resettedCenter) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewUnderLeftWillDisappear object:self userInfo:nil];
		});
	}
	
	if (center.x <= self.resettedCenter && newHorizontalCenter == self.resettedCenter) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewUnderRightWillDisappear object:self userInfo:nil];
		});
	}
	
    if (center.x <= self.resettedCenter && newHorizontalCenter > self.resettedCenter) {
        [self underLeftWillAppear];
    } else if (center.x >= self.resettedCenter && newHorizontalCenter < self.resettedCenter) {
        [self underRightWillAppear];
    }
}

- (void)topViewHorizontalCenterDidChange:(CGFloat)newHorizontalCenter
{
    if (newHorizontalCenter == self.resettedCenter) {
        [self topDidReset];
    }
}

- (void)addTopViewSnapshot
{
    if (!self.topViewSnapshot.superview && !self.shouldAllowUserInteractionsWhenAnchored) {
        topViewSnapshot.layer.contents = (id)[UTUILibrary imageWithView:self.topView].CGImage;
        
        if (self.shouldAddPanGestureRecognizerToTopViewSnapshot && (_resetStrategy & UTPanning)) {
            if (!_topViewSnapshotPanGesture) {
                _topViewSnapshotPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(updateTopViewHorizontalCenterWithRecognizer:)];
            }
            [topViewSnapshot addGestureRecognizer:_topViewSnapshotPanGesture];
        }
        [self.topView addSubview:self.topViewSnapshot];
    }
}

- (void)removeTopViewSnapshot
{
    if (self.topViewSnapshot.superview) {
        [self.topViewSnapshot removeFromSuperview];
    }
}

- (CGFloat)anchorRightTopViewCenter
{
    if (self.anchorRightPeekAmount) {
        return self.view.bounds.size.width + self.resettedCenter - self.anchorRightPeekAmount;
    } else if (self.anchorRightRevealAmount) {
        return self.resettedCenter + self.anchorRightRevealAmount;
    } else {
        return NSNotFound;
    }
}

- (CGFloat)anchorLeftTopViewCenter
{
    if (self.anchorLeftPeekAmount) {
        return -self.resettedCenter + self.anchorLeftPeekAmount;
    } else if (self.anchorLeftRevealAmount) {
        return -self.resettedCenter + (self.view.bounds.size.width - self.anchorLeftRevealAmount);
    } else {
        return NSNotFound;
    }
}

- (CGFloat)resettedCenter
{
    return (self.view.bounds.size.width / 2);
}

- (void)underLeftWillAppear
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewUnderLeftWillAppear object:self userInfo:nil];
    });
    [self.underRightView removeFromSuperview];
    [self updateUnderLeftLayout];
    [self.view insertSubview:self.underLeftView belowSubview:self.topView];
    _underLeftShowing  = YES;
    _underRightShowing = NO;
}

- (void)underRightWillAppear
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewUnderRightWillAppear object:self userInfo:nil];
    });
    [self.underLeftView removeFromSuperview];
    [self updateUnderRightLayout];
    [self.view insertSubview:self.underRightView belowSubview:self.topView];
    _underLeftShowing  = NO;
    _underRightShowing = YES;
}

- (void)topDidReset
{
    self.isTopRettseing = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:UTSideMenuViewTopDidReset object:self userInfo:nil];
    });
    [self.topView removeGestureRecognizer:self.resetTapGesture];
    [self removeTopViewSnapshot];
    self.panGesture.enabled = YES;
    [self.underRightView removeFromSuperview];
    [self.underLeftView removeFromSuperview];
    _underLeftShowing   = NO;
    _underRightShowing  = NO;
    _topViewIsOffScreen = NO;
}

- (BOOL)topViewHasFocus
{
    return !_underLeftShowing && !_underRightShowing && !_topViewIsOffScreen;
}

- (void)updateUnderLeftLayout
{
    if (self.underLeftWidthLayout == UTFullWidth) {
        [self.underLeftView setAutoresizingMask:UT_AUTORESIZINGFULL];
        [self.underLeftView setFrame:self.view.bounds];
    } else if (self.underLeftWidthLayout == UTVariableRevealWidth && !self.topViewIsOffScreen) {
        CGRect frame = self.view.bounds;
        
        frame.size.width = frame.size.width - self.anchorRightPeekAmount;
        self.underLeftView.frame = frame;
    } else if (self.underLeftWidthLayout == UTFixedRevealWidth) {
        CGRect frame = self.view.bounds;
        
        frame.size.width = self.anchorRightRevealAmount;
        self.underLeftView.frame = frame;
    } else {
        [NSException raise:@"Invalid Width Layout" format:@"underLeftWidthLayout must be a valid UTViewWidthLayout"];
    }
}

- (void)updateUnderRightLayout
{
    if (self.underRightWidthLayout == UTFullWidth) {
        [self.underRightViewController.view setAutoresizingMask:UT_AUTORESIZINGFULL];
        self.underRightView.frame = self.view.bounds;
    } else if (self.underRightWidthLayout == UTVariableRevealWidth) {
        CGRect frame = self.view.bounds;
        
        CGFloat newLeftEdge;
        CGFloat newWidth = frame.size.width;
        
        if (self.topViewIsOffScreen) {
            newLeftEdge = 0;
        } else {
            newLeftEdge = self.anchorLeftPeekAmount;
            newWidth   -= self.anchorLeftPeekAmount;
        }
        
        frame.origin.x   = newLeftEdge;
        frame.size.width = newWidth;
        
        self.underRightView.frame = frame;
    } else if (self.underRightWidthLayout == UTFixedRevealWidth) {
        CGRect frame = self.view.bounds;
        
        CGFloat newLeftEdge = frame.size.width - self.anchorLeftRevealAmount;
        CGFloat newWidth = self.anchorLeftRevealAmount;
        
        frame.origin.x   = newLeftEdge;
        frame.size.width = newWidth;
        
        self.underRightView.frame = frame;
    } else {
        [NSException raise:@"Invalid Width Layout" format:@"underRightWidthLayout must be a valid UTViewWidthLayout"];
    }
}

- (UINavigationController *)navigationController{
    if([self.topViewController isKindOfClass:[UINavigationController class]])
        return (UINavigationController *)self.topViewController;
    return self.topViewController.navigationController;
}


-(NSUInteger)supportedInterfaceOrientations{
    return [self.topViewController supportedInterfaceOrientations];
}

- (BOOL)shouldAutorotate{
    return [self.topViewController shouldAutorotate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
    return [self.topViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}

@end
