//
//  UIImage+ImageCache.m
//  UTFramework
//
//  Created by Juyoung Lee on 12. 4. 3..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UIImage+ImageCache.h"
#import "UTCache.h"
#import "UTDefine.h"

@implementation UIImage (ImageCache)

+ (UIImage *)imageCached:(NSString *)name{
    return [self imageCached:name inBundle:[NSBundle mainBundle]];
}
+ (UIImage *)imageUncached:(NSString*)name{
    return [self imageUncached:name inBundle:[NSBundle mainBundle]];
}
- (id)initWithImageCached:(NSString *)name{
    return [self initWithImageCached:name inBundle:[NSBundle mainBundle]];
}
- (id)initWithImageUncached:(NSString *)name{
    return [self initWithImageUncached:name inBundle:[NSBundle mainBundle]];
}

+ (UIImage *)imageCached:(NSString *)name  inBundle:(NSBundle *)bundle{
    UIImage * image = [[UTCache defaultCenter] memCacheForKey:name];
    if(image) return image;
    image = [UIImage imageUncached:name];
    if(image == nil) return image;
    [[UTCache defaultCenter] setMemCache:image forKey:name];
    return image;
}
- (id)initWithImageCached:(NSString *)name  inBundle:(NSBundle *)bundle{
    UIImage * image = [[UTCache defaultCenter] memCacheForKey:name];
    if(image){
        return image;
    }
    image = [[UIImage alloc] initWithImageUncached:name];
    if(image == nil)
        return [self init];
    [[UTCache defaultCenter] setMemCache:image forKey:name];
    return image;
}


- (id)initWithImageUncached:(NSString *)imageName  inBundle:(NSBundle *)bundle{
    NSString	*path	= nil;
	NSString	*name	= [imageName stringByDeletingPathExtension];
	NSString	*ext	= [imageName pathExtension];
	BOOL		isiPhone	= YES;
	if( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
	   [UIImage instancesRespondToSelector:@selector(scale)] )
	{
        
		if( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone )
		{
            isiPhone	= NO;
		}
		if( [[UIScreen mainScreen] scale] == 2.0 && isiPhone )
		{
			// 레티나 디스플레이 지원용 이미지 확인 작업.
			path = [bundle pathForResource:[name stringByAppendingString:@"@2x"] ofType:ext];
		}
        
	}
    
	if( nil == path )
	{
        path = [bundle pathForResource:name ofType:ext];
	}
    
	
    
	if( nil == path )
	{
		path = [bundle pathForResource:imageName ofType:nil];
	}
    return [self initWithContentsOfFile:path];
}

+ (UIImage *)imageUncached:(NSString *)imageName inBundle:(NSBundle *)bundle
{
    NSString	*path	= nil;
	NSString	*name	= [imageName stringByDeletingPathExtension];
	NSString	*ext	= [imageName pathExtension];
	BOOL		isiPhone	= YES;
	if( [[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
	   [UIImage instancesRespondToSelector:@selector(scale)] )
	{
        
		if( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone )
		{
            isiPhone	= NO;
		}
        if ( nil == path && IS_IPAD){
			path = [bundle pathForResource:[name stringByAppendingString:@"_iPad"] ofType:ext];
        }
        if(nil == path && IS_IPHONE4INCH){
			path = [bundle pathForResource:[name stringByAppendingString:@"-568h@2x"] ofType:ext];
        }
        if(nil == path && IS_IPHONE6){
            path = [bundle pathForResource:[name stringByAppendingString:@"-667h@2x"] ofType:ext];
            
        }
        if(nil == path && IS_IPHONE6P){
            path = [bundle pathForResource:[name stringByAppendingString:@"-1104h@2x"] ofType:ext];
        }
		if( nil == path && [[UIScreen mainScreen] scale] == 2.0 && isiPhone )
		{
			// 레티나 디스플레이 지원용 이미지 확인 작업.
			path = [bundle pathForResource:[name stringByAppendingString:@"@2x"] ofType:ext];
		}
        
        
	}
    
	if( nil == path )
	{
        path = [bundle pathForResource:name ofType:ext];
    }
    if( nil == path )
    {
        path = [bundle pathForResource:[name stringByAppendingString:@"@2x"] ofType:ext];
    }
	if( nil == path )
	{
		path = [bundle pathForResource:imageName ofType:nil];
    }
	UIImage	*image	= nil;
    if( path )
	{
		image = [[UIImage alloc] initWithContentsOfFile:path];
	}
#ifdef DEBUG
    if(image == nil)
    {
        //NSLog(@"not exist: %@",imageName);
//        @throw [NSException exceptionWithName:@"image_not_found" reason:[NSString stringWithFormat:@"Image named %@ is not found.", imageName] userInfo:nil];
    }
#endif
    
	return image;
}
//
//+ (UIImage *)imageCachedWithIdentifier:(NSString *)identifier cacheMode:(UTCacheMode)cacheMode{
//    NSData *cachedData = nil;
//    if(cacheMode == UTCacheMode_File){
//        cachedData = [[UTCacheCenter defaultCenter] fileCacheForKey:identifier];
//    }
//    else if(cacheMode == UTCacheMode_Memory){
//        cachedData =[[UTCacheCenter defaultCenter] memCacheForKey:identifier];
//    }
//    if(cachedData){
//        return [[UIImage alloc] initWithData:cachedData];
//    }
//    return nil;
//}

@end
// load from main bundle@end
