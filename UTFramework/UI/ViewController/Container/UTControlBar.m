//
//  UTContainControlView.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTControlBar.h"
#import "UTContainerViewController.h"

@interface UTControlBar ()
@property (nonatomic, assign) UTContainerViewController *containerVC;

- (void)_construct;
- (UILabel *)_titleLabel;
@end

@implementation UTControlBar{
    NSMutableArray *_controlButtons;
    UILabel *_titleLabel;
    UIView *_titleView;
    UIImageView *_oldView;
    BOOL _isOnTransition;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _construct];
    }
    return self;
}

- (id)initWithContainerViewController:(UTContainerViewController *)containerVC{
    if(self = [self init]){
        _containerVC = containerVC;
        [self _construct];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self _construct];
}

- (void)_construct{
    _controlButtons = [NSMutableArray array];
//    [self addSubview:[self _titleLabel]];
}

- (id)mfNavigationController{
    return _containerVC;
}

- (UTContainerViewController *)containerViewController{
    return _containerVC;
}

- (NSArray *)controlButtons{
    return _controlButtons;
}

- (void)setControlButtons:(NSArray *)items animated:(BOOL)animated{
    if([items isEqualToArray:_controlButtons])
        return;
    if(animated){
        for (UTControlButton *button in items){
            [button applyControlBarSize:self.frame.size];
            [button setAlpha:0.0f];
            [self addSubview:button];
        }
        [UIView animateWithDuration:0.25 animations:^{
            for (UTControlButton *button in _controlButtons){
                [button setAlpha:0.0f];
            }
            for (UTControlButton *button in items){
                [button setAlpha:1.0f];
            }
        } completion:^(BOOL finished) {
            if(finished){
                for (UTControlButton *button in _controlButtons){
                    [button removeFromSuperview];
                }
                [_controlButtons removeAllObjects];
                [_controlButtons addObjectsFromArray:items];
                for (UTControlButton *button in _controlButtons){
                    [button setAlpha:1.0f];
                    if(!button.superview){
                        [button applyControlBarSize:self.frame.size];
                        [self addSubview:button];
                    }
                }
            }
        }];
    }
    else{
        for (UTControlButton *button in _controlButtons){
            [button removeFromSuperview];
        }
        [_controlButtons removeAllObjects];
        [_controlButtons addObjectsFromArray:items];
        for (UTControlButton *button in _controlButtons){
            [button applyControlBarSize:self.frame.size];
            [self addSubview:button];
        }
    }
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    for (UTControlButton *button in _controlButtons){
        [button applyControlBarSize:self.frame.size];
    }
}
- (void)_layoutSubViews{
    CGSize size = self.frame.size;
    CGFloat maxWidth = 0;
    for (UTControlButton *button in _controlButtons){
        [button applyControlBarSize:size];
        maxWidth = MAX(button.frame.size.width, maxWidth);
    }
    maxWidth += 5;
    if(_titleView == _titleLabel){
        _titleLabel.frame = CGRectMake(maxWidth, 0, size.width - maxWidth * 2.0f, size.height);
    }
    else{
        _titleView.center = CGPointMake(size.width / 2.0f, size.height / 2.0f);
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self _layoutSubViews];
}

- (void)willTransitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type{
    _isOnTransition = YES;
    UIImage *titleImage = [UTUILibrary imageWithView:_titleView];
    _oldView = [[UIImageView alloc] initWithFrame:_titleView.frame];
    [_oldView setImage:titleImage];
}

- (void)transitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type{
    [self setControlButtons:toViewController.controlButtons animated:type != UTViewTransitionTypeNone];
    UIView *oldView = _oldView;
    _oldView = nil;
    [self addSubview:oldView];
    self.title = toViewController.title;
    if(toViewController.titleView){
        [self setTitleView:toViewController.titleView];
    }
    else if((!self.title || [self.title length] == 0) && self.defaultView){
        [self setTitleView:self.defaultView];
    }
    
    
    if(type == UTViewTransitionTypePop){
        CGRect frame = _titleView.frame;
        CGRect rFrame = oldView.frame;
        CGRect sFrame = self.frame;
        _titleView.frame = CGRectMake(frame.origin.x - sFrame.size.width / 2.0f, frame.origin.y, frame.size.width, frame.size.height);//CGRectOffset(frame, - sFrame.size.width, 0);
        [_titleView setAlpha:0.0];
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _titleView.frame = frame;
            oldView.frame =  CGRectMake(sFrame.size.width / 2.0f + rFrame.origin.x, rFrame.origin.y, rFrame.size.width, rFrame.size.height);
            [_titleView setAlpha:1.0];
            [oldView setAlpha:0.0];
        } completion:^(BOOL finished) {
            [oldView removeFromSuperview];
        }];
    }
    else if(type == UTViewTransitionTypePush){
        CGRect frame = _titleView.frame;
        CGRect rFrame = oldView.frame;
        CGRect sFrame = self.frame;
        _titleView.frame = CGRectMake(frame.origin.x + sFrame.size.width / 2.0f, frame.origin.y, frame.size.width, frame.size.height);
        [_titleView setAlpha:0.0];
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _titleView.frame = frame;
            oldView.frame =  CGRectMake(rFrame.origin.x - sFrame.size.width / 2.0f, rFrame.origin.y, rFrame.size.width, rFrame.size.height);
            [_titleView setAlpha:1.0];
            [oldView setAlpha:0.0];
        } completion:^(BOOL finished) {
            [oldView removeFromSuperview];
        }];
    }
    else{
        [oldView removeFromSuperview];
    }
}
- (void)didTransitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type{
    _isOnTransition = NO;
}

- (UILabel *)_titleLabel{
    if(_titleLabel){
        return _titleLabel;
    }
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textAlignment = UIBaselineAdjustmentAlignCenters;
    return _titleLabel;
    
}

- (void)setTitle:(NSString *)title{
    [self _titleLabel].text = title;
    if((self.title && [self.title length] > 0)){
        [self setTitleView:[self _titleLabel]];
    }
}

- (NSString *)title{
    return [self _titleLabel].text;
}

- (void)setTitleView:(UIView *)titleView{
    if(_titleView == titleView)
        return;
    [_titleView removeFromSuperview];
    _titleView = titleView;
    [self addSubview:titleView];
    [self _layoutSubViews];
}

- (UIView *)titleView{
    if(_titleView == _titleLabel)
        return nil;
    return _titleView;
}

- (UIView *)defaultView{
    return nil;
}

@end
