;//
//  UTNavigationCenter.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTNavigationCenter.h"
#import "UTRootViewController.h"
#import "UTUtils.h"
#import "UTMutableObject.h"
#import "UTApplication.h"
#import "UTDefine.h"


inline NSURL *u(NSString *string){
    return [NSURL URLWithString:string];
}

static NSString *className = nil;
static NSString *customUrlScheme = nil;
@interface UTNavigationCenter ()

- (NSMutableDictionary *)_getVcFactoryBlockHostListForScheme:(NSString *)scheme;
- (NSMutableDictionary *)_getVcFactoryBlockPathListForHost:(NSString *)host withSchemeDict:(NSMutableDictionary *)dict;
- (NSMutableDictionary *)_getVcFactoryBlockListForPath:(NSString *)path withHostDict:(NSMutableDictionary *)dict;

- (void)_loadURILookup;
@end
@implementation UTNavigationCenter{
    UTRootViewController *_rootViewController;
    NSInteger _blockCount;
    NSMutableDictionary *_vcFactories;
    UTNavigationCallback _defaultCallback;
}

+ (UTNavigationCenter *)defaultCenter{
    if(className){
        return [NSClassFromString(className) singleton];
    }
    else{
        return (UTNavigationCenter *)[self singleton];
    }
}

+ (void)setClassName:(NSString *)_className{
    className = _className;
    
}

+ (void)setCustomUrlScheme:(NSString *)_customUrlScheme{
    customUrlScheme = _customUrlScheme;
}

- (id)init{
    self = [super init];
    if(self){
        _rootViewController = [[UTRootViewController alloc] initWithNibName:nil bundle:nil];
        [UTNavigationCenter setCustomUrlScheme:app_string(@"APP_URL_SCHEME")];
        _blockCount = 0;
        _vcFactories = [NSMutableDictionary dictionary];
        _defaultCallback = ^BOOL(NSURL *url, NSDictionary *parameters) {
            UIViewController *vc = VC(url, parameters);
            [[UTNavigationCenter defaultCenter].rootViewController.navigationController pushViewController:vc animated:YES];
            return YES;
        };
        [self addNavigationURI:(@"//checkversion") callback:^BOOL(NSURL *url, NSDictionary *parameters) {
            if([[parameters objectForKey:@"version"] compare:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]] == NSOrderedDescending){
                return NO;
            }
            return YES;
        } defaultViewControllerClassName:nil];
        
        
        [self addNavigationURI:(@"//checksdkversion") callback:^BOOL(NSURL *url, NSDictionary *parameters) {
            if([[parameters objectForKey:@"version"] compare:[NSNumber numberWithDouble:SDK_VERSION]] == NSOrderedDescending){
                return NO;
            }
            return YES;
        } defaultViewControllerClassName:nil];
        
        [self _loadURILookup];
    }
    return self;
}

- (void)_loadURILookup{
}

- (void)dealloc{
    _rootViewController = nil;
    _vcFactories = nil;
}
- (UTRootViewController *)rootViewController{
    return _rootViewController;
}

- (void)setBlock:(BOOL)block{
    if(block)
        _blockCount ++;
    else
        _blockCount --;
    if(_blockCount < 0 )
        _blockCount = 0;

    if((_blockCount == 1 && block == YES) || (_blockCount == 0 && block == NO)){
        [_rootViewController setBlock:block];
    }
}


- (void)blockUIForJob:(dispatch_block_t)jobBlock{
    [self setBlock:YES];
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(taskQ, ^{
        jobBlock();
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self setBlock:NO];
        });
    });
}

- (BOOL)handleOpenURL:(NSURL *)url{
    if(![[url scheme] isEqualToString:customUrlScheme]){
        return false;
    }
    return [self openURL:url isInline:NO];
}


- (BOOL)openURL:(NSURL *)url parameters:(NSDictionary *)parameters{
   return [self openURL:url parameters:parameters isInline:YES];
}

- (BOOL)openURL:(NSURL *)url parameters:(NSDictionary *)parameters isInline:(BOOL)isInline{
    
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    if([vcAPI objectForKey:@"fields"] && !isInline && [[vcAPI objectForKey:@"fields"] boolValue] )
        return false;
    NSMutableDictionary *_parameters = nil;
    if( [[parameters objectForKey:@"___inherited"] boolValue] && [parameters isKindOfClass:[NSMutableDictionary class]]){
        _parameters = (NSMutableDictionary *)parameters;
    }
    else if(parameters != nil){
        _parameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
        [_parameters addEntriesFromDictionary:[UTUtils getParametersFromURL:url]];
    }
    else{
        _parameters = [NSMutableDictionary dictionaryWithDictionary:[UTUtils getParametersFromURL:url]];
    }
    
    if(vcAPI[@"necessaryParams"]){
        for (NSString *paramName in vcAPI[@"necessaryParams"]){
            if(_parameters[paramName] == nil){
                [UTNavigationCenter throwParameterRequired:paramName];
            }
        }
    }
    
    UTNavigationCallback block = [vcAPI objectForKey:@"block"];
    if(block){
        return block(url, [UTMutableObject objectFromDictionary:_parameters]);
    }
    return false;
}

+ (void)throwParameterRequired:(NSString *)paramter{
    @throw [NSException exceptionWithName:@"internal_error" reason:@"param required for navigation" userInfo:nil];
}
- (BOOL)openURL:(NSURL *)url isInline:(BOOL)isInline{
    return [self openURL:url parameters:nil isInline:isInline];
}

- (BOOL)openURL:(NSURL *)url{
    return [self openURL:url isInline:YES];
}



- (void)addNavigationURI:(NSString *)uri callback:(UTNavigationCallback)block  defaultViewControllerClassName:(NSString *)className{
    [self addNavigationURI:uri callback:block defaultViewControllerClassName:className isInline:YES];
}

- (void)addNavigationURI:(NSString *)uri callback:(UTNavigationCallback)block  defaultViewControllerClassName:(NSString *)className isInline:(BOOL)isInline{
    if(className == nil){
        className = @"UTViewController";
    }
    if([uri rangeOfString:@"//"].location != 0){
        uri = [@"//" stringByAppendingString:uri];
    }
    NSURL *url = [NSURL URLWithString:uri];
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    [vcAPI setDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[block copy], @"block", uri, @"uri", className, @"className", [NSNumber numberWithBool:isInline], @"isInline" ,nil]];
}

- (void)addNavigationURI:(NSString *)uri defaultViewControllerClassName:(NSString *)className{
    [self addNavigationURI:uri callback:_defaultCallback defaultViewControllerClassName:className];
}
- (void)addNavigationURI:(NSString *)uri defaultViewControllerClassName:(NSString *)className isInline:(BOOL)isInline{
    [self addNavigationURI:uri callback:_defaultCallback defaultViewControllerClassName:className isInline:isInline];
}

- (void)addURI:(NSString *)uri forViewControllerClassName:(NSString *)className{
    if([uri rangeOfString:@"//"].location != 0){
        uri = [@"//" stringByAppendingString:uri];
    }
    NSURL *url = [NSURL URLWithString:uri];
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    [vcAPI setDictionary:[NSDictionary dictionaryWithObjectsAndKeys:uri, @"uri", className, @"className" ,nil]];
}

- (void)setNecessaryParamaters:(NSArray *)necessaryParams forURI:(NSString *)uri{
    if([uri rangeOfString:@"//"].location != 0){
        uri = [@"//" stringByAppendingString:uri];
    }
    NSURL *url = [NSURL URLWithString:uri];
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    vcAPI[@"necessaryParams"] = necessaryParams;
}

- (NSMutableDictionary *)_getVcFactoryBlockHostListForScheme:(NSString *)scheme{
    if(scheme == nil){
        scheme = customUrlScheme;
    }
    id ret = [_vcFactories objectForKey:scheme];
    if(!ret){
        ret = [NSMutableDictionary dictionary];
        [_vcFactories setObject:ret forKey:scheme];
    }
    return ret;
}

- (NSMutableDictionary *)_getVcFactoryBlockPathListForHost:(NSString *)host withSchemeDict:(NSMutableDictionary *)dict{
    id ret = [dict objectForKey:host];
    if(!ret){
        ret = [NSMutableDictionary dictionary];
        [dict setObject:ret forKey:host];
    }
    return ret;
}

- (NSMutableDictionary *)_getVcFactoryBlockListForPath:(NSString *)path withHostDict:(NSMutableDictionary *)dict{
    if([path isEqualToString:@""])
        path = @"/";
    id ret = [dict objectForKey:path];
    if(!ret){
        ret = [NSMutableDictionary dictionary];
        [dict setObject:ret forKey:path];
    }
    return ret;
}

- (UIViewController *)viewControllerFromURL:(NSURL *)url parameters:(NSDictionary *)parameters{
    if([NSStringFromClass([parameters class]) isEqualToString:@"__NSDictionaryI"]){
        parameters = [UTMutableObject objectFromDictionary:parameters];
    }
    if(parameters == nil){
        parameters = [NSMutableDictionary dictionaryWithDictionary:[UTUtils getParametersFromURL:url]];
    }
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    return [[NSClassFromString([vcAPI objectForKey:@"className"]) alloc] initWithURL:url parameters:parameters];
}

- (UIViewController *)viewControllerFromURI:(NSString *)uri{
    if([uri rangeOfString:@"//"].location != 0){
        uri = [@"//" stringByAppendingString:uri];
    }
    NSURL *url = [NSURL URLWithString:uri];
    id parameters = [UTMutableObject object];
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    return [[NSClassFromString([vcAPI objectForKey:@"className"]) alloc] initWithURL:url parameters:parameters];
}

- (UIViewController *)viewControllerFromURI:(NSString *)uri parameters:(NSDictionary *)parameters{
    if([uri rangeOfString:@"//"].location != 0){
        uri = [@"//" stringByAppendingString:uri];
    }
    NSURL *url = [NSURL URLWithString:uri];
    if([NSStringFromClass([parameters class]) isEqualToString:@"__NSDictionaryI"]){
        parameters = [UTMutableObject objectFromDictionary:parameters];
    }
    if(parameters == nil){
        parameters = [NSMutableDictionary dictionaryWithDictionary:[UTUtils getParametersFromURL:url]];
    }
    NSMutableDictionary *vcAPI = [self _getVcFactoryBlockListForPath:[url path] withHostDict:[self _getVcFactoryBlockPathListForHost:[url host] withSchemeDict:[self _getVcFactoryBlockHostListForScheme:[url scheme]]]];
    return [[NSClassFromString([vcAPI objectForKey:@"className"]) alloc] initWithURL:url parameters:parameters];
}

- (NSString *)description{
    return [NSString stringWithFormat:@"===NavigatonCenter===\nUrlScheme:%@\nViewControllers%@",customUrlScheme,_vcFactories.description];
}

- (UINavigationController *)navigationController{
    return self.rootViewController.navigationController;
}

- (void)popViewControllerWithParameters:(id)parameters{
    [(UIViewController<UTViewController> *)(self.rootViewController.navigationController.topViewController) checkToSouldPopViewController:^(BOOL shouldPop) {
        if(shouldPop){
            NSString *animate = [parameters objectForKey:@"animate"];
            [[UTNavigationCenter defaultCenter].navigationController popViewControllerAnimated:animate ? [animate boolValue]: YES];
            [((UIViewController<UTViewController> *)[UTNavigationCenter defaultCenter].navigationController.topViewController) receiveData:parameters];
        }
    }];
}
@end
