//
//  UTTableView.h
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UTTableCell;

@protocol UTTableViewDelegate <UITableViewDelegate>
@optional
- (NSString *)tableCellName:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
@end

@interface UTTableView : UITableView
@property (nonatomic, assign) BOOL useSevenStyleGrouping;
@end
