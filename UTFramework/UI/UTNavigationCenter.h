//
//  UTNavigationCenter.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#ifndef UTNavigationCenter__h
#define UTNavigationCenter__h

#import "UTObject.h"
#import "UTViewController.h"
#define VC(url,parameters) [[UTNavigationCenter defaultCenter] viewControllerFromURL:url parameters:parameters]
#define GlobalNC [UTNavigationCenter defaultCenter]

@protocol UTViewControllerFactory <NSObject>

- (UIViewController *)viewControllerFromURL:(NSURL *)url parameters:(NSDictionary *)parameters;
- (UIViewController *)viewControllerFromURI:(NSString *)uri;
- (UIViewController *)viewControllerFromURI:(NSString *)uri parameters:(NSDictionary *)parameters;

@end

@class UTRootViewController;

typedef BOOL (^UTNavigationCallback)(NSURL *url, NSDictionary *parameters);

@interface UTNavigationCenter : UTObject<UTViewControllerFactory> {
    
}

extern NSURL *u(NSString *);

@property (nonatomic, strong) NSString *scheme;
- (UTRootViewController *)rootViewController;

- (void)setBlock:(BOOL)block;
- (void)blockUIForJob:(dispatch_block_t)jobBlock;

- (void)addNavigationURI:(NSString *)uri callback:(UTNavigationCallback)block defaultViewControllerClassName:(NSString *)className;
- (void)addNavigationURI:(NSString *)uri callback:(UTNavigationCallback)block defaultViewControllerClassName:(NSString *)className isInline:(BOOL)isInline;
- (void)addNavigationURI:(NSString *)uri defaultViewControllerClassName:(NSString *)className;
- (void)addNavigationURI:(NSString *)uri defaultViewControllerClassName:(NSString *)className isInline:(BOOL)isInline;
- (void)setNecessaryParamaters:(NSArray *)necessaryParams forURI:(NSString *)uri;
- (void)addURI:(NSString *)uri forViewControllerClassName:(NSString *)className;

- (BOOL)handleOpenURL:(NSURL *)url;
- (BOOL)openURL:(NSURL *)url isInline:(BOOL)isInline;
- (BOOL)openURL:(NSURL *)url parameters:(NSDictionary *)parameters isInline:(BOOL)isInline;
- (BOOL)openURL:(NSURL *)url parameters:(NSDictionary *)parameters;
- (BOOL)openURL:(NSURL *)url;

+ (UTNavigationCenter *)defaultCenter;
+ (void)setClassName:(NSString *)className;
+ (void)setCustomUrlScheme:(NSString *)customUrlScheme;
- (void)popViewControllerWithParameters:(id)parameters;

+ (void)throwParameterRequired:(NSString *)paramter;

- (UINavigationController *)navigationController;


@end

#endif