//
//  UTTableCell.m
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTTableCell.h"
#import "UTTableView.h"
#import "UTGroupTableCellBGView.h"
#import "UTDefine.h"

@interface UTTableCell ()
@property (nonatomic, strong) NSIndexPath *currentIndexPath;
@end
@implementation UTTableCell

+ (instancetype)getReusedCellFromTableView:(UITableView *)tableView withReusedentifier:(NSString*)reuseIdentifier indexPath:(NSIndexPath *)indexPath{
    
    static NSMutableDictionary *identifierLookup = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        identifierLookup = [NSMutableDictionary dictionary];
    });
    
    NSString *className = NSStringFromClass([self class]);
    
    if(reuseIdentifier == nil)
        reuseIdentifier = className;
    NSString *origianlReuseIdentifier = [identifierLookup objectForKey:reuseIdentifier];
    UTTableCell *cell;
    if(origianlReuseIdentifier){
        cell = [tableView dequeueReusableCellWithIdentifier:origianlReuseIdentifier];
    }
    if(cell == nil){
        
        NSString *nibName = [[NSBundle mainBundle] pathForResource:className ofType:@"nib"]? className : nil;
        if(!nibName){
            if(IS_IPHONE){
                NSString *iPhoneNibName = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_iPhone",className] ofType:@"nib"];
                if(iPhoneNibName){
                    nibName = [NSString stringWithFormat:@"%@_iPhone",className];
                }
            }
            else{
                NSString *iPadNibName = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_iPad",className] ofType:@"nib"];
                if(iPadNibName){
                    nibName = [NSString stringWithFormat:@"%@_iPad",className];
                }
            }
        }
        if(nibName){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibName owner:tableView options:nil];
            
            for (id oneObject in nib) {
                if ([oneObject isKindOfClass:[UTTableCell class]]) {
                    cell = oneObject;
                    if(cell.reuseIdentifier != nil && reuseIdentifier != nil)
                        [identifierLookup setObject:cell.reuseIdentifier forKey:reuseIdentifier];
                    break;
                }
            }
        }
        if(cell == nil){
            cell = [[[self class] alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        }
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    
    if([tableView isKindOfClass:[UTTableView class]]){
        if(((UTTableView *)tableView).useSevenStyleGrouping){
            UTGroupTableCellBGView *bgView = nil;
            BOOL isBottom = YES;
            for (UIView *view in cell.contentView.subviews){
                if([view isMemberOfClass:[UTGroupTableCellBGView class]]){
                    bgView = (UTGroupTableCellBGView *)view;
                    break;
                }
                isBottom = NO;
            }
            if(!bgView){
                bgView = [[UTGroupTableCellBGView alloc] initWithFrame:cell.contentView.bounds];
                bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                [cell.contentView insertSubview:bgView atIndex:0];
                isBottom = YES;
            }
            if(!isBottom){
                [cell.contentView sendSubviewToBack:bgView];
            }
            if(indexPath.row == 0 && [tableView numberOfRowsInSection:indexPath.section] == 1){
                bgView.position = GROUPTABLECELL_SINGLE;
            }else if(indexPath.row == 0){
                // top
                bgView.position = GROUPTABLECELL_TOP;
            }else if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1){
                // bottom
                bgView.position = GROUPTABLECELL_BOTTOM;
            }else{
                // middle
                bgView.position = GROUPTABLECELL_MIDDLE;
            }
            cell.contentView.backgroundColor = [UIColor whiteColor];
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = UIColorFromRGB(0xdddddd);
            [cell setSelectedBackgroundView:bgColorView];
        }
    }
    cell.currentIndexPath = indexPath;
    return cell;
}

- (void)applyData:(id)data{
    _data = data;
//    self.textLabel.text = [_data description];
}

- (void)dealloc{
    _data = nil;
}

- (void)prepareForReuse{
    
}

+ (CGFloat)cellHeightWithData:(id)data bounds:(CGSize)bounds{
    return 44.0f;
}

+ (UTTableCell*)getReusedCellFromName:(NSString*)name tableView:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    
    if([name length] > 0 && name != nil){
        return [NSClassFromString(name) getReusedCellFromTableView:tableView withReusedentifier:name indexPath:indexPath];
        }else{
            [NSException raise:@"Invalid cellname" format:@"cellname is invalid: %@", name];
            return nil;
        }

}

- (UITableView *)tableView{
    UIView* temp = self.superview;
    
    while(![temp isKindOfClass:[UITableView class]] && temp != nil){
        temp = temp.superview;
    }
    
    return (UITableView*)temp;
}
- (NSIndexPath *)currentIndexPath{
    if(_currentIndexPath == nil){
        @throw [NSException exceptionWithName:@"use getReusedCellFromTableView:withReusedentifier:indexPath:" reason:@"" userInfo:nil];
    }
    return _currentIndexPath;
}
@end
