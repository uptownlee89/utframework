// UIImage+RoundedCorner.h
// Created by Trevor Harmon on 9/20/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

// Extends the UIImage class to support making rounded corners
#import <UIKit/UIKit.h>
typedef enum {
    UIImageRoundedCornerTopLeft = 1,
    UIImageRoundedCornerTopRight = 1 << 1,
    UIImageRoundedCornerBottomRight = 1 << 2,
    UIImageRoundedCornerBottomLeft = 1 << 3,
    UIImageRoundedCornerAll = 1 | 1 << 1 | 1 <<2 | 1 << 3
} UIImageRoundedCorner;

@interface UIImage (RoundedCorner)
- (UIImage *)roundedCornerImage:(CGFloat)cornerSize borderSize:(NSInteger)borderSize mask:(UIImageRoundedCorner)mask;
@end
