//
//  UTNavController.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 10. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTNavigationController.h"
#import "UTControlButton+Protected.h"

@interface UTNavigationController ()

@end

@implementation UTNavigationController

- (id)initWithRootViewController:(UTViewController *)vc{
    return [self initWithRootViewController:vc controlBarClass:[UTNavigationBar class]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLeftButton:(UTControlButton *)leftButton leftPadding:(CGFloat)padding{
//    //NSLog(@"%@", self.topViewController);
    if(leftButton == nil){
        [self.topViewController removeControlButtonForIdentifier:UTNavigationLeftButtonIdentifier];
    }
    else{
        leftButton.identifier = UTNavigationLeftButtonIdentifier;
        leftButton.position = UTControlButtonPositionLeft;
        __weak UTControlButton *wButton = leftButton;
        __weak UTControlBar *wBar = self.controlBar;
        leftButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleHeight;
        [leftButton setSizeCallback:^(UTControlButton *button, CGSize barSize) {
            
            UTControlButton *sButton = wButton;
            UTControlBar *sBar = wBar;
            if(sButton.frame.size.height >sBar.bounds.size.height){
                sButton.frame = CGRectMake(padding, 0, sButton.frame.size.width,  sBar.bounds.size.height);
            }
            else{
                sButton.frame = CGRectMake(padding,( sBar.frame.size.height - sButton.frame.size.height) / 2.0f, sButton.frame.size.width, sButton.frame.size.height);
            }
        }];
        [self.topViewController addControlButton:leftButton];
    }
}
- (void)setRightButton:(UTControlButton *)rightButton rightPadding:(CGFloat)padding{
//    //NSLog(@"%@", self.topViewController);
    if(rightButton == nil){
        [self.topViewController removeControlButtonForIdentifier:UTNavigationRightButtonIdentifier];
    }
    else{
        rightButton.identifier = UTNavigationRightButtonIdentifier;
        rightButton.position = UTControlButtonPositionRight;
        __weak UTControlButton *wButton = rightButton;
        __weak UTControlBar *wBar = self.controlBar;
        rightButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        [rightButton setSizeCallback:^(UTControlButton *button, CGSize barSize) {
            
            UTControlBar *sBar = wBar;
            UTControlButton *sButton = wButton;
            if(sButton.frame.size.height >sBar.bounds.size.height){
                sButton.frame = CGRectMake(sBar.frame.size.width - sButton.frame.size.width - padding, 0, sButton.frame.size.width, sBar.bounds.size.height);
            }
            else{
                sButton.frame = CGRectMake(sBar.frame.size.width - sButton.frame.size.width - padding, ( sBar.frame.size.height - sButton.frame.size.height) / 2.0f, sButton.frame.size.width, sButton.frame.size.height);
            }
        }];
        [self.topViewController addControlButton:rightButton];
    }
}
- (void)setLeftButton:(UTControlButton *)leftButton{
    [self setLeftButton:leftButton leftPadding:0];
    
}
- (void)setRightButton:(UTControlButton *)rightButton{
    [self setRightButton:rightButton rightPadding:0];
}

- (UTControlButton *)leftButton{
    return [self.topViewController controlButtonForIdentifier:UTNavigationLeftButtonIdentifier];
}

- (UTControlButton *)rightButton{
    return [self.topViewController controlButtonForIdentifier:UTNavigationRightButtonIdentifier];
}

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    [self setControlBarHidden:hidden animated:animated];
}

- (void)setNavigationBarHidden:(BOOL)navigationBarHidden{
    [self setControlBarHidden:navigationBarHidden];
}

- (BOOL)navigationBarHidden{
    return self.controlBarHidden;
}
@end
