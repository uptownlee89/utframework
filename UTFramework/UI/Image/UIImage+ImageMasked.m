//
//  UIImage+ImageMasked.m
//  MMTFramework
//
//  Created by 엄태건 on 13. 9. 13..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UIImage+ImageMasked.h"

@implementation UIImage (ImageMasked)

+(UIImage*)imageMasked:(UIImage*)image maskImage:(UIImage*)maskImage
{
	CGImageRef imageRef = [image CGImage];
	CGImageRef maskRef = [maskImage CGImage];
    
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef),
                                        NULL, false);
    
	CGImageRef masked = CGImageCreateWithMask(imageRef, mask);
	CGImageRelease(mask);
    
	UIImage *maskedImage = [UIImage imageWithCGImage:masked];
	CGImageRelease(masked);
	
	return maskedImage;
}

- (UIImage*)masking:(UIImage *)maskImage{
    UIImage* image = self;
    CGImageRef imageRef = [image CGImage];
	CGImageRef maskRef = [maskImage CGImage];
    
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef),
                                        NULL, false);
    
	CGImageRef masked = CGImageCreateWithMask(imageRef, mask);
	CGImageRelease(mask);
    
	UIImage *maskedImage = [UIImage imageWithCGImage:masked];
	CGImageRelease(masked);
	
	return maskedImage;
}
@end
