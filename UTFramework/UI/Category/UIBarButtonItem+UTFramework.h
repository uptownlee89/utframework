//
//  UIBarButtonItem+UTFramework.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+UTFramework.h"



@interface UIBarButtonItem (UTFramework)

- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem callback:(UTActionBlock)action;
- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action;
- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action;
- (id)initWithImage:(UIImage *)image landscapeImagePhone:(UIImage *)landscapeImagePhone style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action NS_AVAILABLE_IOS(5_0);
@end
