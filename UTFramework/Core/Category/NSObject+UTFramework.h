//
//  NSObject+UTFramework.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^UTActionBlock)(id sender);

@interface NSObject (UTFramework)

- (void)retainAssociateValue:(id)value withKey:(const char *)key;
- (void)copyAssociateValue:(id)value withKey:(const char *)key;
- (void)assignAssociateValue:(id)value withKey:(const char *)key;
- (id)associatedValueForKey:(const char *)key;
- (void)removeAllAssociatedObjects;

+ (void)retainAssociateValue:(id)value withKey:(const char *)key;
+ (void)copyAssociateValue:(id)value withKey:(const char *)key;
+ (void)assignAssociateValue:(id)value withKey:(const char *)key;
+ (id)associatedValueForKey:(const char *)key;
+ (void)removeAllAssociatedObjects;
@end
