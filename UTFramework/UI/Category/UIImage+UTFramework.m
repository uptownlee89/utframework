
//
//  UIImage+UTFramework.m
//  UTFramework
//
//  Created by Lee, Juyoung on 2014. 11. 5..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UIImage+UTFramework.h"
#import <AFNetworking/AFNetworking.h>
#import "UTCache.h"

@implementation UIImage(UTFramework)

+ (void)getImageFromURL:(NSURL *)url callback: (void (^)(UIImage*))callback{
    NSString *identifier = [url absoluteString];
    UIImage * cachedData =[[UTCache defaultCenter] memCacheForKey:identifier];
    if(cachedData){
        callback(cachedData);
        return;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPShouldHandleCookies:NO];
//    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject){
            callback(responseObject);
            [[UTCache defaultCenter] setMemCache:responseObject forKey:identifier];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    [requestOperation start];
    
    
}
@end
