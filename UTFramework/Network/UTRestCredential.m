//
//  UTRestCredential.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 17..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTRestCredential.h"
#import "UTRestCenter.h"

@interface UTRestCredential ()

@property (nonatomic, strong) NSString *access_token;
@property (nonatomic, strong) NSString *refresh_token;
@property (nonatomic, strong) NSString *token_type;
@property (nonatomic, strong) NSDate *expire;

@end
@implementation UTRestCredential


- (id)initWithCredential:(NSDictionary *)credential{
    if(self = [super init]){
        self.access_token = credential[@"access_token"];
        self.refresh_token = credential[@"refresh_token"];
        self.token_type = credential[@"token_type"];
        NSLog(@"access_token: %@", self.access_token);
        if(credential[@"expires_in"]){
            self.expire = [NSDate dateWithTimeIntervalSinceNow:[credential[@"expires_in"] intValue]];
        }
        else{
            self.expire = credential[@"expire"];
        }
    }
    return self;
}

- (BOOL)is_expired{
    return [self.expire compare:[NSDate new]] == NSOrderedAscending;
}

+ (void)setDefaultCredential:(UTRestCredential *)credential{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if(credential){
        [userDefaults setObject:@{@"access_token": credential.access_token,
                                  @"refresh_token": credential.refresh_token,
                                  @"token_type": credential.token_type,
                                  @"expire": credential.expire,
                                  }
                         forKey:@"ut_rest_credential"];
        
    }
    else{
        [userDefaults removeObjectForKey:@"ut_rest_credential"];
    }
    [userDefaults synchronize];
    [[UTRestCenter defaultCenter] setRestCredential:credential];
}

+ (UTRestCredential *)defaultCredential{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [userDefaults objectForKey:@"ut_rest_credential"];
    if(dict){
        return [[UTRestCredential alloc] initWithCredential:dict];
    }
    return nil;
}

+ (void)getCredential:(NSString *)tokenUrl parameters:(NSDictionary *)paramemters callback:(UTRestCredentialCallback)callback{
    [[UTRestCenter defaultCenter] POSTUnsafe:tokenUrl parameters:paramemters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        //NSLog(@"%@", [NSString stringWithUTF8String:[responseObject bytes]]);
        callback(operation, [[UTRestCredential alloc] initWithCredential:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(operation, nil, error);
    }];
}
@end
