//
//  MMTThreeColoredLineView.h
//  UTFramework
//
//  Created by 주영 이 on 13. 8. 22..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    GROUPTABLECELL_TOP,
    GROUPTABLECELL_MIDDLE,
    GROUPTABLECELL_BOTTOM,
    GROUPTABLECELL_SINGLE,
    GROUPTABLECELL_UNDEFINED
}GroupTableCellPosition;

@interface UTGroupTableCellBGView : UIView

@property (nonatomic,assign) GroupTableCellPosition position;

@end
