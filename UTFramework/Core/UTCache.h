//
//  UTCache.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTObject.h"


@interface UTCache : UTObject

+ (id)defaultCenter;

- (id)memCacheForKey:(id)key;
- (id)memCacheForKey:(id)key loadCallback:(id (^)())callback;
- (void)setMemCache:(id)obj forKey:(id)key;

- (void)flushFileCache;
- (void)removeFileCacheCreatedBefore:(NSDate *)createdDate;
- (void)removeFileCache:(NSString *)identifier context:(NSString *)context;
- (void)removeFileCache:(NSString *)identifier;
- (void)setFileCache:(NSData *)data forKey:(NSString*)identifier;
- (void)setFileCache:(NSData *)data forKey:(NSString*)identifier context:(NSString *)context;

- (NSData *)fileCacheForKey:(NSString *)identifier;
- (NSData *)fileCacheForKey:(NSString *)identifier context:(NSString *)context;
- (unsigned long long int)fileCacheSize;
@end
