//
//  UTImage.m
//  UTFramework
//
//  Created by Juyoung Lee on 12. 6. 27..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UIImage+resizable.h"
#import <QuartzCore/QuartzCore.h>


@implementation UIImage (resizable)


- (UIImage *)resizableImageWithCapInsets:(UIEdgeInsets)capInsets isStetchable:(BOOL)isStetchable{
    if(isStetchable){
        return [self resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
    }
    return [self resizableImageWithCapInsets:capInsets];
}

@end
