//
//  UIImage+ImageCache.h
//  UTFramework
//
//  Created by Juyoung Lee on 12. 4. 3..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface UIImage (ImageCache)

+ (UIImage *)imageCached:(NSString *)name;
+ (UIImage *)imageUncached:(NSString*)name;
- (id)initWithImageCached:(NSString *)name;
- (id)initWithImageUncached:(NSString *)name;


+ (UIImage *)imageCached:(NSString *)name inBundle:(NSBundle *)bundle;
+ (UIImage *)imageUncached:(NSString*)name inBundle:(NSBundle *)bundle;
- (id)initWithImageCached:(NSString *)name inBundle:(NSBundle *)bundle;
- (id)initWithImageUncached:(NSString *)name inBundle:(NSBundle *)bundle;


//+ (UIImage *)imageCachedWithIdentifier:(NSString *)identifier cacheMode:(UTCacheMode)cacheMode;
@end
