//
//  UTControlButton+Protected.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 10. 7..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTControlButton.h"

@interface UTControlButton (Protected)

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, assign) UTControlButtonPosition position;
@property (nonatomic, assign) UTControlButtonType type;

@end
