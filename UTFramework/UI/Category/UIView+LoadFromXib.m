//
//  UIView+LoadFromXib.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 8. 26..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UIView+LoadFromXib.h"

@implementation UIView (LoadFromXib)

+ (id) loadNib{
    return [self loadNibNamed: NSStringFromClass([self class])];
}
+ (id) loadNibNamed:(NSString *) nibName {
    return [UIView loadNibNamed:nibName fromBundle:[NSBundle mainBundle] retainingObjectWithTag:NSUIntegerMax];
}

+ (id) loadNibNamed:(NSString *) nibName fromBundle:(NSBundle *) bundle retainingObjectWithTag:(NSUInteger) tag {
    NSString *nibPath = [bundle pathForResource:nibName ofType:@"nib"];
    if(!nibPath)
        return nil;
    NSArray * nib = [bundle loadNibNamed:nibName owner:nil options:nil];
    if(!nib) return nil;
    UIView * target = nil;
    if(tag == NSUIntegerMax){
        return nib[0];
    }
    for(UIView * view in nib) {
        if(view.tag == tag) {
            target = view;
            break;
        }
    }
    if(target && [target respondsToSelector:@selector(viewDidLoad)]) {
        [target performSelector:@selector(viewDidLoad)];
    }
    return target;
    
}

@end
