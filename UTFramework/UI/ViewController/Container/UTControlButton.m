//
//  UTControlButton.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 26..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTControlButton.h"

@interface UTControlButton ()
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, assign) UTControlButtonPosition position;
@property (nonatomic, assign) UTControlButtonType type;

@end

@implementation UTControlButton{
    UTControlButtonCallback _callback;
    UTControlButtonSizeCallback _sizeCallback;
}

- (id)initWithType:(UTControlButtonType)type position:(UTControlButtonPosition)position{
    if(self = [super initWithFrame:CGRectZero]){
        _position = position;
        [self _contruct];
    }
    return self;
}

- (id)initWithIdentifier:(NSString *)identifier{
    if(self = [super initWithFrame:CGRectZero]){
        _identifier = identifier;
        [self _contruct];
    }
    return self;
}
- (void)_contruct{
    
}

- (void)dealloc{
    _callback = nil;
}

- (void)applyControlBarSize:(CGSize)size{
    if(_sizeCallback){
        _sizeCallback(self, size);
    }
    else{
        if(_position == UTControlButtonPositionLeft){
            self.frame = CGRectMake(0, 0, size.height, size.height);
        }
        else{
            self.frame = CGRectMake(size.width - size.height, 0, size.height, size.height);
        }
    }
}


- (void)addCallback:(UTControlButtonCallback)callback forControlEvents:(UIControlEvents)controlEvents{
    //    [_callbacks setObject:[callback copy] forKey:@(0)];
    [self removeTarget:self action:@selector(_action:forEvent:) forControlEvents:controlEvents];
    _callback = [callback copy];
    [self addTarget:self action:@selector(_action:forEvent:) forControlEvents:controlEvents];
}

- (void)_action:(UTControlButton *)sender forEvent:(UIEvent *)event{
//    UTControlButtonCallback callback = [_callbacks objectForKey:@(0)];
    if(_callback)
        _callback(sender);
}

- (id)initWithImage:(UIImage *)image position:(UTControlButtonPosition)position{
    
    if(self = [super initWithFrame:CGRectZero]){
        _position = position;
        [self setImage:image forState:UIControlStateNormal];
        _type = UTControlButtonTypeImage;
        [self _contruct];
    }
    return self;
}
- (id)initWithText:(NSString *)text position:(UTControlButtonPosition)position{
    if(self = [super initWithFrame:CGRectZero]){
        _position = position;
        [self setTitle:text forState:UIControlStateNormal];
        _type = UTControlButtonTypeImage;
        [self _contruct];
    }
    return self;
}

- (void)setSizeCallback:(UTControlButtonSizeCallback)callback{
    _sizeCallback = [callback copy];
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%p %@ %@ %d %d %@ %@",self, _identifier, NSStringFromCGRect(self.frame), self.position, self.type, self.titleLabel.text, self.imageView.image];
}
@end
