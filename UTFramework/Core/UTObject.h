//
//  UTObject.h
//  UTFramework
//
//  Created by Juyoung Lee on 13. 1. 29..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UTObject : NSObject
+ (instancetype)singleton;
- (id)addObserverForName:(NSString *)name object:(id)obj queue:(NSOperationQueue *)queue usingBlock:(void (^)(NSNotification *))block;

@property (strong, nonatomic) id model;
@end
