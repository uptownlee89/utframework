//
//  UTTable.m
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTArrayProvider.h"
#import "UTDataProvider+Protected.h"

@interface UTArrayProvider (){
    NSMutableArray *_defaultStorage;
}
@end
@implementation UTArrayProvider

- (id)objectAtIndex:(NSUInteger)index{
    if ([self count] > index)
        return [_defaultStorage objectAtIndex:index];
    return nil;
}

- (id)objectAtIndexedSubscript:(NSUInteger)index{
    return [self objectAtIndex:index];
}

- (NSUInteger)count{
    return [_defaultStorage count];
}
- (id)initWithArray:(NSArray *)array{
    self = [super init];
    if(self){
        _defaultStorage = [NSMutableArray arrayWithArray:array];
        [self _statusChangeTo:UTDataProviderLoaded];
    }
    return self;
}

- (id)init{
    self = [super init];
    if(self){
        _defaultStorage = [NSMutableArray array];
    }
    return self;
}
- (id)initWithContentsOfFile:(NSString *)file{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"plist"];
    NSArray *data = [NSMutableArray arrayWithContentsOfFile:path];
    
    return[self initWithArray:data];
}
- (id)initWithContentsOfFile:(NSString *)file inBundle:(NSBundle *)bundle{
    
    NSString *path = [bundle pathForResource:file ofType:@"plist"];
    NSArray *data = [NSMutableArray arrayWithContentsOfFile:path];
    
    return[self initWithArray:data];
}

- (void)dealloc{
    _defaultStorage = nil;
}

- (void)asyncGetMoreDataWithCallback:(UTDataProviderCallback)callback{
    callback(nil, nil);
}

- (void)asyncLoadWithCallback:(UTDataProviderCallback)callback{
    callback([NSArray arrayWithArray:_defaultStorage], nil);
}

- (void)_setData:(NSArray *)data{
    [_defaultStorage setArray:data];
}

- (void)_getMoreData:(id)data{
    [_defaultStorage addObjectsFromArray:data];
}

- (NSUInteger)indexOfObject:(id)anObject{
    return [_defaultStorage indexOfObject:anObject];
}

@end
