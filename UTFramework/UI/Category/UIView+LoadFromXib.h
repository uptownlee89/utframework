//
//  UIView+LoadFromXib.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 8. 26..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LoadFromXib)
+ (id) loadNib;
+ (id) loadNibNamed:(NSString *) nibName;
+ (id) loadNibNamed:(NSString *) nibName fromBundle:(NSBundle *) bundle retainingObjectWithTag:(NSUInteger) tag ;
@end
