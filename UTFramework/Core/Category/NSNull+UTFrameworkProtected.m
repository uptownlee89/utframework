//
//  NSNull.m
//  UTFramework
//
//  Created by Lee, Juyoung on 2014. 10. 22..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "NSNull+UTFrameworkProtected.h"
@implementation NSNull (UTFrameworkProtected)

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    return [NSMethodSignature signatureWithObjCTypes:"@@:"];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    id ret = nil;
    [anInvocation setReturnValue:&ret];
}


- (NSUInteger)length{
    return 0;
}
@end
