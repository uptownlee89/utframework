//
//  UTAPICenter.h
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 5..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "UTRestCredential.h"

static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

typedef void (^UTRestCredentialInvalidCallback)(NSError *error);

@interface UTJSONResponseSerializerWithData : AFJSONResponseSerializer

@end

@protocol UTMutableObject;

@interface UTRestCenter : AFHTTPRequestOperationManager
@property (nonatomic, readonly) UTRestCredential *restCredential;
@property (nonatomic, copy) UTRestCredentialInvalidCallback credentialInvalidCallback;

- (void)setRestCredential:(UTRestCredential *)restCredential;
+ (UTRestCenter *)defaultCenter;

- (AFHTTPRequestOperation *)POSTUnsafe:(NSString *)URLString
                            parameters:(NSDictionary *)parameters
                               success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
