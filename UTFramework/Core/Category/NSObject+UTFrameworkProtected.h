//
//  NSObject+UTFrameworkProtected.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <Foundation/Foundation.h>

static char* kUTFrameworkBlockKey = "kUTFrameworkBlockKey";
@interface NSObject (UTFrameworkProtected)
- (void)_invokeBlock:(id)sender;
@end
