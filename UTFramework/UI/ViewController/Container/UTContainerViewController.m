//
//  UTContainerViewController.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTContainerViewController.h"
#import "UTContainerViewController+Protected.h"
#import <QuartzCore/QuartzCore.h>
#import "UTUILibrary.h"
#import "UTSideMenuViewController.h"
#import "UIView+LoadFromXib.h"
#import "UTControlBar+Protected.h"
#import "UTNavigationCenter.h"
#import "UTUILibrary.h"
#import "UIView+MTAnimation.h"

@interface UTContainerViewController (){
    UTViewController *_topViewController;
    UTControlBar *_controlBar;
    UIView *_contentView;
    NSMutableArray *_viewControllers;
    BOOL _isUnloaded;
    BOOL _isNavigationWorking;
    void (^_reservedNavBarHide)();
    UIView *_statusViewBackground;
}

@property (nonatomic, assign) BOOL isAnimating;

- (UTViewController *)_viewControllerFromVCOrUrl:(id)VCOrUrl;
@end

@implementation UTContainerViewController
@synthesize controlBar = _controlBar;

- (UTViewController *)_viewControllerFromVCOrUrl:(id)VCOrUrl{
    if([VCOrUrl isKindOfClass:[NSDictionary class]]){
        return (UTViewController *)[self.datasource viewControllerFromURI:[VCOrUrl objectForKey:@"uri"] parameters:[VCOrUrl objectForKey:@"params"]];
    }
    return VCOrUrl;
}

- (NSArray *)viewControllers{
    return _viewControllers;
}

- (id)initWithRootViewController:(UTViewController *)vc controlBarClass:(Class)theClass{
    self = [super initWithNibName:nil bundle:nil];
    if(self){
        _viewControllers  = [NSMutableArray array];
        _topViewController = vc;
        [vc willMoveToParentViewController:self];
        [self addChildViewController:vc];
        _controlBar = [theClass loadNib];
        if(!_controlBar)
            _controlBar = [[theClass alloc] initWithContainerViewController:self];
        else{
            _controlBar.containerVC = self;
        }
        [vc didMoveToParentViewController:self];
        _isAnimating = NO;
        _isNavigationWorking = NO;
        self.datasource = [UTNavigationCenter defaultCenter];
    }
    return self;
}

- (id)initWithRootViewController:(UTViewController *)vc{
    return [self initWithRootViewController:vc controlBarClass:[UTControlBar class]];
}

- (void)setRootViewController:(UTViewController *)vc{
    if(!vc){
        //NSLog(@"viewcontroller is null...");
        return;
    }
    if([vc.url isEqual:_topViewController.url] && [vc.parameters isEqualToDictionary:_topViewController.parameters]){
        return;
    }
    else{
        void (^block) () =  ^void(){
            [self popToRootViewControllerAnimated:YES];
            
            UTViewController *popedVC = [self _viewControllerFromVCOrUrl:[_viewControllers lastObject] ];
            [_viewControllers removeLastObject];
            UTViewController *viewingVC = vc;
            [_viewControllers addObject:vc];
            
            viewingVC.view.frame = popedVC.view.frame;
            [viewingVC willMoveToParentViewController:self];
            [self addChildViewController:viewingVC];
            
            [_controlBar willTransitionFrom:popedVC to:viewingVC transitionType:UTViewTransitionTypeNone];
            self.topViewController = viewingVC;
            [_contentView addSubview:viewingVC.view];
            [_controlBar transitionFrom:popedVC to:viewingVC transitionType:UTViewTransitionTypeNone];
            [_controlBar didTransitionFrom:popedVC to:viewingVC transitionType:UTViewTransitionTypeNone];
            _controlBar.title = _topViewController.title;
            
            [popedVC willMoveToParentViewController:nil];
            
            [popedVC.view removeFromSuperview];
            [popedVC removeFromParentViewController];
            [popedVC didMoveToParentViewController:nil];
            [viewingVC didMoveToParentViewController:self];
//            _topViewController = viewingVC;
            _topViewController.view.frame = [self frameForViewController:_topViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
        };
        block();
    }
}

- (void)setTopViewController:(UTViewController *)topViewController{
    _topViewController = topViewController;
    _contentView.backgroundColor = topViewController.view.backgroundColor;
    self.view.backgroundColor = topViewController.view.backgroundColor;
    [_contentView bringSubviewToFront:_statusViewBackground];
    [self didTopViewControllerChanged];
}

- (void)didTopViewControllerChanged{
}

- (void)loadView{
    if (self.parentViewController){
        self.view = [[UIView alloc] initWithFrame:self.parentViewController.view.frame];
        self.view.backgroundColor = [UIColor whiteColor];
    }
    else{
        self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.view.backgroundColor = [UIColor whiteColor];
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _statusViewBackground = [[UIView alloc] init];
    [_statusViewBackground setBackgroundColor:[UIColor whiteColor]];
    [_statusViewBackground setAlpha:0.9];
    _statusViewBackground.frame = [self frameForStatusBarBackground:[UIApplication sharedApplication].statusBarOrientation];
    
    [_viewControllers addObject:_topViewController];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    CGRect rect =  [self frameForContentView:[UIApplication sharedApplication].statusBarOrientation];
    _contentView = [[UIView alloc] initWithFrame:rect];
    _contentView.autoresizesSubviews = YES;
    _contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    [_contentView setBackgroundColor:[UIColor whiteColor]];
    _topViewController.view.frame = [self frameForViewController:_topViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
    
    _controlBar.frame = [self frameForControlBar:[UIApplication sharedApplication].statusBarOrientation];
    _controlBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [_topViewController willMoveToParentViewController:self];
    
    [_controlBar willTransitionFrom:nil to:_topViewController transitionType:UTViewTransitionTypeNone];
    self.topViewController = _topViewController;
    [_contentView addSubview:_topViewController.view];
    [self.view addSubview:_contentView];
    [self.view addSubview:_controlBar];
    [_controlBar transitionFrom:nil to:_topViewController transitionType:UTViewTransitionTypeNone];
    [_controlBar didTransitionFrom:nil to:_topViewController transitionType:UTViewTransitionTypeNone];
    [_topViewController didMoveToParentViewController:self];
    
    [self.view addSubview:_statusViewBackground];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    _contentView.frame = [self frameForContentView:[UIApplication sharedApplication].statusBarOrientation];
    _topViewController.view.frame = [self frameForViewController:_topViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
    _controlBar.frame = [self frameForControlBar:[UIApplication sharedApplication].statusBarOrientation];
    _statusViewBackground.frame = [self frameForStatusBarBackground:[UIApplication sharedApplication].statusBarOrientation];
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    _controlBarHiddenRate = _controlBarHidden? 1.0f : 0;
}

- (void)dealloc{
    _viewControllers = nil;
    _contentView = nil;
    _topViewController = nil;
    _statusViewBackground = nil;
}

- (void)_pushViewController:(UTViewController *)viewController animated:(BOOL)animated{
    _isNavigationWorking = YES;
    if(!viewController){
        viewController = [[UTViewController alloc] initWithNibName:nil bundle:nil];
    }
    UTViewController *oldViewController = _topViewController;
//    CGRect oldFrame = oldViewController.view.frame;
    
    
    [self addChildViewController:viewController];
    [oldViewController willMoveToParentViewController:nil];
    
    [_viewControllers addObject:viewController];
    
    [self.controlBar willTransitionFrom:oldViewController to:viewController transitionType:animated ? UTViewTransitionTypePush: UTViewTransitionTypeNone];
    UIView *oldView = oldViewController.view;
    self.topViewController = viewController;
    [_contentView addSubview:viewController.view];
    [self.controlBar transitionFrom:oldViewController to:viewController transitionType:animated ? UTViewTransitionTypePush: UTViewTransitionTypeNone];
    
    void (^completionBlock)() = ^{
        [oldView removeFromSuperview];
        [viewController didMoveToParentViewController:self];
        [oldViewController removeFromParentViewController];
        [self.controlBar didTransitionFrom:oldViewController to:viewController transitionType:animated ? UTViewTransitionTypePush: UTViewTransitionTypeNone];
    };
    
    CGRect newFrame = [self frameForViewController:viewController orientation:[UIApplication sharedApplication].statusBarOrientation];
    if( animated){
        
        
        // Get the views
        UIView * fromView = oldView;
        UIView * toView = viewController.view;
        UIView *darkenView = [[UIView alloc] initWithFrame:fromView.frame];
        [darkenView setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0]];
        [fromView addSubview:darkenView];
        
        // Get the size of the view area.
        CGRect viewSize = fromView.frame;
        
        // Add the new view to the old view.
        [fromView.superview addSubview:toView];
        
        // Position the new view outside of the screen
        toView.frame = CGRectMake( viewSize.size.width , viewSize.origin.y, viewSize.size.width, viewSize.size.height);
        [UIView mt_animateViews:[NSArray arrayWithObjects:fromView, toView, nil]
                       duration:.55
                 timingFunction:MTTimingFunctionEaseOutExpo
                        options:(MTViewAnimationOptions)UIViewAnimationOptionAllowAnimatedContent
                     animations:
         ^{
             // Animate the replacing of the views
             fromView.frame =CGRectMake( -(viewSize.size.width/3) , viewSize.origin.y, viewSize.size.width, viewSize.size.height);
             toView.frame =CGRectMake(0, viewSize.origin.y, viewSize.size.width, viewSize.size.height);
             [darkenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.1]];
         }
                     completion:
         ^{
             // Remove the old view 
             [darkenView removeFromSuperview];
//             [fromView removeFromSuperview];
             completionBlock();
             _isAnimating = NO;
         }];
        
//        CALayer *aLayer = viewController.view.layer;
//        [aLayer setShadowColor:[UIColor blackColor].CGColor];
//        [aLayer setShadowRadius:10.0];
//        [aLayer setShadowOpacity:0.3];
//        [aLayer setShadowOffset:CGSizeMake(0, 0)];
//        aLayer.shadowPath = [UIBezierPath bezierPathWithRect:viewController.view.bounds].CGPath;
//        viewController.view.frame = CGRectMake(newFrame.size.width, newFrame.origin.y, newFrame.size.width, newFrame.size.height);
//
//        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//            oldView.frame = CGRectMake(oldFrame.origin.x - oldFrame.size.width / 2, oldFrame.origin.y, oldFrame.size.width, oldFrame.size.height);
//            viewController.view.frame = newFrame;
//        } completion:^(BOOL finished) {
//            completionBlock();
//            _isAnimating = NO;
//        }];
    }
    else{
        viewController.view.frame = newFrame;
        completionBlock();
    }
    _isNavigationWorking = NO;
}

- (void)pushViewController:(UTViewController *)viewController animated:(BOOL)animated{
    if([viewController.url isEqual:_topViewController.url] && [viewController.parameters isEqualToDictionary:_topViewController.parameters]){
        return;
    }
    if(animated){
        if(_isAnimating) return;
        _isAnimating = YES;
        
        if(viewController.isPopover){
            
            CATransition* transition = [CATransition animation];
            transition.duration = 0.3;
            transition.type = kCATransitionMoveIn;
            transition.subtype = kCATransitionFromRight;
            
            [self.view.layer
             addAnimation:transition forKey:kCATransition];
            
            _isAnimating = NO;
            [self _pushViewController:viewController animated:NO];
        }
        else{
            [self _pushViewController:viewController animated:animated];
        }
        
    }
    else{
        [self _pushViewController:viewController animated:animated];
    }
}

- (UTViewController *)_popViewControllerAnimated:(BOOL)animated{
    
    _isNavigationWorking = YES;
    if(animated){
        [CATransaction begin];
        [CATransaction lock];
    }
    UTViewController *poppingViewController = _topViewController;
    [_viewControllers removeLastObject];
    UTViewController *appearingViewController = [self _viewControllerFromVCOrUrl:[_viewControllers lastObject]];
    CGRect aFrame = [self frameForViewController:appearingViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
//    CGRect pFrame = poppingViewController.view.frame;
    [self addChildViewController:appearingViewController];
    [poppingViewController willMoveToParentViewController:nil];
    
    UIView *oldView = poppingViewController.view;
//    CALayer *layer = oldView.layer;
//    [layer setShadowColor:[UIColor blackColor].CGColor];
//    [layer setShadowOpacity:0.3];
//    [layer setShadowRadius:1.0];
//    [layer setShadowOffset:CGSizeMake(0, 0)];
//    layer.shadowPath = [UIBezierPath bezierPathWithRect:oldView.bounds].CGPath;
    
    
    [self.controlBar willTransitionFrom:poppingViewController to:appearingViewController transitionType:animated ? UTViewTransitionTypePop: UTViewTransitionTypeNone];
    self.topViewController = appearingViewController;
    [_contentView insertSubview:appearingViewController.view belowSubview:oldView];
    [self.controlBar transitionFrom:poppingViewController to:appearingViewController transitionType:animated ? UTViewTransitionTypePop: UTViewTransitionTypeNone];
    void (^completionBlock)() = ^{
        [oldView removeFromSuperview];
        [appearingViewController didMoveToParentViewController:self];
        [poppingViewController removeFromParentViewController];
        [self.controlBar didTransitionFrom:poppingViewController to:appearingViewController transitionType:animated ? UTViewTransitionTypePop: UTViewTransitionTypeNone];
    };
    if(animated){
//        appearingViewController.view.frame = CGRectMake(aFrame.origin.x - aFrame.size.width / 2.0f, aFrame.origin.y, aFrame.size.width, aFrame.size.height);
//        [UIView animateWithDuration:0.25 delay:(NSTimeInterval)0 options:(UIViewAnimationOptions)UIViewAnimationOptionCurveEaseOut animations:^{
//            oldView.frame = CGRectMake(pFrame.origin.x + aFrame.size.width, pFrame.origin.y, pFrame.size.width, pFrame.size.height);///CGRectOffset(pageFrame, pageWidth,0);
//            appearingViewController.view.frame = aFrame;
//        } completion:^(BOOL finished) {
//            completionBlock();
//            _isAnimating = NO;
//        }];
//        [CATransaction unlock];
//        [CATransaction commit];
        
        // Get the views.
        UIView * fromView = oldView;
        UIView * toView = poppingViewController.view;
        
        // Get the size of the view area.
        CGRect viewSize = fromView.frame;
        
        // Add the new view to the old view.
        [fromView.superview insertSubview:toView belowSubview:fromView];
        
        // Position the new view outside of the screen
        toView.frame = CGRectMake( -(viewSize.size.width/3) , viewSize.origin.y, viewSize.size.width, viewSize.size.height);
        
        UIView *darkenView = [[UIView alloc] initWithFrame:toView.frame];
        [darkenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.1]];
        [toView addSubview:darkenView];
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionAllowAnimatedContent animations:
         ^{
             // Animate the replacing of the views
             fromView.frame =CGRectMake(viewSize.size.width , viewSize.origin.y, viewSize.size.width, viewSize.size.height);
             toView.frame =CGRectMake(0, viewSize.origin.y, viewSize.size.width, viewSize.size.height);
             darkenView.frame = toView.frame;
             [darkenView setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0]];
         }
                         completion:^(BOOL finished)
        {
            if (finished)
            {
                // Remove the old view 
//                [fromView removeFromSuperview];
                [darkenView removeFromSuperview];
                completionBlock();
                _isAnimating = NO;
            }
        }];
    }
    else{
        appearingViewController.view.frame = aFrame;
        completionBlock();
        _isAnimating = NO;
    }
    appearingViewController.view.frame = [self frameForViewController:appearingViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
    _isNavigationWorking = NO;
    return poppingViewController;

}

- (UTViewController *)popViewControllerAnimated:(BOOL)animated{
    if([_viewControllers count] < 2)
        return nil;
    if(_topViewController.isPopover && animated){
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionReveal;
        transition.subtype = kCATransitionFromBottom;
        [self.view.layer
         addAnimation:transition forKey:kCATransition];
        return [self _popViewControllerAnimated:NO];
    }
    return [self _popViewControllerAnimated:animated];
    
}
- (NSArray *)popToViewController:(UTViewController *)viewController animated:(BOOL)animated{
    NSMutableArray *ret = [NSMutableArray array];
    
    UTViewController *popVC = self.topViewController;
    while(YES){
        UTViewController *toViewController = [self _viewControllerFromVCOrUrl:[_viewControllers objectAtIndex:[_viewControllers count]-2]];
        if (toViewController == viewController || ([viewController.url isEqual:toViewController.url] && [viewController.parameters isEqualToDictionary:toViewController.parameters])) {
            UTViewController *vc2 = [self popViewControllerAnimated:animated];
            [ret addObject:vc2];
            [self.controlBar willTransitionFrom:popVC to:vc2 transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar transitionFrom:popVC to:vc2 transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar didTransitionFrom:popVC to:vc2 transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            return ret;
        }
        UTViewController *vc = [self popViewControllerAnimated:NO];
        
        if(vc == nil || _topViewController == viewController){
            [self.controlBar willTransitionFrom:popVC to:viewController transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar transitionFrom:popVC to:viewController transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar didTransitionFrom:popVC to:viewController transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            return ret;
        }
        [ret addObject:vc];
        
    }
}
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated{
    NSMutableArray *ret = [NSMutableArray array];
    UTViewController *vc = nil;
    UTViewController *popVC = self.topViewController;
    while(YES){
        vc = [self popViewControllerAnimated:NO];
        if(vc == nil){
            [self.controlBar willTransitionFrom:popVC to:vc transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar transitionFrom:popVC to:vc transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];
            [self.controlBar didTransitionFrom:popVC to:vc transitionType:animated ? UTViewTransitionTypePop : UTViewTransitionTypeNone];

            return ret;
        }
        [ret addObject:vc];
        
    }
    
//    [_navigationBarController prepareForViewController:vc animate:NO];
//    [_navigationBarController popForViewController:vc animate:NO];
    return ret;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    for (NSUInteger index = 0 ; index < [_viewControllers count] - 1 ; index ++ ){
        UTViewController * viewController = [_viewControllers objectAtIndex:index];
        if(viewController != _topViewController && [viewController isKindOfClass:[UTViewController class]]){
            [_viewControllers setObject:[NSDictionary dictionaryWithObjectsAndKeys:viewController.url, @"url", viewController.parameters, @"params", nil] atIndexedSubscript:index];
        }
    }
}

- (UTViewController *)topViewController{
    return _topViewController;
}

- (NSUInteger)numberOfPushedViewControllers{
    return [_viewControllers count];
}

- (void)setTitle:(NSString *)title{
    _controlBar.title = title;
}


- (void)setControlBarHidden:(BOOL)controlBarHidden{
    self.controlBarHiddenRate = controlBarHidden ? 1.0f : 0.0f;
}

- (void)setControlBarHidden:(BOOL)controlBarHidden animated:(BOOL)animated{
    _controlBarHidden = controlBarHidden;
    if(animated){
        [UIView animateWithDuration:0.25 delay:(NSTimeInterval)0 options:(UIViewAnimationOptions)UIViewAnimationOptionCurveEaseOut animations:^{
            [self setControlBarHidden:controlBarHidden];
            _controlBar.alpha = 1.0f;
        } completion:^(BOOL finished) {
            if(controlBarHidden)
                _controlBar.alpha = 0.0f;
        }];
    }
    else{
        [self setControlBarHidden:controlBarHidden];
    }
}

- (void)setControlBarHiddenRate:(CGFloat)controlBarHiddenRate{
    if(controlBarHiddenRate > 1.0f)
        controlBarHiddenRate = 1.0f;
    else if(controlBarHiddenRate < 0.0f)
        controlBarHiddenRate = 0.0f;
    if(_controlBarHiddenRate != controlBarHiddenRate){
        _controlBarHiddenRate = controlBarHiddenRate;
        _contentView.frame = [self frameForContentView:[UIApplication sharedApplication].statusBarOrientation];
        _topViewController.view.frame = [self frameForViewController:_topViewController orientation:[UIApplication sharedApplication].statusBarOrientation];
        _controlBar.frame = [self frameForControlBar:[UIApplication sharedApplication].statusBarOrientation];
        if(_controlBarHiddenRate >= 0.5f){
            _controlBarHidden = YES;
            _controlBar.alpha = 0.0f;
        }
        else{
            _controlBarHidden = NO;
            _controlBar.alpha = 1.0f;
        }
    }
    else if(controlBarHiddenRate == 1.0f){
        _controlBar.alpha = 0.0f;
    }
}


- (void)receiveData:(id)data{
    [self.topViewController receiveData:data];
}

- (UTContainerViewController *)containerViewController{
    return self;
}


- (void)setHidesBackButton:(BOOL)hidesBackButton{
    self.controlBar.isBackButtonHidden = hidesBackButton;
}

//rotate 관련
-(NSUInteger)supportedInterfaceOrientations{
    return [self.topViewController supportedInterfaceOrientations];
}

- (BOOL)shouldAutorotate{
    return [self.topViewController shouldAutorotate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return [self.topViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}

- (void)setControlButtons:(NSArray *)items{
    [self.controlBar setControlButtons:items animated:NO];
}

- (void)setControlButtons:(NSArray *)items animated:(BOOL)animated{
    [self.controlBar setControlButtons:items animated:animated];
}

- (void)setTitleView:(UIView *)titleView{
    self.controlBar.titleView = titleView;
}

- (UIView *)titleView{
    return self.controlBar.titleView;
}
@end
