//
//  UTTableBasedViewController.m
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTTableViewController.h"
#import "UTTableView.h"
#import "UTTableCell.h"
#import "ODRefreshControl.h"
#import "UTNavigationCenter.h"
#import "UTUILibrary.h"
#import "UTArrayProvider.h"

@interface UTTableViewController ()

@property (nonatomic, strong) ODRefreshControl *refreshControl;
@end

@implementation UTTableViewController


- (id)initWithDataProvider:(UTDataProvider<UTTableProviderProtocol> *)dataProvider{
    self = [super initWithNibName:nil bundle:nil];
    if(self){
        self.dataProvider = dataProvider;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

- (void)setDataProvider:(UTDataProvider<UTTableProviderProtocol> *)dataProvider{
    _dataProvider = dataProvider;
    _dataProvider.delegate = self;
    
    if([_dataProvider canRefresh]){
        
        self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
        [self.refreshControl addTarget:self.dataProvider action:@selector(refreshWithControl:) forControlEvents:UIControlEventValueChanged];
        [self.refreshControl setEnabled:NO];
    }
}

- (void)loadDefaultView{
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.autoresizingMask = UTAUTORESIZINGFULL;
    self.tableView = [[UTTableView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.tableView.autoresizingMask = UTAUTORESIZINGFULL;
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSTimeInterval passed = [[NSDate date] timeIntervalSinceDate:self.dataProvider.lastUpdatedDate];
    if(passed > 300){
        [self.dataProvider refresh];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataProvider:(UTDataProvider *)provider changeState:(UTDataProviderState)toState from:(UTDataProviderState)fromState{
    [self.refreshControl setEnabled:YES];
    
    
    if(toState == UTDataProviderGettingMore){
         //indicator
        UIEdgeInsets inset = self.tableView.contentInset;
        inset.bottom = 13;
        self.tableView.contentInset = inset;
        [self.tableView.tableFooterView setHidden:NO];
    }
    
    if(toState == UTDataProviderLoaded){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self setBlock:NO];
        });
        if(fromState == UTDataProviderRefeshing){
            [self.refreshControl endRefreshing];
        }
    }
    else if(fromState == UTDataProviderInit ){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBlock:YES];
        });
    }
    if(toState == UTDataProviderNotAvailable){
        if(self.notAvailableView){
            self.notAvailableView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height);
            [self.tableView addSubview:self.notAvailableView];
        }
        [self setAvaliable:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBlock:NO];
        });
    }
    else if(toState == UTDataProviderFailed){
        if(self.failedView){
            self.failedView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height);
            [self.tableView addSubview:self.failedView];
        }
        [self setAvaliable:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBlock:NO];
        });
    }
    else if(toState != UTDataProviderRefeshing){
        [self.failedView removeFromSuperview];
        [self.notAvailableView removeFromSuperview];
        [self setAvaliable:YES];
    }
}

- (void)setAvaliable:(BOOL)isAvailable{
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [dataProvider titleForSection:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [dataProvider numberOfObjectForSection:section];
}

- (NSString *)tableCellName:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    return [self tableCellName:tableView];
}

- (NSString *)tableCellName:(UITableView *)tableView{
    return @"UTTableCell";
}

- (void)dealloc{
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
}
- (UTDataProvider<UTTableProviderProtocol> *)dataProviderForTable:(UIScrollView *)tableView{
    return self.dataProvider;
}

- (void)scrollViewDidScroll: (UIScrollView*)scroll {
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:scroll];
    if([dataProvider canGetMore]){
        // UITableView only moves in one direction, y axis
        if([self useReverseGetMore]){
            NSInteger currentOffset = scroll.contentOffset.y;
            NSInteger minOffset = -scroll.contentInset.top;
            
            // Change 10.0 to adjust the distance from bottom
            if (currentOffset - minOffset >= -10.0) {
                [dataProvider getMore];
            }
        }
        else{
            NSInteger currentOffset = scroll.contentOffset.y;
            NSInteger maximumOffset = scroll.contentSize.height - scroll.frame.size.height;
            
            // Change 10.0 to adjust the distance from bottom
            if (maximumOffset - currentOffset <= 10.0) {
                [dataProvider getMore];
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
        UTTableCell *cell = [NSClassFromString([self tableCellName:tableView atIndexPath:indexPath]) getReusedCellFromTableView:tableView withReusedentifier:[self tableCellName:tableView atIndexPath:indexPath] indexPath:indexPath];
        [cell applyData:[dataProvider objectAtIndexPath:indexPath]];
        return cell;
    }
    @catch (NSException *exception) {
        [self.tableView reloadData];
        return [[UITableViewCell alloc] init];
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [dataProvider sectionIndexTitles];
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [dataProvider sectionForSectionIndexTitle:title atIndex:index];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [dataProvider numberOfSection];
}
- (UIScrollView *)hiddenableNavigationTargetScrollView{
    return self.tableView;
}

- (BOOL)useHiddenableNavigation{
    return NO;
}

- (BOOL)useReverseGetMore{
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    UTDataProvider<UTTableProviderProtocol> *dataProvider = [self dataProviderForTable:tableView];
    return [NSClassFromString([self tableCellName:tableView atIndexPath:indexPath]) cellHeightWithData:[dataProvider objectAtIndexPath:indexPath] bounds:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)];
}
@end
