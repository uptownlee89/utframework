//
//  UTRestCollectionProvider.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 21..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTArrayProvider.h"
#import "UTRestResource.h"
#import "UTTableProvider.h"

@interface UTRestCollectionProvider : UTArrayProvider<UTTableProviderProtocol>

- (id)initWithRestResourceClass:(Class)resourceCls;
- (id)initWithRestResourceClass:(Class)resourceCls parameters:(NSDictionary *)parameters;

- (id)initWithRestResourceClass:(Class)resourceCls URI:(NSString *)URI;
@end
