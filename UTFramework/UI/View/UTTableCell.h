//
//  UTTableCell.h
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTTableView.h"

@interface UTTableCell : UITableViewCell{
@protected
    id _data;
}
@property (nonatomic, readonly) NSIndexPath *currentIndexPath;
@property (nonatomic, readonly) UITableView *tableView;


+ (instancetype)getReusedCellFromTableView:(UITableView *)tableView withReusedentifier:(NSString*)reuseIdentifier indexPath:(NSIndexPath*)indexPath;
- (void)applyData:(id)data;
+ (CGFloat)cellHeightWithData:(id)data bounds:(CGSize)bounds;
+ (UTTableCell*)getReusedCellFromName:(NSString*)name tableView:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath;
@end
