//
//  UTNavigationBar.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 10. 7..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTNavigationBar.h"
#import "UTApplication.h"
#import "UTControlBar+Protected.h"

@implementation UTNavigationBar{
    UTControlButton * _backButton;
    UTControlButton * _closeButton;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
 */
- (void)_construct{
    [super _construct];
    self.backgroundColor = [UIColor blackColor];
}


@end
