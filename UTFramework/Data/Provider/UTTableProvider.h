//
//  UTTableProvider.h
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 15..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTDataProvider.h"

@protocol UTTableProviderProtocol <NSObject>
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)sectionIndexTitles;
- (NSInteger)sectionForSectionIndexTitle:(NSString *)title atIndex:(NSUInteger)index;
- (NSUInteger)numberOfSection;
- (NSString *)titleForSection:(NSUInteger)section;
- (NSUInteger)numberOfObjectForSection:(NSUInteger)section;
@end

@class UTArrayProvider;

@interface UTTableProvider : UTDataProvider<UTTableProviderProtocol>

- (id)initWithArray:(NSArray *)array;
- (id)initWithContentsOfFile:(NSString *)file;
- (id)initWithContentsOfFile:(NSString *)file inBundle:(NSBundle *)bundle;
@end
