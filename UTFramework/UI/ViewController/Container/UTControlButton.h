//
//  UTControlButton.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 26..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    UTControlButtonPositionLeft,
    UTControlButtonPositionRight,
} UTControlButtonPosition;

typedef enum {
    UTControlButtonTypeText,
    UTControlButtonTypeImage,
} UTControlButtonType;

@class UTControlButton;
typedef void (^UTControlButtonCallback)(UTControlButton *control);
typedef void (^UTControlButtonSizeCallback)(UTControlButton *button, CGSize barSize);

@interface UTControlButton : UIButton
@property (nonatomic, readonly) NSString *identifier;
@property (nonatomic, readonly) UTControlButtonPosition position;
@property (nonatomic, readonly) UTControlButtonType type;

- (id)initWithType:(UTControlButtonType)type position:(UTControlButtonPosition)position;
- (id)initWithImage:(UIImage *)image position:(UTControlButtonPosition)position;
- (id)initWithText:(NSString *)text position:(UTControlButtonPosition)position;
- (id)initWithIdentifier:(NSString *)identifier;
- (void)applyControlBarSize:(CGSize)size;
- (void)setSizeCallback:(UTControlButtonSizeCallback)callback;
- (void)addCallback:(UTControlButtonCallback)callback forControlEvents:(UIControlEvents)controlEvents;
@end
