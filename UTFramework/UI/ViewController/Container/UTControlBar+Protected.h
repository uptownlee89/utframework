//
//  UTControlBar+Protected.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 10. 7..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTControlBar.h"

@interface UTControlBar (Protected)
@property (nonatomic, assign) UTContainerViewController *containerVC;
- (void)_construct;
@end
