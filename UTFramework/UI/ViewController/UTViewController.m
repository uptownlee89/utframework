//
//  UTViewController.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTViewController.h"
#import "UTDefine.h"
#import "UTUtils.h"
#import "UTNavigationCenter.h"
#import "UTRootViewController.h"
#ifdef USE_UTContainer
#import "UTContainerViewController.h"
#import "UIImage+ImageCache.h"
#endif

@interface UTViewController (){
    NSMutableArray *_observers;
    UIView *_blockView;
    BOOL _viewIsAppearing;
    BOOL _viewIsDisappearing;
    BOOL _viewIsShow;
#ifdef USE_UTContainer
    NSMutableArray *_controlButtons;
    UTControlButton *_backButton;
#endif
}

@property (nonatomic, strong) UIPopoverController *popOverViewController;
@end

@implementation UTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    NSString *className = nil;
    if(nibNameOrNil == nil){
        className = NSStringFromClass([self class]);
    }
    else{
        className = nibNameOrNil;
    }
    NSMutableArray *possiblePostfix = [NSMutableArray array];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [possiblePostfix addObject:@"iOS7"];
    }
    else{
        [possiblePostfix addObject:@"iOS6"];
    }
    if(IS_IPHONE){
        if(IS_IPHONE4INCH){
            [possiblePostfix addObject:@"568h"];
        }
        [possiblePostfix addObject:@"iPhone"];
    }
    else{
        [possiblePostfix addObject:@"iPad"];
    }
    NSString *nibPath = [[NSBundle mainBundle] pathForResource:className ofType:@"nib"];
    if(!nibPath){
        NSString *nibName = nil;
        NSMutableArray *possibleNames = [NSMutableArray arrayWithObject:className];
        for (NSString *postFix in possiblePostfix){
            NSMutableArray *addings = [NSMutableArray array];
            for(NSString *prePossibleName in possibleNames){
                NSString *newPossibleNames = [NSString stringWithFormat:@"%@_%@",prePossibleName, postFix];
                nibPath = [[NSBundle mainBundle] pathForResource:newPossibleNames ofType:@"nib"];
                nibName = newPossibleNames;
                if(nibPath)
                    break;
                [addings addObject:newPossibleNames];
            }
            if(nibPath)
                break;
            [possibleNames addObject:addings];
            NSString *newPossibleNames = [NSString stringWithFormat:@"%@_%@",className, postFix];
            nibPath = [[NSBundle mainBundle] pathForResource:newPossibleNames ofType:@"nib"];
            nibName = newPossibleNames;
            if(nibPath)
                break;
            [possibleNames addObject:newPossibleNames];
        }
        if(nibPath)
            nibNameOrNil = nibName;
    }
    else{
        nibNameOrNil = className;
    }
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        _url = [NSURL URLWithString:@"___mf://"];
    }
    return self;
}

- (id)initWithURL:(NSURL *)url{
    return [self initWithURL:url parameters:[NSMutableDictionary dictionary]];
}


- (id)initWithURL:(NSURL *)url parameters:(NSDictionary *)params{
    self = [self initWithNibName:nil bundle:nil];
    if(self){
        _url = url;
        _parameters = params;
    }
    return self;
    
}

- (void)viewDidLoad{
    _viewIsShow = NO;
    _viewIsDisappearing = NO;
    _viewIsAppearing = NO;
#ifdef USE_UTContainer
    _controlButtons = [NSMutableArray array];
#endif
    [super viewDidLoad];
    _observers = [NSMutableArray array];
    if(self.parameters && [self.parameters objectForKey:@"title"]){
        self.title = [UTUtils decodeFromPercentEscapeString:[self.parameters objectForKey:@"title"]];
    }
    else if(self.parameters && [self.parameters objectForKey:@"title_string_key"]){
        self.title = _([UTUtils decodeFromPercentEscapeString:[self.parameters objectForKey:@"title_string_key"]]);
    }
    
}


- (void)viewWillAppear:(BOOL)animated{
    _viewIsShow = YES;
    _viewIsAppearing = YES;
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _viewIsAppearing = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    _viewIsDisappearing = YES;
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _viewIsDisappearing = NO;
    _viewIsShow = NO;
    
}
- (void)dealloc{
    //NSLog(@"%@ dealloc", [self class]);
    for (id observer in _observers){
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }
    _observers = nil;
    _blockView = nil;
}

- (void)setBlock:(BOOL)block showIndicator:(BOOL)showIndicator text:(NSString *)text toView:(UIView *)view{
    if(view == nil){
        view = self.view;
    }
    if (block){
        if(_blockView == nil){
            _blockView = [[UIView alloc] initWithFrame:view.bounds];
            _blockView.autoresizingMask = UT_AUTORESIZINGFULL;
            [_blockView setBackgroundColor:[UIColor blackColor]];
            [_blockView setAlpha:0.7];
            if(showIndicator){
                UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [indicator startAnimating];
                indicator.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin);
                indicator.center = _blockView.center;
                [_blockView addSubview:indicator];
            }
            else if(text){
                UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
                [label setBackgroundColor:[UIColor clearColor]];
                [label setTextColor:[UIColor whiteColor]];
                [label setTextAlignment:NSTextAlignmentCenter];
                [_blockView addSubview:label];
                [label setText:text];
            }
            [view addSubview:_blockView];
        }
        [_blockView setHidden:NO];
    }
    else{
        [_blockView setHidden:YES];
    }
}

- (void)setBlock:(BOOL)block showIndicator:(BOOL)showIndicator toView:(UIView *)view{
    [self setBlock:block showIndicator:showIndicator text:nil toView:view];
}

- (void)setBlock:(BOOL)block{
    [self setBlock:block showIndicator:YES toView:self.view];
}

- (UIView *)blockView{
    return _blockView;
}

- (id)addObserverForName:(NSString *)name object:(id)obj queue:(NSOperationQueue *)queue usingBlock:(void (^)(NSNotification *))block{
    //if(queue == nil)
    //    queue = [NSOperationQueue mainQueue];
    id observer = [[NSNotificationCenter defaultCenter] addObserverForName:name object:obj queue:queue usingBlock:block];
    [_observers addObject:observer];
    return observer;
}

- (void)receiveData:(id)data{
}

- (void)checkToSouldPopViewController:(void (^)(BOOL))callback{
    callback(YES);
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:viewControllerToPresent];
        
        popover.delegate = self;
        viewControllerToPresent.view.frame = [UIScreen mainScreen].bounds;
        self.popOverViewController = popover;
        [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2, 10, 10) inView:[UTNavigationCenter defaultCenter].rootViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:flag];
        
        if(completion)
            completion();
    } else {
        [super presentViewController:viewControllerToPresent animated:flag completion:completion];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    NSUInteger ori = [self supportedInterfaceOrientations];
    if((toInterfaceOrientation == UIInterfaceOrientationPortrait)  && (ori | UIInterfaceOrientationMaskPortrait)){
        return YES;
    }
    if((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)  && (ori | UIInterfaceOrientationMaskLandscapeLeft)){
        return YES;
    }
    if((toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)  && (ori | UIInterfaceOrientationMaskLandscapeRight)){
        return YES;
    }
    if((toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)  && (ori | UIInterfaceOrientationMaskPortraitUpsideDown)){
        return YES;
    }
    return NO;
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(self.blockView){
        self.blockView.frame = self.view.bounds;
    }
}

#ifdef USE_UTContainer

- (void)_removeControlButtonForIdentifier:(NSString *)identifier{
    id removing = nil;
    for (UTControlButton *button in _controlButtons){
        if([button.identifier isEqualToString:identifier]){
            removing = button;
            break;
        }
    }
    [_controlButtons removeObject:removing];
}

- (void)removeControlButtonForIdentifier:(NSString *)identifier{
    [self _removeControlButtonForIdentifier:identifier];
    if(_viewIsShow && !_viewIsAppearing)
        [self.containerViewController setControlButtons:_controlButtons];
}

- (void)removeControlButton:(UTControlButton *)controlButton{
    if(!controlButton) return;
    id removing = nil;
    for (UTControlButton *button in _controlButtons){
        if(button == controlButton || [button.identifier isEqualToString:controlButton.identifier]){
            removing = button;
            break;
        }
    }
    [_controlButtons removeObject:removing];
    if(_viewIsShow && !_viewIsAppearing)
        [self.containerViewController setControlButtons:_controlButtons];
}

- (void)addControlButton:(UTControlButton *)controlButton{
    if(!controlButton) return;
    if(controlButton.identifier){
        [self _removeControlButtonForIdentifier:controlButton.identifier];
    }
    [_controlButtons addObject:controlButton];
    if(_viewIsShow && !_viewIsAppearing)
        [self.containerViewController setControlButtons:_controlButtons];
}


- (UTControlButton *)controlButtonForIdentifier:(NSString *)identifier{
    id ret = nil;
    for (UTControlButton *button in _controlButtons){
        if([button.identifier isEqualToString:@"identifier"]){
            ret = button;
        }
    }
    return ret;
}

- (UTContainerViewController *)containerViewController{
    if([self.parentViewController isKindOfClass:[UTContainerViewController class]]){
        return (UTContainerViewController *)self.parentViewController;
    }
    return nil;
    
}

- (NSArray *)controlButtons{
    return _controlButtons;
}

- (void)setTitleView:(UIView *)titleView{
    _titleView = titleView;
    if(self.containerViewController.topViewController == self)
        self.containerViewController.titleView = titleView;
}

- (BOOL)isPopover{
    return NO;
}

- (UTControlButton *)backButton{
    if(_backButton){
        return _backButton;
    }
    if(self.isPopover){
        _backButton = [[UTControlButton alloc] initWithImage:[UIImage imageCached:@"btn_close.png" inBundle:[NSBundle mainBundle]] position:UTControlButtonPositionRight];
        //        __weak UIViewController<UTViewController> *wself = self;
        [_backButton addCallback:^(UTControlButton *control) {
            //            UIViewController<UTViewController> *sself = wself;
            [[UTNavigationCenter defaultCenter] popViewControllerWithParameters:nil];
            //            [sself.mfNavigationController popViewControllerAnimated:YES];
        } forControlEvents:UIControlEventTouchUpInside];
        _backButton.frame = CGRectMake(0, 0, 44, 44);
        return _backButton;
    }
    else{
        _backButton = [[UTControlButton alloc] initWithImage:[UIImage imageCached:@"btn_back.png" inBundle:[NSBundle mainBundle]] position:UTControlButtonPositionLeft];
        //        __weak UIViewController<UTViewController> *wself = self;
        [_backButton addCallback:^(UTControlButton *control) {
            //            UIViewController<UTViewController> *sself = wself;
            [[UTNavigationCenter defaultCenter] popViewControllerWithParameters:nil];
            //            [sself.mfNavigationController popViewControllerAnimated:YES];
        } forControlEvents:UIControlEventTouchUpInside];
        _backButton.frame = CGRectMake(0, 0, 44, 44);
        return _backButton;
    }
}

#endif

@end
