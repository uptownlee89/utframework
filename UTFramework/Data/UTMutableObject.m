//
//  UTMutableObject.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTMutableObject.h"
#import "UTUtils.h"
#import <objc/runtime.h>

typedef enum{
    UTSelectorTypeGet,
    UTSelectorTypeSet,
    UTSelectorNone
    
} UTSelectorSelectType;

@interface UTMutableArray : NSMutableArray

- (id)initWrappingArray:(NSArray *)otherArray;
- (id)objectifyAtIndex:(NSUInteger)index;
- (void)objectifyAll;

@end



@interface UTMutableObject ()

- (id)initWrappingDictionary:(NSDictionary *)otherDictionary;
- (void)objectifyAll;
- (id)objectifyAtKey:(id)key;
- (void)_removeNullFromObject:(id)object;
+ (instancetype)objectWrappingObject:(id)originalObject;
+ (UTSelectorSelectType)inferredImplTypeForSelector:(SEL)sel;
+ (BOOL)isProtocolImplementationInferable:(Protocol *)protocol checkObjectAdoption:(BOOL)checkAdoption;

@end
@implementation UTMutableObject{
    NSMutableDictionary *_dictionary;
    NSString *_file;
    NSMutableSet *_opeatorClses;
}

- (id)objectifyAtKey:(id)key {
    id object = [_dictionary objectForKey:key];
    id possibleReplacement = [UTMutableObject objectWrappingObject:object];
    if (object != possibleReplacement) {
        [_dictionary setObject:possibleReplacement forKey:key];
        object = possibleReplacement;
    }
    if(!object && _opeatorClses){
        NSMutableString *selectorName = [NSMutableString stringWithString:key];
        NSString *firstChar = [[selectorName substringWithRange:NSMakeRange(0,1)] uppercaseString];
        [selectorName replaceCharactersInRange:NSMakeRange(0, 1) withString:firstChar];
        selectorName = [NSMutableString stringWithFormat:@"get%@:",selectorName];
        
        SEL sel = NSSelectorFromString(selectorName);
        for(NSString *clsName in _opeatorClses){
            //NSSelectorFromString(selector);
            __unsafe_unretained id result;
            Class class = NSClassFromString(clsName);
            NSMethodSignature *signature =  [class methodSignatureForSelector:sel];
            if(signature){
                NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
                invocation.selector = sel;
                invocation.target = class;
                [invocation setArgument:&self atIndex:2];
                [invocation invoke];
                [invocation getReturnValue:&result];
                return [UTMutableObject objectWrappingObject:result];
            }
        }
    }
    return object;
}

- (void)addOperator:(NSString *)opeartorCls{
    if(!_opeatorClses)
        _opeatorClses = [[NSMutableSet set] retain];
    if(![_opeatorClses containsObject:opeartorCls])
        [_opeatorClses addObject:opeartorCls];
}
- (void)removeOperator:(NSString *)opeartorCls{
    if([_opeatorClses containsObject:opeartorCls])
        [_opeatorClses removeObject:opeartorCls];
}
- (void)objectifyAll {
    NSArray *keys = [_dictionary allKeys];
    for (NSString *key in keys) {
        [self objectifyAtKey:key];
    }
}
- (void)save{
    [self save:_file];
}
- (void)load{
    [self load:_file];
}

- (void)_removeNullFromObject:(id)object{
    if([object isKindOfClass:[NSDictionary class]]){
        NSMutableArray *del = [NSMutableArray array];
        NSMutableArray *others = [NSMutableArray array];
        for (NSString *key in [object allKeys]){
            if([[object objectForKey:key ] isKindOfClass:[NSNull class]]){
                [del addObject:key];
            }
            else{
                [others addObject:key];
            }
        }
        for (id key in others){
            if ([[object objectForKey:key ] isKindOfClass:[NSDictionary class]]){
                [object setObject:[NSMutableDictionary dictionaryWithDictionary:[object objectForKey:key ]] forKey:key];
            }
            else if ([[object objectForKey:key ] isKindOfClass:[NSArray class]]){
                [object setObject:[NSMutableArray arrayWithArray:[object objectForKey:key ]] forKey:key];
            }
            [self _removeNullFromObject:[object objectForKey:key ]];
        }
        for (id key in del){
            [object removeObjectForKey:key];
        }
    }
    else if([object isKindOfClass:[NSArray class]]){
        int i = 0;
        NSMutableArray *indexes = [NSMutableArray array];
        for (id subObject in object){
            if([subObject isKindOfClass:[NSNull class]]){
                [object removeObject:subObject];
            }
            
            else{
                [indexes addObject:@(i)];
            }
            i ++;
        }
        for (NSNumber *ii in indexes){
            int i = ii.intValue;
            id subObject = [object objectAtIndex:i];
            if ([subObject isKindOfClass:[NSDictionary class]]){
                [object setObject:[NSMutableDictionary dictionaryWithDictionary:subObject] atIndex:i];
            }
            else if ([subObject isKindOfClass:[NSArray class]]){
                [object setObject:[NSMutableArray arrayWithArray:subObject] atIndex:i];
            }
            [self _removeNullFromObject:[object objectAtIndex:i]];
        }
    }
}
- (void)save:(NSString *)filename{
    NSString *path = [[UTUtils applicationHiddenDocumentsDirectory] stringByAppendingPathComponent:filename];
    [self _removeNullFromObject:_dictionary];
    [_dictionary writeToFile:path atomically:YES];
    if(_file)
        [_file release];
    _file = [filename retain];
}
- (void)load:(NSString *)filename{
    NSString *path = [[UTUtils applicationHiddenDocumentsDirectory] stringByAppendingPathComponent:filename];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    if(dict){
        if(_dictionary)
            [_dictionary release];
        _dictionary = [dict retain];
    }
}

- (void)connectWithFile:(NSString*)filename{
    if(_file)
       [_file release];
    _file = [filename retain];
    [self load];
    
}



#pragma mark -

#pragma mark NSDictionary and NSMutableDictionary overrides

- (NSUInteger)count {
    return _dictionary.count;
}

- (id)objectForKey:(id)key {
    return [self objectifyAtKey:key];
}

- (NSEnumerator *)keyEnumerator {
    [self objectifyAll];
    return _dictionary.keyEnumerator;
}

- (void)setObject:(id)object forKey:(id)key {
    [self realSetObject:object forKey:key];
}

- (void)realSetObject:(id)object forKey:(id)key {
     [self willChangeValueForKey:key]; 
    if(object != nil)
        [_dictionary setObject:object forKey:key];
    else{
        [_dictionary removeObjectForKey:key];
    }
    [self didChangeValueForKey:key];
}

- (void)removeObjectForKey:(id)key {
    return [_dictionary removeObjectForKey:key];
}

- (id)filteredObject:(NSArray *)filter{
    id model = [UTMutableObject copiedObjectFromDictionary:self];
    for(NSString *key in [model allKeys]){
        if([filter indexOfObject:key] == NSNotFound){
            [model removeObjectForKey:key];
        }
    }
    return model;
}


#pragma mark Lifecycle

- (id)initWrappingDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if ([dictionary isKindOfClass:[UTMutableObject class]]) {
            [dictionary retain];
            [self release];
            return (UTMutableObject*)dictionary;
        } else {
            _dictionary = [[NSMutableDictionary dictionaryWithDictionary:dictionary] retain];
        }
    }
    return self;
}

- (void)dealloc {
    [_dictionary release];
    if(_opeatorClses){
        [_opeatorClses release];
        _opeatorClses = nil;
    }
    if(_file){
        [_file release];
        _file = nil;
    }
    [super dealloc];
}

- (void)addEntriesFromDictionary:(NSDictionary *)dictionary{
    for (NSString *key in dictionary) {
        id originalObj = [self objectForKey:key];
        id addingObj = [dictionary objectForKey:key];
        
        if (originalObj != nil){
            if([originalObj isKindOfClass:[NSDictionary class]] && [addingObj isKindOfClass:[NSDictionary class]]){
                [originalObj addEntriesFromDictionary:addingObj];
            }
            else if([originalObj isKindOfClass:[NSArray class]] && [addingObj isKindOfClass:[NSArray class]]){
                [originalObj addObjectsFromArray:addingObj];
            }
            else{
                [self setObject:addingObj forKey:key];
            }
        }
        else{
            [self setObject:addingObj forKey:key];
        }
    }
}

+ (instancetype)objectWrappingObject:(id)originalObject {
    id result = originalObject;
    if ([originalObject isKindOfClass:[NSDictionary class]]) {
        result = [[[self alloc] initWrappingDictionary:originalObject] autorelease];
    } else if ([originalObject isKindOfClass:[NSArray class]]) {
        result = [[[UTMutableArray alloc] initWrappingArray:originalObject] autorelease];
    }
    // return our object
    return result;
}

+ (instancetype)object {
    return [self objectFromDictionary:[NSMutableDictionary dictionary]];
}

+ (instancetype)objectFromDictionary:(NSDictionary*)jsonDictionary {
    return [self objectWrappingObject:jsonDictionary];
}

+ (instancetype)copiedObjectFromDictionary:(NSDictionary*)jsonDictionary {
    return [self objectWrappingObject:[[jsonDictionary copy] autorelease]];
}

+ (BOOL)isObjectID:(id<UTMutableObject>)anObject sameAs:(id<UTMutableObject>)anotherObject {
    if (anObject != nil &&
        anObject == anotherObject) {
        return YES;
    }
    id anID = [anObject objectForKey:@"id"];
    id anotherID = [anotherObject objectForKey:@"id"];
    if ([anID isKindOfClass:[NSString class]] &&
        [anotherID isKindOfClass:[NSString class]]) {
        return [(NSString*)anID isEqualToString:anotherID];
    }
    return NO;
}

+ (UTSelectorSelectType)inferredImplTypeForSelector:(SEL)sel {
    NSString *selectorName = NSStringFromSelector(sel);
    
    unsigned long parameterCount = [[selectorName componentsSeparatedByString:@":"] count] - 1;
    if (parameterCount == 0) {
        return UTSelectorTypeGet;
    }
    
    else if (parameterCount == 1 &&                   // ... we have the correct arity
             [selectorName hasPrefix:@"set"] &&       // ... we have the proper prefix
             selectorName.length > 4) {               // ... there are characters other than "set" & ":"
        return UTSelectorTypeSet;
    }
    
    return UTSelectorNone;
}


+ (BOOL)isProtocolImplementationInferable:(Protocol*)protocol checkObjectAdoption:(BOOL)checkAdoption {
    // first handle base protocol questions
    if (checkAdoption && !protocol_conformsToProtocol(protocol, @protocol(UTMutableObject))) {
        return NO;
    }
    
    if ([protocol isEqual:@protocol(UTMutableObject)]) {
        return YES; // by definition
    }
    
    unsigned int count = 0;
    struct objc_method_description *methods = nil;
    
    // then confirm that all methods are required
    methods = protocol_copyMethodDescriptionList(protocol,
                                                 NO,        // optional
                                                 YES,       // instance
                                                 &count);
    if (methods) {
        free(methods);
        return NO;
    }
    
    @try {
        // fetch methods of the protocol and confirm that each can be implemented automatically
        methods = protocol_copyMethodDescriptionList(protocol,
                                                     YES,   // required
                                                     YES,   // instance
                                                     &count);
        for (int index = 0; index < count; index++) {
            if ([UTMutableObject inferredImplTypeForSelector:methods[index].name] == UTSelectorNone) {
                // we have a bad actor, short circuit
                return NO;
            }
        }
    } @finally {
        if (methods) {
            free(methods);
        }
    }
    
    // fetch adopted protocols
    Protocol **adopted = nil;
    @try {
        adopted = protocol_copyProtocolList(protocol, &count);
        for (int index = 0; index < count; index++) {
            // here we go again...
            if (![UTMutableObject isProtocolImplementationInferable:adopted[index]
                                  checkObjectAdoption:NO]) {
                return NO;
            }
        }
    } @finally {
        if (adopted) {
            free(adopted);
        }
    }
    
    // protocol ran the gauntlet
    return YES;
}



#pragma mark -
#pragma mark NSObject overrides

// make the respondsToSelector method do the right thing for the selectors we handle
- (BOOL)respondsToSelector:(SEL)sel
{
    return  [super respondsToSelector:sel] ||
    ([UTMutableObject inferredImplTypeForSelector:sel] != UTSelectorNone);
}

- (BOOL)conformsToProtocol:(Protocol *)protocol {
    return  [super conformsToProtocol:protocol] ||
    ([UTMutableObject isProtocolImplementationInferable:protocol
                      checkObjectAdoption:YES]);
}

// returns the signature for the method that we will actually invoke
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    SEL alternateSelector = sel;
    
    // if we should forward, to where?
    switch ([UTMutableObject inferredImplTypeForSelector:sel]) {
        case UTSelectorTypeGet:
            alternateSelector = @selector(objectForKey:);
            break;
        case UTSelectorTypeSet:
            alternateSelector = @selector(setObject:forKey:);
            break;
        case UTSelectorNone:
        default:
            break;
    }
    
    return [super methodSignatureForSelector:alternateSelector];
}

// forwards otherwise missing selectors that match the FBobject convention
- (void)forwardInvocation:(NSInvocation *)invocation {
    // if we should forward, to where?
    switch ([UTMutableObject inferredImplTypeForSelector:[invocation selector]]) {
        case UTSelectorTypeGet: {
            // property getter impl uses the selector name as an argument...
            NSString *propertyName = NSStringFromSelector([invocation selector]);
            [invocation setArgument:&propertyName atIndex:2];
            //... to the replacement method objectForKey:
            invocation.selector = @selector(objectForKey:);
            [invocation invokeWithTarget:self];
            break;
        }
        case UTSelectorTypeSet: {
            // property setter impl uses the selector name as an argument...
            NSMutableString *propertyName = [NSMutableString stringWithString:NSStringFromSelector([invocation selector])];
            // remove 'set' and trailing ':', and lowercase the new first character
            [propertyName deleteCharactersInRange:NSMakeRange(0, 3)];                       // "set"
            [propertyName deleteCharactersInRange:NSMakeRange(propertyName.length - 1, 1)]; // ":"
            
            NSString *firstChar = [[propertyName substringWithRange:NSMakeRange(0,1)] lowercaseString];
            [propertyName replaceCharactersInRange:NSMakeRange(0, 1) withString:firstChar];
            // the object argument is already in the right place (2), but we need to set the key argument
            [invocation setArgument:&propertyName atIndex:3];
            // and replace the missing method with setObject:forKey:
            invocation.selector = @selector(setObject:forKey:);
            [invocation invokeWithTarget:self];
            break;
        }
        case UTSelectorNone:
        default:
            [super forwardInvocation:invocation];
            return;
    }
}


@end
@implementation UTMutableArray {
    NSMutableArray *_jsonArray;
}

- (id)initWrappingArray:(NSArray *)jsonArray {
    self = [super init];
    if (self) {
        if ([jsonArray isKindOfClass:[UTMutableArray class]]) {
            // in this case, we prefer to return the original object,
            // rather than allocate a wrapper
            
            // we are about to return this, better make it the caller's
            [jsonArray retain];
            
            // we don't need self after all
            [self release];
            
            // no wrapper needed, returning the object that was provided
            return (UTMutableArray*)jsonArray;
        } else {
            _jsonArray = [[NSMutableArray arrayWithArray:jsonArray] retain];
        }
    }
    return self;
}

- (void)dealloc {
    [_jsonArray release];
    [super dealloc];
}

- (NSUInteger)count {
    return _jsonArray.count;
}

- (id)objectifyAtIndex:(NSUInteger)index {
    if(index == NSUIntegerMax){
        return nil;
    }
    id object = [_jsonArray objectAtIndex:index];
    // make certain it is FBObjectGraph-ified
    id possibleReplacement = [UTMutableObject objectWrappingObject:object];
    if (object != possibleReplacement) {
        // and if not-yet, replace the original with the wrapped object
        [_jsonArray replaceObjectAtIndex:index withObject:possibleReplacement];
        object = possibleReplacement;
    }
    return object;
}

- (void)objectifyAll {
    unsigned long count = [_jsonArray count];
    for (int i = 0; i < count; ++i) {
        [self objectifyAtIndex:i];
    }
}

- (id)objectAtIndex:(NSUInteger)index {
    return [self objectifyAtIndex:index];
}

- (id)lastObject{
    return [self objectifyAtIndex:(_jsonArray.count - 1)];
    
}

- (NSEnumerator *)objectEnumerator {
    [self objectifyAll];
    return _jsonArray.objectEnumerator;
}

- (NSEnumerator *)reverseObjectEnumerator {
    [self objectifyAll];
    return _jsonArray.reverseObjectEnumerator;
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index {
    [_jsonArray insertObject:object atIndex:index];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [_jsonArray removeObjectAtIndex:index];
}

- (void)addObject:(id)object {
    [_jsonArray addObject:object];
}

- (void)removeLastObject {
    [_jsonArray removeLastObject];
}

- (void)exchangeObjectAtIndex:(NSUInteger)idx1 withObjectAtIndex:(NSUInteger)idx2{
    
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)object {
    [_jsonArray replaceObjectAtIndex:index withObject:object];
}



@end

