//
//  NSNumber+humanize.m
//  MyMusicTaste
//
//  Created by 주영 이 on 13. 3. 13..
//  Copyright (c) 2013년 JJS Media. All rights reserved.
//

#import "NSNumber+UTFramework.h"
#import "UTApplication.h"
#import "UTDefine.h"


@implementation NSNumber (UTFramework)


- (NSString *)getLocalizedTimeString {
	if ([self floatValue] == 0) {
		return ([NSString stringWithFormat:@""]);
	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970] - ([self floatValue] / 1000);
	
	NSString *result;
	if (timeInterval < 70) {
		result = _(@"overall_now");
    }
	else if (timeInterval < 3600) {
		result = [NSString stringWithFormat:_(@"overall_minute_ago"), (int)(timeInterval / 60)];
	}
	else if (timeInterval < 86400) {
		result = [NSString stringWithFormat:_(@"overall_hour_ago"), (int)(timeInterval / 3600)];
	}
	else {
		NSString *strLocale = [[[NSLocale currentLocale] localeIdentifier] substringToIndex:2];
		
		if ([strLocale isEqualToString:@"en"]) {
			[dateFormatter setDateFormat:@"MMM d"];
		}
		else {
			[dateFormatter setDateFormat:@"yyyy.M.d"];
		}
		
        long timezoneoffset = ([[NSTimeZone systemTimeZone] secondsFromGMT] / 3600);
		result = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[self floatValue] + timezoneoffset]];
	}
	
	
	return result;
}


- (NSString *)getDate {
    NSDate *startDate = nil;
    long timezoneoffset = ([[NSTimeZone systemTimeZone] secondsFromGMT] / 3600);
    startDate = [NSDate dateWithTimeIntervalSince1970:self.doubleValue + timezoneoffset];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | kCFCalendarUnitHour |kCFCalendarUnitMinute fromDate:startDate];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSInteger hour = [components hour];
    NSInteger min = [components minute];
    
    return [NSString stringWithFormat:@"%d.%d.%d %02d:%02d",(int)year,(int)month,(int)day,(int)hour,(int)min];
}

- (NSString *)getStringNumberWithComma{
    NSString *string = @"";
    
    int value = [self intValue];
    
    while(value != 0){
        int mod = value % 1000;
        value = value / 1000;
     
        if( value == 0){
            string = [NSString stringWithFormat:@"%d%@", mod, string];
        }else{
            // mod가 100자리수가 아닐때 0을 채워줌.
            if( mod < 10){
                string = [NSString stringWithFormat:@",00%d%@", mod, string];
            }else if( mod < 100){
                string = [NSString stringWithFormat:@",0%d%@", mod, string];
            }
            else{
                string = [NSString stringWithFormat:@",%d%@", mod, string];
            }
        }
    }
    if([string isEqualToString:@""])
        return @"0";
    
    return string;
}

- (NSString *)distance{
    if(self.doubleValue > 1000){
        return [NSString stringWithFormat:@"04%fKm",(self.doubleValue / 1000)];
    }
    return [NSString stringWithFormat:@"%dm", (int)self.doubleValue];
}

- (NSString *)secToDuration{
    NSInteger totalSeconds = self.integerValue;
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    NSInteger hours = totalSeconds / 3600;
    if(hours > 0)
        return [NSString stringWithFormat:@"%02d:%02d:%02d", (int)hours, (int)minutes, (int)seconds];
    else if(minutes > 0){
        return [NSString stringWithFormat:@"%02d:%02d",minutes, seconds];
    }
    return [NSString stringWithFormat:@"%02d", seconds];
}
@end
