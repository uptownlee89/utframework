//
//  UTNavController.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 10. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTContainerViewController.h"
#import "UTControlButton.h"
#import "UTNavigationBar.h"
#define UTNavigationLeftButtonIdentifier @"mf_left_control_button"
#define UTNavigationRightButtonIdentifier @"mf_right_control_button"
@interface UTNavigationController : UTContainerViewController
@property (nonatomic, strong) UTControlButton *leftButton;
@property (nonatomic, strong) UTControlButton *rightButton;
@property (nonatomic, assign) BOOL navigationBarHidden;

- (void)setLeftButton:(UTControlButton *)leftButton leftPadding:(CGFloat)padding;
- (void)setRightButton:(UTControlButton *)rightButton rightPadding:(CGFloat)padding;
- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;
@end
