//
//  UTContainerViewController+Private.m
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTContainerViewController+Protected.h"
#import "UTDefine.h"

@implementation UTContainerViewController (Protected)

-(CGRect)frameForContentView:(UIInterfaceOrientation)orientation{
    CGRect rect =  self.view.bounds;
    rect.size.height = rect.size.height;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        rect.origin.y = UT_STATUS_BAR_HEIGHT + rect.origin.y;
    }
    return rect;
}

-(CGRect)frameForViewController:(UIViewController<UTViewController> *)viewController orientation:(UIInterfaceOrientation)orientation{
    CGFloat navHeight = HEIGHT_FOR_NAVIGATIONBAR(UIDeviceOrientationIsPortrait(orientation));//Y_ENDPOINT_OF_NAVIGATIONBAR(UIDeviceOrientationIsPortrait(orientation));
    CGRect frame =  [self frameForContentView:orientation];
    frame.origin.y = navHeight * (1.0f - self.controlBarHiddenRate);
    frame.size.height -= frame.origin.y;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        frame.origin.y -= UT_STATUS_BAR_HEIGHT;
    }
    return frame;
}

- (CGRect)frameForControlBar:(UIInterfaceOrientation)orientation{
    CGFloat navHeight = HEIGHT_FOR_NAVIGATIONBAR(UIDeviceOrientationIsPortrait(orientation));
    CGRect frame = [self frameForContentView:orientation];
    frame.size.height = navHeight;
    frame.origin.y = -navHeight * self.controlBarHiddenRate;
    return frame;
}


- (CGRect)frameForStatusBarBackground:(UIInterfaceOrientation)orientation{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        CGRect frame = [self frameForContentView:orientation];
        frame.size.height = UT_STATUS_BAR_HEIGHT;
        frame.origin.y = -UT_STATUS_BAR_HEIGHT;
//        //NSLog(@"%@", NSStringFromCGRect(frame));
        return frame;
    }
    return CGRectZero;
}
@end
