//
//  UTTableView.m
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTTableView.h"
#import "UTGroupTableCellBGView.h"
#import "UTDefine.h"
@interface UTTableView ()
//- (void)_setSevenStyleGroupingAtIndexPath:(NSIndexPath *)indexPath;
@end

@implementation UTTableView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return self;
}

- (void)setUseSevenStyleGrouping:(BOOL)useSevenStyleGrouping{
    _useSevenStyleGrouping = useSevenStyleGrouping;
    if(_useSevenStyleGrouping){
        self.backgroundColor = UIColorFromRGB(0xeeeeee);
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

@end
