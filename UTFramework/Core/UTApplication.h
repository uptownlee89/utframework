//
//  UTCore.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UTApplication;
@protocol UTApplicationDelegate <UIApplicationDelegate>
- (void)didApplicationLaunched:(UTApplication *)application;
- (BOOL)useRemoteNotificationApplication:(UTApplication *)application;
@optional
- (id)application:(UTApplication *)application propertyForKey:(NSString *)key;
- (void)application:(UTApplication *)application didReceiveRemoteNotificationOnActive:(NSDictionary *)userInfo;
- (void)application:(UTApplication *)application didLaunchWithRemoteNotification:(NSDictionary *)userInfo;
@end

extern int UTApplicationMain(int argc, char *argv[], NSString *delegateClassName, NSString *navigationCenterClassName);

@interface UTApplication : UIApplication

@property (nonatomic,assign) id<UTApplicationDelegate> delegate;

- (id)propertyForKey:(NSString *)key;
- (UIFont *)fontForKey:(NSString *)key;
//- (UIImage *)imageForKey:(NSString *)key;
- (UIColor *)colorForKey:(NSString *)key;
- (NSString *)stringForKey:(NSString *)key;
- (NSNumber *)numberForKey:(NSString *)key;
- (NSInteger)integerForKey:(NSString *)key;
- (NSInteger)doubleForKey:(NSString *)key;
- (NSString *)applicationName;

+ (UTApplication *)sharedApplication;
- (id)getSingletonedObjectForClass:(Class)classObject;

+ (BOOL)devicePushPossible;
+ (void)disableDevicePush;
+ (BOOL)enableDevicePush;

+ (void)throwInternalError:(NSString *)reason;
@end
