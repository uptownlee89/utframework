//
//  UTRestResource.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 16..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTMutableObject.h"
#import "UTRestCenter.h"

typedef void (^UTRestCallback)(NSInteger statusCode, id object, NSError *error);
typedef void (^UTRestCollectionCallback)(NSInteger statusCode, NSArray *objects, NSDictionary *paging, NSError *error);

@interface UTRestResource : UTMutableObject

@property (nonatomic, readonly) id pk;
@property (nonatomic, readonly) NSDictionary *parameters;

- (id)initWithPK:(id)pk paramemters:(NSDictionary *)parameters;
- (id)initWithPK:(id)pk;


+ (void)setBasePath:(NSString *)path;
+ (NSString *)basePath;

+ (NSString *)collectionIdentifier;
+ (NSString *)uniqueIdentifierWithPk:(id)pk;

+ (NSString *)singular;
+ (NSString *)plural;


- (void)CREATE:(UTRestCallback)callback;
+ (void)CREATE:(UTRestCallback)callback parameters:(NSDictionary *)params;
+ (void)CREATE:(UTRestCallback)callback parameters:(NSDictionary *)params constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block;
+ (void)CREATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block;
+ (void)CREATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params;

+ (void)COLLECTION:(UTRestCollectionCallback)callback;
+ (void)COLLECTION:(UTRestCollectionCallback)callback parameters:(NSDictionary *)params;
+ (void)COLLECTION:(NSString *)URI callback:(UTRestCollectionCallback)callback parameters:(NSDictionary *)params;
+ (void)NEXT:(UTRestCallback)callback paging:(NSDictionary *)paging;
+ (void)PREV:(UTRestCallback)callback paging:(NSDictionary *)paging;

- (NSString *)uniqueIdentifier;
+ (instancetype)resource:(id)data;

+ (void)READ:(UTRestCallback)callback pk:(id)pk;
+ (void)READ:(UTRestCallback)callback pk:(id)pk parameters:(NSDictionary *)params;
+ (void)READ:(UTRestCallback)callback URI:(NSString *)URI parameters:(NSDictionary *)params;


- (void)UPDATE:(UTRestCallback)callback parameters:(NSDictionary *)params;
+ (void)UPDATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params;

+ (void)DELETE:(UTRestCallback)callback;
+ (void)DELETE:(NSString *)URI callback:(UTRestCallback)callback;
+ (void)DELETE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params;
- (void)DELETE:(UTRestCallback)callback;
- (void)DELETE:(UTRestCallback)callback parameters:(NSDictionary *)params;

@end
