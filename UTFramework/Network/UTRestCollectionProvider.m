//
//  UTRestCollectionProvider.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 21..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTRestCollectionProvider.h"
#import "UTRestResource.h"
@interface UTRestCollectionProvider ()
@property (nonatomic, copy) Class resourceCls;
@property (nonatomic, strong) NSDictionary *paging;
@property (nonatomic, strong) NSString *uri;
@end

@implementation UTRestCollectionProvider

- (id)initWithRestResourceClass:(Class)resourceCls{
    if (self = [super init]){
        self.resourceCls = resourceCls;
    }
    return self;
}
- (id)initWithRestResourceClass:(Class)resourceCls URI:(NSString *)URI{
    if (self = [super init]){
        self.resourceCls = resourceCls;
        self.uri = URI;
    }
    return self;
}

- (id)initWithRestResourceClass:(Class)resourceCls parameters:(NSDictionary *)parameters{
    if (self = [super init]){
        self.resourceCls = resourceCls;
        self.params = [NSMutableDictionary dictionaryWithDictionary:parameters];
        if(self.params[@"URI"]){
            self.uri = self.params[@"URI"];
        }
    }
    return self;
}

- (void)asyncGetMoreDataWithCallback:(UTDataProviderCallback)callback{
    [self.resourceCls NEXT:^(NSInteger statusCode, id object, NSError *error) {
        if(!error && statusCode == 200){
            self.paging = object[@"paging"];
            callback(object[@"data"], nil);
        }
        else{
            callback(nil, error);
        }
    } paging:self.paging];
}

- (void)asyncLoadWithCallback:(UTDataProviderCallback)callback{
    void (^endBlock)(NSInteger statusCode, NSArray *objects, NSDictionary *paging, NSError *error) = ^(NSInteger statusCode, NSArray *objects, NSDictionary *paging, NSError *error){
        if(!error && statusCode == 200){
            self.paging = paging;
            callback(objects, nil);
        }
        else{
            callback(nil, error);
        }
    };
    
    if(self.uri){
        [self.resourceCls COLLECTION:self.uri callback:^(NSInteger statusCode, NSArray *objects, NSDictionary *paging, NSError *error) {
            endBlock(statusCode, objects, paging, error);
        } parameters:self.params[@"parameters"]];
    }
    else{
        [self.resourceCls COLLECTION:^(NSInteger statusCode, NSArray *objects, NSDictionary *paging, NSError *error) {
            endBlock(statusCode, objects, paging, error);
        } parameters:self.params];
    }
}

- (id)objectAtIndex:(NSUInteger)index{
    id resourceData = [super objectAtIndex:index];
    return [self.resourceCls resource:resourceData];
}

- (BOOL)canGetMore{
    return self.paging[@"next"] != nil;
}

- (BOOL)canRefresh{
    return YES;
}


- (id)objectAtIndexPath:(NSIndexPath *)indexPath{
    return [self objectAtIndex:indexPath.row];
}

- (NSArray *)sectionIndexTitles{
    return nil;
}

- (NSInteger)sectionForSectionIndexTitle:(NSString *)title atIndex:(NSUInteger)index{
    return 0;
}

- (NSUInteger)numberOfSection{
    return 1;
}

- (NSString *)titleForSection:(NSUInteger)section{
    return nil;
}

- (NSUInteger)numberOfObjectForSection:(NSUInteger)section{
    return [self count];
}
@end
