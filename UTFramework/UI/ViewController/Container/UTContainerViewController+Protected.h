//
//  UTContainerViewController+Private.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTContainerViewController.h"

@interface UTContainerViewController (Protected)

- (CGRect)frameForContentView:(UIInterfaceOrientation)orientation;
- (CGRect)frameForControlBar:(UIInterfaceOrientation)orientation;
- (CGRect)frameForStatusBarBackground:(UIInterfaceOrientation)orientation;
-(CGRect)frameForViewController:(UIViewController<UTViewController> *)viewController orientation:(UIInterfaceOrientation)orientation;
@end
