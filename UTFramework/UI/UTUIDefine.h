//
//  UTUIDefine.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#ifndef UTFramework_UTUIDefine_h
#define UTFramework_UTUIDefine_h



@protocol MFViewControllerFactory <NSObject>

- (MFViewController *)viewControllerFromUrl:(NSURL *)url parameters:(NSDictionary *)parameters;

@end

#endif
