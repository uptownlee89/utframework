//
//  UIImage+ImageMasked.h
//  MMTFramework
//
//  Created by 엄태건 on 13. 9. 13..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageMasked)
-(UIImage*)masking:(UIImage*)maskImage;
+(UIImage*)imageMasked:(UIImage*)image maskImage:(UIImage*)maskImage;
@end
