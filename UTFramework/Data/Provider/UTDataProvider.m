//
//  UTDataProvider.m
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTDataProvider.h"
#import "UTMutableObject.h"
#import "UTDataProvider+Protected.h"
#import "ODRefreshControl.h"
@interface UTDataProvider()
@property (nonatomic, assign) BOOL reservedLoadOn;
@property (nonatomic, strong) NSDate *lastUpdatedDate;
@property (nonatomic, strong) NSArray *getMoreIndexArray;
@end
@implementation UTDataProvider

@synthesize state = _currentState;

- (id)init{
    self = [super init];
    if(self){
        _currentState = UTDataProviderInit;
    }
    return self;
}

- (id)initWithParams:(id)params{
    if(self = [self init]){
        self.params = params;
    }
    return self;
}

- (void)refresh{
    if(self.canRefresh && self.state != UTDataProviderRefeshing){
        [self forceRefresh];
    }
}

- (void)refreshWithControl:(ODRefreshControl *)control{
//    [control beginRefreshing];
    [self refresh];
}

- (void)_statusChangeTo:(UTDataProviderState)to{
    if(to == _currentState) return;
    BOOL changeCallbackExists = [_delegate respondsToSelector:@selector(dataProvider:changeState:from:)];
    if (changeCallbackExists)
        [_delegate dataProvider:self changeState:to from:_currentState];
    _currentState  = to;
}
- (void)setNotAvailable{
    [self _statusChangeTo:UTDataProviderNotAvailable];
}


- (void)setFailed{
    [self _statusChangeTo:UTDataProviderFailed];
}

- (void)load{
    [self _statusChangeTo:UTDataProviderLoading];
    
    [self asyncLoadWithCallback:^(id data, NSError *error) {
        
        if(self.reservedLoadOn){
            self.reservedLoadOn = NO;
            [self load];
            return;
        }
        _lastError = error;
        if(!error){
            
            [self _setData:data];
            [self _statusChangeTo:UTDataProviderLoaded];
            self.lastUpdatedDate = [NSDate date];
            
            
        }
        else{
            [self _statusChangeTo:UTDataProviderNotAvailable];
        }
    }];
}

- (void)getMore{
    if(self.state == UTDataProviderLoaded){
        [self _statusChangeTo:UTDataProviderGettingMore];
        
        [self asyncGetMoreDataWithCallback:^(id data, NSError *error) {
            _lastError = error;
            if(!error){
                
                [self _getMoreData:data];
                [self _statusChangeTo:UTDataProviderLoaded];
            }
            else{
                [self _statusChangeTo:UTDataProviderNotAvailable];
            }
        }];
    }
}

- (void)loadWithData:(id)data{
    [self _statusChangeTo:UTDataProviderLoading];
    self.reservedLoadOn = NO;
    [self _setData:data];
    [self _statusChangeTo:UTDataProviderLoaded];
    self.lastUpdatedDate = [NSDate date];
}

- (void)getMoreWithData:(id)data{
    if(self.state == UTDataProviderLoaded){
        [self _statusChangeTo:UTDataProviderGettingMore];
        [self _getMoreData:data];
        [self _statusChangeTo:UTDataProviderLoaded];
    }
}

- (void)forceRefresh{
    BOOL changeCallbackExists = [_delegate respondsToSelector:@selector(dataProvider:changeState:from:)];
    if (changeCallbackExists)
        [_delegate dataProvider:self changeState:UTDataProviderRefeshing from:_currentState];
    _currentState = UTDataProviderRefeshing;
    
    [self asyncLoadWithCallback:^(id data, NSError *error) {
        _lastError = error;
        if(!error){
            [self _setData:data];
            [self _statusChangeTo:UTDataProviderLoaded];
            self.lastUpdatedDate = [NSDate date];
        }
        else{
            [self _statusChangeTo:UTDataProviderNotAvailable];
        }
        
        
    }];
}

- (void)reservedLoad{
    if(_currentState == UTDataProviderLoaded || _currentState == UTDataProviderInit || _currentState == UTDataProviderNotAvailable){
        [self load];
    }
    else{
        self.reservedLoadOn = YES;
    }
}

- (BOOL)canGetMore{
    return NO;
}

- (BOOL)canRefresh{
    return NO;
}

- (void)asyncGetMoreDataWithCallback:(UTDataProviderCallback)callback{
    callback(nil, nil);
}

- (void)asyncLoadWithCallback:(UTDataProviderCallback)callback{
    callback(nil, nil);
}

- (void)filter:(id)filter{
}

- (id)params{
    if(_params == nil){
        _params = [UTMutableObject object];
    }
    return _params;
}

- (void)_setData:(id)data{
}
- (void)_getMoreData:(id)data{
}
@end
