//
//  UTAppDelegate.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTAppDelegate.h"
#import "UTApplication.h"
#import "UTNavigationCenter.h"
#import "UTRootViewController.h"
//#import "UTDevice.h"
#import "UTDefine.h"
#import "UIImage+ImageCache.h"

@implementation UTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [UTNavigationCenter defaultCenter].rootViewController;
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (localNotif != nil) {
        [self application:application didReceiveLocalNotification:localNotif];
    }
    else if(userInfo != nil){
        [self application:application didReceiveRemoteNotification:userInfo];
    }
    else{
        [self didApplicationLaunched:(UTApplication* )application];
    }
    [self.window makeKeyAndVisible];
    
    @autoreleasepool {
        UIImageView *_splashView = nil;
        if(IS_IPHONE){
            if(IS_IPHONE4INCH){
                _splashView = [[UIImageView alloc] initWithImage:[UIImage imageCached:@"Default-568h@2x.png"]];
            }
            else{
                _splashView = [[UIImageView alloc] initWithImage:[UIImage imageCached:@"Default@2x.png"]];
            }
        }
        
        _splashView.frame = self.window.bounds;
        [self.window addSubview:_splashView];
        [self.window bringSubviewToFront:_splashView];
        _splashView.alpha = 1.0f;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.5f];
        [UIView setAnimationDelegate:_splashView];
        [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
        _splashView.alpha = 0.0f;
        [UIView commitAnimations];
    }
    [self useRemoteNotificationApplication:(UTApplication* )application];
    return YES;
}

- (BOOL)useRemoteNotificationApplication:(UTApplication *)application{
    return [UTApplication enableDevicePush];
}

- (void)didApplicationLaunched:(UTApplication *)application{
    [[UTNavigationCenter defaultCenter] openURL:u(@"//open")];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
    if ( application.applicationState == UIApplicationStateActive ){
        if([self respondsToSelector:@selector(application:didReceiveRemoteNotificationOnActive:)]){
            [self application:[UTApplication sharedApplication] didReceiveRemoteNotificationOnActive:userInfo];
        }
    }
    else{
        if([self respondsToSelector:@selector(application:didLaunchWithRemoteNotification:)]){
            [self application:[UTApplication sharedApplication] didLaunchWithRemoteNotification:userInfo];
        }
    }
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSMutableString *deviceId = [NSMutableString string];
    const unsigned char* ptr = (const unsigned char*) [deviceToken bytes];
    
    for(int i = 0 ; i < 32 ; i++)
    {
        [deviceId appendFormat:@"%02x", ptr[i]];
    }
    //NSLog(@"%@",deviceId);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
}

- (id)application:(UTApplication *)application propertyForKey:(NSString *)key{
    return nil;
}



@end
