//
//  UTCore.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTApplication.h"
#import <UIKit/UIKit.h>
#import "UTDefine.h"
#import "UTUtils.h"
#import "UTNavigationCenter.h"

/** @todo delegate encapsulation */
int UTApplicationMain(int argc, char *argv[], NSString *delegateClassName, NSString *navigationCenterClassName){
    [UTNavigationCenter setClassName:navigationCenterClassName];
    @autoreleasepool {
#ifdef DEBUG
        int retVal;
        @try {
            return UIApplicationMain(argc, argv, @"UTApplication", delegateClassName);
        }
        @catch (NSException *exception) {
            
            // the path to write file
            
            //NSLog(@"CRASH: %@", exception);
            //NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
        }
        @finally {
        }
        return retVal;
#else
        return UIApplicationMain(argc, argv, @"UTApplication", delegateClassName);
#endif
    }
}

@interface UTApplication (){
    NSMutableDictionary *_singletons;
}
@property (nonatomic, strong) NSDictionary *mainFontList;
@property (nonatomic, strong) NSDictionary *mainSettingList;
@property (nonatomic, strong) NSDictionary *mainColorList;
@property (nonatomic, strong) NSMutableDictionary *cachedFonts;
@property (nonatomic, strong) NSMutableDictionary *cachedColors;
@end

@implementation UTApplication

- (id)init{
    if( self = [super init]){
        _singletons = [[NSMutableDictionary alloc] init];
        self.cachedColors = [NSMutableDictionary dictionary];
        self.cachedFonts = [NSMutableDictionary dictionary];
        
        NSString *mainSettingList = [[NSBundle mainBundle] pathForResource:@"appsetting" ofType:@"plist"];
        NSString *mainFontList = [[NSBundle mainBundle] pathForResource:@"appfont" ofType:@"plist"];
        NSString *mainColorList = [[NSBundle mainBundle] pathForResource:@"appcolor" ofType:@"plist"];
        NSAssert(mainSettingList != NULL, @"No appsetting.plist in main bundle...");
        //NSAssert(mainFontList != NULL, @"No appsetting.plist in main bundle...");
        //NSAssert(mainColorList != NULL, @"No appsetting.plist in main bundle...");
        
        self.mainSettingList = [NSMutableDictionary dictionaryWithContentsOfFile:mainSettingList];
        if(mainFontList)
            self.mainFontList = [NSMutableDictionary dictionaryWithContentsOfFile:mainFontList];
        else
            self.mainFontList = [NSMutableDictionary dictionary];
        if(mainColorList)
            self.mainColorList = [NSMutableDictionary dictionaryWithContentsOfFile:mainColorList];
        else
            self.mainColorList = [NSMutableDictionary dictionary];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * __unused notification) {
            [_cachedColors removeAllObjects];
            [_cachedFonts removeAllObjects];
        }];
    }
    return self;
}

- (void)dealloc{
    _singletons = nil;
}


- (id)propertyForKey:(NSString *)key{
    if(self.delegate && [self.delegate respondsToSelector:@selector(application:propertyForKey:)]){
        id ret =[(id<UTApplicationDelegate>)self.delegate application:self propertyForKey:key];
        if(ret)
            return ret;
    }
    return [self propertyForKey:key from:self.mainSettingList];
}


- (id)propertyForKey:(NSString *)key from:(NSDictionary *)dict{
    
    id ret = [[NSBundle mainBundle] objectForInfoDictionaryKey:key];
    if(ret)
        return ret;
    ret = [dict objectForKey:key];
    if(ret)
        return ret;
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] no property.", key] userInfo:nil];
    
}


- (UIFont *)fontForKey:(NSString *)key{
    id ret = [self.cachedFonts objectForKey:key];
    if(ret){
        return ret;
    }
    ret = [self propertyForKey:key from:self.mainFontList];
    if([ret isKindOfClass:[NSDictionary class]]){
        if([[ret[@"name"] lowercaseString] isEqualToString:@"system"]){
            ret = [UIFont systemFontOfSize:[ret[@"size"] floatValue]];
        }
        else if([[ret[@"name"] lowercaseString] isEqualToString:@"system-bold"]){
            ret = [UIFont boldSystemFontOfSize:[ret[@"size"] floatValue]];
        }
        else{
            ret = [UIFont fontWithName:ret[@"name"] size:[ret[@"size"] floatValue]];
        }
    }
    else if([ret isKindOfClass:[NSString class]]){
        if([[ret substringToIndex:1] isEqualToString:@"@"]){
            ret = [self fontForKey:[ret substringFromIndex:1]];
        }
        else{
            NSArray *array = [ret componentsSeparatedByString:@" "];
            if([[array[0] lowercaseString] isEqualToString:@"system"]){
                ret = [UIFont systemFontOfSize:[array[1] floatValue]];
            }
            else if([[array[0] lowercaseString] isEqualToString:@"system-bold"]){
                ret = [UIFont boldSystemFontOfSize:[array[1] floatValue]];
            }
            else{
                ret = [UIFont fontWithName:array[0] size:[array[1] floatValue]];
            }
        }
    }
    if(ret && [ret isKindOfClass:[UIFont class]]){
        [self.cachedFonts setObject:ret forKey:key];
        return ret;
    }
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] font-type fields must be a dictionary or a string.", key] userInfo:nil];
    
}

- (UIColor *)colorForKey:(NSString *)key{
    
    
    id ret = [self.cachedColors objectForKey:key];
    if(ret){
        return ret;
    }
    
    ret = [self propertyForKey:key from:self.mainColorList];
    
    if([ret isKindOfClass:[NSNumber class]]){
        ret = UIColorFromRGB([ret intValue]);
    }
    else if([ret isKindOfClass:[NSDictionary class]]){
        NSNumber *nAlpha = ret[@"alpha"];
        int alpha = [nAlpha floatValue];
        if(!nAlpha){
            alpha = 1;
        }
        ret = [UIColor colorWithRed:([ret[@"red"] intValue]/255.0f) green:([ret[@"green"] intValue]/255.0f) blue:([ret[@"blue"] intValue]/255.0f) alpha:alpha];
    }
    else if([ret isKindOfClass:[NSString class]]){
        if([[ret substringToIndex:1] isEqualToString:@"@"]){
            ret = [self colorForKey:[ret substringFromIndex:1]];
        }
        else{
            if([[[ret substringToIndex:2] lowercaseString] isEqualToString:@"0x"]){
                unsigned int value;
                [[NSScanner scannerWithString:ret] scanHexInt: &value];
                ret = UIColorFromRGB(value);
            }
            else{
                NSDictionary *dict= [UTUtils getParametersFromQuery:ret];
                ret = [UIColor colorWithRed:([dict[@"red"] intValue]/255.0f) green:([dict[@"green"] intValue]/255.0f) blue:([dict[@"blue"] intValue]/255.0f) alpha:[dict[@"alpha"] floatValue]];
            }
        }
    }
    if(ret && [ret isKindOfClass:[UIColor class]]){
        [self.cachedColors setObject:ret forKey:key];
        return ret;
    }
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] color-type fields must be a dictionary, a unsigned int or a string.", key] userInfo:nil];
}

- (NSString *)stringForKey:(NSString *)key{
    return [self propertyForKey:key];
}

- (NSNumber *)numberForKey:(NSString *)key{
    id ret = [self propertyForKey:key];
    if([ret isKindOfClass:[NSNumber class]]){
        return ret;
    }
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] object must be number-type", key] userInfo:nil];
    
}

- (NSInteger)integerForKey:(NSString *)key{
    id ret = [self propertyForKey:key];
    if([ret isKindOfClass:[NSNumber class]]){
        return [ret intValue];
    }
    else if([ret isKindOfClass:[NSString class]]){
        return [ret intValue];
    }
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] object must be number-type or string-type", key] userInfo:nil];
}

- (NSInteger)doubleForKey:(NSString *)key{
    id ret = [self propertyForKey:key];
    if([ret isKindOfClass:[NSNumber class]]){
        return [ret doubleValue];
    }
    else if([ret isKindOfClass:[NSString class]]){
        return [ret doubleValue];
    }
    @throw [NSException exceptionWithName:@"invalid_property" reason:[NSString stringWithFormat:@"[key:%@] object must be number-type or string-type", key] userInfo:nil];
}


- (NSString *)applicationName{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}


+ (UTApplication *)sharedApplication{
    return (UTApplication *)[super sharedApplication];
}

- (id)getSingletonedObjectForClass:(Class)classObject{
    NSString *className = NSStringFromClass(classObject);
    id object = [_singletons objectForKey:className];
    if(object) return object;
    dispatch_semaphore_t singleton_sema = dispatch_semaphore_create(1);
    dispatch_semaphore_wait(singleton_sema, DISPATCH_TIME_FOREVER);
    
    object = [_singletons objectForKey:className];
    if(!object){
        
        object = [[NSClassFromString(className) alloc] init] ;
        [_singletons setObject:object forKey:className];
    }
    dispatch_semaphore_signal(singleton_sema);
    return object;
}

+ (BOOL)devicePushPossible{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        UIUserNotificationType types = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
        if (types == UIUserNotificationTypeNone){
            return NO;
        }else{
            return YES;
        }
    }
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone){
            return NO;
        }else{
            return YES;
        }
    }
}

+ (BOOL)enableDevicePush{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeAlert|
         UIRemoteNotificationTypeBadge|
         UIRemoteNotificationTypeSound];
    }
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        UIUserNotificationType types = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
        if (types == UIUserNotificationTypeNone){
            return NO;
        }else{
            return YES;
        }
    }
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone){
            return NO;
        }else{
            return YES;
        }
    }
}

+ (void)disableDevicePush{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
}

+ (void)throwInternalError:(NSString *)reason{
    @throw [NSException exceptionWithName:@"internal_error" reason:reason userInfo:nil];
}

@end
