//
//  UTMasonryViewController.h
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 16..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTViewController.h"
#import "UTMasonryView.h"
#import "UTArrayProvider.h"

@interface UTMasonryViewController : UTViewController<UTMasonryViewDataSource, UTMasonryViewDelegate, UTDataProviderDelegate>

@property (nonatomic, strong) UTDataProvider<UTArrayProviderProtocol> *dataProvider;
@property (nonatomic, strong) IBOutlet UTMasonryView *masonryView;
@property (nonatomic, strong) UIView *notAvailableView;
@property (nonatomic, strong) UIView *failedView;

- (id)initWithArrayProvider:(UTDataProvider<UTArrayProviderProtocol> *)dataProvider;
- (void)setAvaliable:(BOOL)isAvailable;
- (void)loadDefaultView;
@end
