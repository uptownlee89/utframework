//
//  UTCache.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTCache.h"
#import "UTUtils.h"
#import "UTDefine.h"

@interface UTCache (){
    NSCache * _coreCache;
}
@end
@implementation UTCache

+ (id)defaultCenter{
    return [self singleton];
}

- (id)init{
    self = [super init];
    if (self){
        
        _coreCache =  [[NSCache alloc] init];
        [_coreCache setCountLimit:UT_MEMCACHED_COUNT];
        [self removeFileCacheCreatedBefore:[NSDate dateWithTimeIntervalSinceNow:-UT_FILECACHE_RETENTION_DAYS * 86400]];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * __unused notification) {
            [_coreCache removeAllObjects];
        }];
    }
    return self;
}

- (void)dealloc{
    _coreCache = nil;
}

- (id)memCacheForKey:(id)key{
    return [_coreCache objectForKey:key];
}

- (id)memCacheForKey:(id)key loadCallback:(id (^)())callback{
    id ret = [self memCacheForKey:key];
    if(ret) return ret;
    id cachedObject = callback();
    [self setMemCache:cachedObject forKey:key];
    return cachedObject;
}
- (void)setMemCache:(id)obj forKey:(id)key{
    [_coreCache setObject:obj forKey:key];
}


- (void)flushFileCache{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[UTUtils applicationHiddenCacheDocumentsDirectory]]) {
        NSError *error;
        NSString *path =[UTUtils applicationHiddenCacheDocumentsDirectory] ;
        [[NSFileManager defaultManager] removeItemAtPath:[UTUtils applicationHiddenCacheDocumentsDirectory] error:&error];
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
            // Handle error.
            [NSException raise:@"Failed creating directory" format:@"[%@], %@", path, error];
        }
    }
}


- (void)removeFileCacheCreatedBefore:(NSDate *)createdDate{
    NSUInteger removedSize = 0;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[UTUtils applicationHiddenCacheDocumentsDirectory]]) {
        NSError *error;
        NSString *_documentFilePath = nil;
        NSArray *_documentsFileList = [[NSFileManager defaultManager] subpathsAtPath:[UTUtils applicationHiddenCacheDocumentsDirectory]];
        NSEnumerator *_documentsEnumerator = [_documentsFileList objectEnumerator];
        while (_documentFilePath = [_documentsEnumerator nextObject]) {
            
            NSString *_path = [[UTUtils applicationHiddenCacheDocumentsDirectory] stringByAppendingPathComponent:_documentFilePath];
            
            NSDictionary *_documentFileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:_path error: &error];
            NSDate *fileCreationDate = [_documentFileAttributes objectForKey:NSFileCreationDate];
            if([fileCreationDate compare:createdDate] <= 0){
                removedSize += [[_documentFileAttributes objectForKey:NSFileSize] intValue];
                [[NSFileManager defaultManager] removeItemAtPath:_path error:&error];
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithUnsignedLongLong:[self fileCacheSize] - removedSize] forKey:UT_USERDEFAULT_FILECACHESIZE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)removeFileCache:(NSString *)identifier context:(NSString *)context{
    if(context == nil){
        [self removeFileCache:identifier];
        return;
    }
    NSString *newIdentifier = [[NSString alloc] initWithFormat:@"%@___%@",context,identifier];
    [self removeFileCache:newIdentifier];
}

- (void)removeFileCache:(NSString *)identifier{
    NSString *path = [[NSString alloc] initWithFormat:@"%@/%@.utc", [UTUtils applicationHiddenCacheDocumentsDirectory],[UTUtils md5:identifier]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error;
        NSDictionary *_documentFileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error: NULL];
        NSString *fileSize = [_documentFileAttributes objectForKey:NSFileSize];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithUnsignedLongLong:[self fileCacheSize] - [fileSize intValue]] forKey:UT_USERDEFAULT_FILECACHESIZE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    }
}
- (void)setFileCache:(NSData *)data forKey:(NSString*)identifier{
    NSString *path = [[NSString alloc] initWithFormat:@"%@/%@.utc", [UTUtils applicationHiddenCacheDocumentsDirectory],[UTUtils md5:identifier]];
    BOOL ret =[data writeToFile:path atomically:NO];
    if(ret){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithUnsignedLongLong:[self fileCacheSize] + [data length]] forKey:UT_USERDEFAULT_FILECACHESIZE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)setFileCache:(NSData *)data forKey:(NSString*)identifier context:(NSString *)context{
    if(context == nil){
        [self setFileCache:data forKey:identifier];
        return;
    }
    NSString *newIdentifier = [[NSString alloc] initWithFormat:@"%@___%@",context,identifier];
    [self setFileCache:data forKey:newIdentifier];
}
- (NSData *)fileCacheForKey:(NSString *)identifier context:(NSString *)context{
    if(context == nil){
        return [self fileCacheForKey:identifier];
    }
    NSString *newIdentifier = [[NSString alloc] initWithFormat:@"%@___%@",context,identifier];
    NSData *ret = [self fileCacheForKey:newIdentifier];
    return ret;
}
- (NSData *)fileCacheForKey:(NSString *)identifier{
    if(identifier == nil) return nil;
    NSString *path = [[NSString alloc] initWithFormat:@"%@/%@.utc", [UTUtils applicationHiddenCacheDocumentsDirectory],[UTUtils md5:identifier]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    NSData *data = [NSData dataWithContentsOfFile:path];
    return data;
}


- (unsigned long long int)fileCacheSize {
    unsigned long long int cacheSize = [[[NSUserDefaults standardUserDefaults] objectForKey:UT_USERDEFAULT_FILECACHESIZE] unsignedLongLongValue];
    if(cacheSize <= 0.0f){
        NSFileManager *_manager = [NSFileManager defaultManager];
        NSString *_documentsDirectory = [UTUtils applicationHiddenCacheDocumentsDirectory];
        NSArray *_documentsFileList;
        NSEnumerator *_documentsEnumerator;
        NSString *_documentFilePath;
        cacheSize = 0.0f;
        _documentsFileList = [_manager subpathsAtPath:_documentsDirectory];
        _documentsEnumerator = [_documentsFileList objectEnumerator];
        while (_documentFilePath = [_documentsEnumerator nextObject]) {
            NSDictionary *_documentFileAttributes = [_manager attributesOfItemAtPath: [_documentsDirectory stringByAppendingPathComponent:_documentFilePath] error: NULL];
            NSString *fileSize = [_documentFileAttributes objectForKey:NSFileSize];
            cacheSize += [fileSize intValue];
        }
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithUnsignedLongLong:cacheSize] forKey:UT_USERDEFAULT_FILECACHESIZE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return cacheSize;
    }
    return cacheSize;
}

@end
