//
//  UTAppDelegate.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTApplication.h"

@interface UTAppDelegate : UIResponder <UTApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@end
