//
//  UTUtils.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTUtils.h"
#import <UIKit/UIKit.h>
#import "UTDefine.h"
#import <sys/utsname.h>
#import <CommonCrypto/CommonDigest.h>

@implementation UTUtils

+ (void)shuffleWithFirstIndex:(NSMutableArray *)array  first:(NSInteger)first{
    
    static BOOL seeded = NO;
    if(!seeded)
    {
        seeded = YES;
        srandom((unsigned int)time(NULL));
    }
    
    NSUInteger count = [array count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        unsigned long nElements = count - i;
        unsigned long n = (random() % nElements) + i;
        if( i != first && n != first)
        [array exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    [array exchangeObjectAtIndex:first withObjectAtIndex:0];
}
+ (void)shuffle:(NSMutableArray *)array{
    static BOOL seeded = NO;
    if(!seeded)
    {
        seeded = YES;
        srandom((unsigned int)time(NULL));
    }
    
    NSUInteger count = [array count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        unsigned long nElements = count - i;
        unsigned long n = (random() % nElements) + i;
        [array exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

+ (NSString *)stringWithUTF8PercentEscape:(NSString *)str {
	return (__bridge_transfer NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, ( __bridge CFStringRef)str, NULL, CFSTR("￼=,!$&'()*+;@?\n\"<>#\t :/"),kCFStringEncodingUTF8);
}

+ (NSString*)decodeFromPercentEscapeString:(NSString *) str {
    return ( __bridge_transfer NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                                   ( __bridge CFStringRef)str,
                                                                                                   CFSTR(""),
                                                                                                   kCFStringEncodingUTF8) ;
}

+ (NSString *)randomString:(NSInteger)length{
    NSMutableString *string = [[NSMutableString alloc] initWithString:@""];
    for (int x=0;x<length; x ++){
        [string appendFormat:@"%c",(char)('A' + (arc4random_uniform(26)))];
    }
    return string;
}


+ (NSString *)queryString:(NSDictionary *)dict{
    return [self queryStringWithKey:@"" dict:dict];
}


+ (NSString *)queryStringWithKey:(NSString *)string dict:(NSDictionary *)dict{
    if([string length] > 0){
        string = [NSString stringWithFormat:@"%@__",string];
    }
    if(dict) {
        NSMutableArray *parts = [NSMutableArray array];
        for (id key in dict){
            id value = [dict objectForKey:key];
            if([value isKindOfClass:[NSNull class]]){
                [parts addObject:[NSString stringWithFormat:@"%@%@=",[UTUtils stringWithUTF8PercentEscape:string], [UTUtils stringWithUTF8PercentEscape:key]]];
            }
            else if([value isKindOfClass:[NSString class]]){
                [parts addObject:[NSString stringWithFormat:@"%@%@=%@",[UTUtils stringWithUTF8PercentEscape:string], [UTUtils stringWithUTF8PercentEscape:key], [UTUtils stringWithUTF8PercentEscape:value]]];
            }
            else if([value isKindOfClass:[NSArray class]]){
                [parts addObject:[UTUtils queryStringWithKey:[NSString stringWithFormat:@"%@%@",string, key] array:value]];
            }
            else if([value isKindOfClass:[NSDictionary class]]){
                [parts addObject:[UTUtils queryStringWithKey:[NSString stringWithFormat:@"%@%@",string, key] dict:value]];
            }
            else{
                [parts addObject:[NSString stringWithFormat:@"%@%@=%@",[UTUtils stringWithUTF8PercentEscape:string],[UTUtils stringWithUTF8PercentEscape:key], value]];
            }
        }
        return [parts componentsJoinedByString:@"&"];
    }
    return @"";
}

+ (NSString *)queryStringWithKey:(NSString *)string array:(NSArray *)array{
    if(array) {
        NSMutableArray *parts = [NSMutableArray array];
        for (id object in array){
            if([object isKindOfClass:[NSNull class]]){
                [parts addObject:[NSString stringWithFormat:@"%@[]=",[UTUtils stringWithUTF8PercentEscape:string]]];
            }
            else if([object isKindOfClass:[NSString class]]){
                [parts addObject:[NSString stringWithFormat:@"%@[]=%@",[UTUtils stringWithUTF8PercentEscape:string], [UTUtils stringWithUTF8PercentEscape:object]]];
            }
            else if([object isKindOfClass:[NSArray class]]){
                [parts addObject:[UTUtils queryStringWithKey:[NSString stringWithFormat:@"%@[]",string] array:object]];
            }
            else if([object isKindOfClass:[NSDictionary class]]){
                [parts addObject:[UTUtils queryStringWithKey:[NSString stringWithFormat:@"%@[]",string] dict:object]];
            }
            else{
                [parts addObject:[NSString stringWithFormat:@"%@[]=%@",[UTUtils stringWithUTF8PercentEscape:string], object]];
            }
        }
        return [parts componentsJoinedByString:@"&"];
    }
    return @"";
}

static NSString *applicationDocumentsDirectory = nil;
static NSString *applicationHiddenDocumentsDirectory = nil;
static NSString *applicationHiddenCacheDocumentsDirectory = nil;

+ (NSString *)applicationDocumentsDirectory {
    if(applicationDocumentsDirectory == nil){
        applicationDocumentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] ;
    }
    return applicationDocumentsDirectory;
}

+ (NSString *)applicationHiddenDocumentsDirectory {
    if(applicationHiddenDocumentsDirectory == nil){
        // NSString *path = [[self applicationHiddenDocumentsDirectory] stringByAppendingPathComponent:@".data"];
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        applicationHiddenDocumentsDirectory = [libraryPath stringByAppendingPathComponent:@"Hidden"] ;
        
        BOOL isDirectory = NO;
        if ([[NSFileManager defaultManager] fileExistsAtPath:applicationHiddenDocumentsDirectory isDirectory:&isDirectory]) {
            if (isDirectory)
            return applicationHiddenDocumentsDirectory;
            else {
                // Handle error. ".data" is a file which should not be there...
                [NSException raise:@".data exists, and is a file" format:@"Path: %@", applicationHiddenDocumentsDirectory];
                // NSError *error = nil;
                // if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error]) {
                //     [NSException raise:@"could not remove file" format:@"Path: %@", path];
                // }
            }
        }
        NSError *error = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:applicationHiddenDocumentsDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            // Handle error.
            [NSException raise:@"Failed creating directory" format:@"[%@], %@", applicationHiddenDocumentsDirectory, error];
        }
    }
    return applicationHiddenDocumentsDirectory;
}

+ (NSString *)applicationHiddenCacheDocumentsDirectory {
    if(applicationHiddenCacheDocumentsDirectory == nil){
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        applicationHiddenCacheDocumentsDirectory = [libraryPath stringByAppendingPathComponent:@"Cache"] ;
        
        BOOL isDirectory = NO;
        if ([[NSFileManager defaultManager] fileExistsAtPath:applicationHiddenCacheDocumentsDirectory isDirectory:&isDirectory]) {
            if (isDirectory)
            return applicationHiddenCacheDocumentsDirectory;
            else {
                // Handle error. ".data" is a file which should not be there...
                [NSException raise:@".data exists, and is a file" format:@"Path: %@", applicationHiddenCacheDocumentsDirectory];
                // NSError *error = nil;
                // if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error]) {
                //     [NSException raise:@"could not remove file" format:@"Path: %@", path];
                // }
            }
        }
        NSError *error = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:applicationHiddenCacheDocumentsDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            // Handle error.
            [NSException raise:@"Failed creating directory" format:@"[%@], %@", applicationHiddenCacheDocumentsDirectory, error];
        }
    }
    return applicationHiddenCacheDocumentsDirectory;
}

+ (NSString*)machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

+ (NSString *) buildVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+ (NSString *) shortVersionString
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+ (NSString *) bundleIdentifier
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleIdentifierKey];
}

+ (NSString *)localeIdenfier2{
    return [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
}

+ (NSString *)deviceIdentifier{
    if([[NSUserDefaults standardUserDefaults ] objectForKey:@"ut_device_identifier"]){
        return [[NSUserDefaults standardUserDefaults ] objectForKey:@"ut_device_identifier"];
    }
    NSString * ret = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    [[NSUserDefaults standardUserDefaults ] setObject:ret forKey:@"ut_device_identifier"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return ret;
}
+ (NSString *)systemVersion{
    return [[UIDevice currentDevice] systemVersion];
}


+ (NSString *)stringFromData:(NSData *)data{
    
    NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return result ;
}

+ (id)jsonFromData:(NSData *)data error:(NSError **)error{
    
    
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
    
    
    if (!jsonObject) {
        return nil;
    } else {
        
        if ([jsonObject isKindOfClass:[NSArray class]]) {
            NSArray *jsonArray = (NSArray *)jsonObject;
            return jsonArray;
        }
        else {
            NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
            return jsonDictionary;
        }
    }
}

+ (UIImage *)imageFromData:(NSData *)data{
    return [[UIImage alloc] initWithData:data] ;
}


+ (NSDictionary *)getParametersFromURL:(NSURL *)url{
    
    return [self getParametersFromQuery:[url query]];
}


+ (NSDictionary *)getParametersFromQuery:(NSString *)query{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [query componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        [params setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
    }
    return params ;
}

+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+ (NSString *)pluralize:(NSString *)singular{
    NSDictionary *ABERRANT_PLURAL_MAP = @{
                                        @"appendix": @"appendices",
                                        @"barracks": @"barracks",
                                        @"cactus": @"cacti",
                                        @"child": @"children",
                                        @"criterion": @"criteria",
                                        @"deer": @"deer",
                                        @"echo": @"echoes",
                                        @"elf": @"elves",
                                        @"embargo": @"embargoes",
                                        @"focus": @"foci",
                                        @"fungus": @"fungi",
                                        @"goose": @"geese",
                                        @"hero": @"heroes",
                                        @"hoof": @"hooves",
                                        @"index": @"indices",
                                        @"knife": @"knives",
                                        @"leaf": @"leaves",
                                        @"life": @"lives",
                                        @"man": @"men",
                                        @"mouse": @"mice",
                                        @"nucleus": @"nuclei",
                                        @"person": @"people",
                                        @"phenomenon": @"phenomena",
                                        @"potato": @"potatoes",
                                        @"self": @"selves",
                                        @"syllabus": @"syllabi",
                                        @"tomato": @"tomatoes",
                                        @"torpedo": @"torpedoes",
                                        @"veto": @"vetoes",
                                        @"woman": @"women",
                                         };
    NSString *VOWELS = @"aeiou";
    NSString *root, *suffix;
    root = singular;
    NSString *plural = ABERRANT_PLURAL_MAP[singular];
    if(plural)
        return plural;
    NSUInteger len = [singular length];
    NSRange range0, range1, range2, range3;
    range0.location = len - 1;
    range0.length = 1;
    range1.location = len - 2;
    range1.length = 1;
    range2.location = len - 3;
    range2.length = 3;
    range3.location = len - 2;
    range3.length = 2;
    if([@"y" isEqualToString:[singular substringWithRange:range0]] &&
       [VOWELS rangeOfString:[singular substringWithRange:range1]].location == NSNotFound){
        root = [singular substringToIndex:len - 1];
        suffix = @"ies";
    }
    else if([@"s" isEqualToString:[singular substringWithRange:range0]]){
        if([VOWELS rangeOfString:[singular substringWithRange:range1]].location != NSNotFound){
            if([@"ius" isEqualToString:[singular substringWithRange:range2]]){
                root = [singular substringToIndex:len - 2];
                suffix = @"i";
            }
            else{
                root = [singular substringToIndex:len - 1];
                suffix = @"ses";
            }
        }
        else{
            suffix = @"es";
        }
    }
    else if([@"ch" isEqualToString:[singular substringWithRange:range3]] || [@"sh" isEqualToString:[singular substringWithRange:range3]]){
        suffix = @"es";
    }
    else{
        suffix = @"s";
    }

    return [NSString stringWithFormat:@"%@%@", root, suffix];

}
@end
