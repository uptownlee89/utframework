//
//  UTRootViewController.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTViewController.h"

@interface UTRootViewController : UTViewController<UIAlertViewDelegate, UIActionSheetDelegate, UITextFieldDelegate>

@property(nonatomic, readonly) UIViewController *currentViewController;
@property(nonatomic, assign) BOOL manageStatusBar;
- (void)setViewController:(UIViewController *)mainViewController;
- (void)setFirstViewController:(UIViewController *)mainViewController;
- (void)switchViewControllerFrom:(UIViewController *)oldViewController to:(UIViewController *)newViewController;
- (void)confirmDialogWithTitle:(NSString *)title message:(NSString *)message confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle callback:( void (^)(BOOL))callback;
- (void)textFieldDialogWithTitle:(NSString *)title message:(NSString*)message textFieldContent:(NSString*)content confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle type:(UIKeyboardType)keyboardType callback:( void (^)(UITextField*, BOOL))callback;
- (void)multiChoiceDialogWithTitle:(NSString *)title message:(NSString *)message titles:(NSArray*)titles cancelTitle:(NSString *)cancelTitle callback:( void (^)(NSInteger index))callback;
- (void)showLogView:(NSString *)message withFont:(UIFont *)font duration:(CGFloat)duration;
- (void)showLogView:(NSString *)message withFont:(UIFont *)font;
- (void)actionSheetWithTitle:(NSString *)title cancelTitle:(NSString *)cancelTitle destructiveTitle:(NSString *)destructiveTitle others:(NSArray *)others callback:( void (^)(NSString *title, NSInteger index))callback;
- (void)forceHideLogView;
- (void)showLogView:(NSString *)message;
- (void)showLogView:(NSString *)message duration:(CGFloat)duration;
- (void)doubleTextFieldDialogWithTitle:(NSString *)title message:(NSString*)message textFieldContent:(NSString*)content0 extraTextFieldContent:(NSString*)content1 confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle type:(UIKeyboardType)keyboardType callback:( void (^)(UITextField*, UITextField*, BOOL))callback;
@end
