//
//  NSObject+UTFrameworkProtected.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "NSObject+UTFrameworkProtected.h"
#import "NSObject+UTFramework.h"

@implementation NSObject (UTFrameworkProtected)

- (void)_invokeBlock:(id)sender {
    UTActionBlock block = [self associatedValueForKey:kUTFrameworkBlockKey];
    if (block)
        block(sender);
}
@end
