//
//  NSObject+UTFramework.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "NSObject+UTFramework.h"
#import <objc/runtime.h>

@implementation NSObject (UTFramework)

- (void)retainAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)copyAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)assignAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_ASSIGN);
}

- (id)associatedValueForKey:(const char *)key {
    return objc_getAssociatedObject(self, key);
}

- (void)removeAllAssociatedObjects {
    objc_removeAssociatedObjects(self);
}

+ (void)retainAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (void)copyAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+ (void)assignAssociateValue:(id)value withKey:(const char *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_ASSIGN);
}

+ (id)associatedValueForKey:(const char *)key {
    return objc_getAssociatedObject(self, key);
}

+ (void)removeAllAssociatedObjects {
    objc_removeAssociatedObjects(self);
}

@end
