//
//  UTRestResource.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 16..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTRestResource.h"
#import "UTRestCenter.h"
#import "UTUtils.h"

static NSString *basePath = nil;
@interface UTRestResource ()

@end
@implementation UTRestResource

- (id)initWithPK:(id)pk{
    return [self initWithPK:pk paramemters:nil];
}

- (id)initWithPK:(id)pk paramemters:(NSDictionary *)parameters{
    if( self = [super init]){
        _pk = pk;
        _parameters = parameters;
    }
    return self;
}
+ (instancetype)resource:(id)data{
    return [self objectFromDictionary:data];
}

- (void)CREATE:(UTRestCallback)callback{
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:self];
    NSMutableDictionary *dataParams = [NSMutableDictionary dictionary];
    for (id key in newParams.allKeys){
        Class cls = [newParams[key] class];
        if([cls isSubclassOfClass:[UIImage class]]){
            dataParams[key] = @{@"data": UIImageJPEGRepresentation(newParams[key], 1), @"mimeType": @"image/jpeg"};
            [newParams removeObjectForKey:key];
        }
        else if ([cls isSubclassOfClass:[NSData class]]){
            dataParams[key] = @{@"data": newParams[key], @"mimeType": @"text/txt"};
            [newParams removeObjectForKey:key];
        }
    }
    
    [[self class] CREATE:callback parameters:newParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id key in dataParams.allKeys){
            [formData appendPartWithFileData:dataParams[key][@"data"] name:key fileName:@"upload" mimeType:dataParams[key][@"mimeType"]];
        }
    }];
}

+ (void)CREATE:(UTRestCallback)callback parameters:(NSDictionary *)params{
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:params];
    NSMutableDictionary *dataParams = [NSMutableDictionary dictionary];
    for (id key in newParams.allKeys){
        Class cls = [newParams[key] class];
        if([cls isSubclassOfClass:[UIImage class]]){
            dataParams[key] = @{@"data": UIImageJPEGRepresentation(newParams[key], 1), @"mimeType": @"image/jpeg"};
            [newParams removeObjectForKey:key];
        }
        else if ([cls isSubclassOfClass:[NSData class]]){
            dataParams[key] = @{@"data": newParams[key], @"mimeType": @"text/txt"};
            [newParams removeObjectForKey:key];
        }
    }
    [self CREATE:callback parameters:newParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id key in dataParams.allKeys){
            [formData appendPartWithFileData:dataParams[key][@"data"] name:key fileName:@"upload" mimeType:dataParams[key][@"mimeType"]];
        }
        
    }];
}

+ (void)CREATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params{
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:params];
    NSMutableDictionary *dataParams = [NSMutableDictionary dictionary];
    for (id key in newParams.allKeys){
        Class cls = [newParams[key] class];
        if([cls isSubclassOfClass:[UIImage class]]){
            dataParams[key] = @{@"data": UIImageJPEGRepresentation(newParams[key], 1), @"mimeType": @"image/jpeg"};
            [newParams removeObjectForKey:key];
        }
        else if ([cls isSubclassOfClass:[NSData class]]){
            dataParams[key] = @{@"data": newParams[key], @"mimeType": @"text/txt"};
            [newParams removeObjectForKey:key];
        }
    }
    [self CREATE:URI callback:callback parameters:newParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id key in dataParams.allKeys){
            [formData appendPartWithFileData:dataParams[key][@"data"] name:key fileName:@"upload" mimeType:dataParams[key][@"mimeType"]];
        }
        
    }];
}

+ (NSError *)error:(NSInteger)code info:(NSDictionary *)info{
    return [NSError errorWithDomain:@"ut_rest_resource_error" code:code userInfo:info];
}

+ (void)CREATE:(UTRestCallback)callback parameters:(NSDictionary *)params constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block{
    [self CREATE:self.collectionIdentifier callback:callback parameters:params constructingBodyWithBlock:block];
}

+ (void)CREATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block{
    [[UTRestCenter defaultCenter] POST:URI parameters:params constructingBodyWithBlock:block success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
}

+ (void)COLLECTION:(UTRestCollectionCallback)callback{
    [self COLLECTION:callback parameters:nil];
}

+ (void)COLLECTION:(UTRestCollectionCallback)callback parameters:(NSDictionary *)params{
    [self COLLECTION:self.collectionIdentifier callback:callback parameters:params];
}

+ (void)COLLECTION:(NSString *)URI callback:(UTRestCollectionCallback)callback parameters:(NSDictionary *)params{
    [[UTRestCenter defaultCenter] GET:URI parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            NSMutableArray *ret = [NSMutableArray array];
            for (id eachObject in responseObject[@"data"]){
                [ret addObject:[[self class] resource:eachObject]];
            }
            callback([operation.response statusCode], ret, responseObject[@"paging"], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], nil, nil, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
    
}

+ (void)NEXT:(UTRestCallback)callback paging:(NSDictionary *)paging{
    [[UTRestCenter defaultCenter] GET:paging[@"next"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
}

+ (void)PREV:(UTRestCallback)callback paging:(NSDictionary *)paging{
    [[UTRestCenter defaultCenter] GET:paging[@"prev"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
}

+ (void)READ:(UTRestCallback)callback pk:(id)pk{
    [self READ:callback pk:pk parameters:nil];
}

+ (void)READ:(UTRestCallback)callback pk:(id)pk parameters:(NSDictionary *)params{
    [self READ:callback URI:[self uniqueIdentifierWithPk:pk] parameters:params];
}


+ (void)READ:(UTRestCallback)callback URI:(NSString *)URI parameters:(NSDictionary *)params{
    [[UTRestCenter defaultCenter] GET:URI parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
    
}


+ (void)UPDATE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params{
//    [[UTRestCenter defaultCenter] PUT:URI parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if(callback){
//            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
//        if(callback)
//            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
//    }];
//    
    // manager needs to be init'd with a valid baseURL
    
    NSMutableDictionary *dataParams = [NSMutableDictionary dictionary];
    NSMutableDictionary *newParams = [NSMutableDictionary dictionary];
    for(NSString *key in params){
        id each = params[key];
        if([each isKindOfClass:[UIImage class]]){
            dataParams[key] = @{@"data": UIImageJPEGRepresentation(each, 1), @"mimeType": @"image/jpeg"};
        }
        else if ([each isKindOfClass:[NSData class]]){
            dataParams[key] = @{@"data": each, @"mimeType": @"text/txt"};
        }
        else{
            [newParams setObject:each forKey:key];
        }
    }
    
    // need to pass the full URLString instead of just a path like when using 'PUT' or 'POST' convenience methods
    NSString *URLString = [NSString stringWithFormat:@"%@%@", [UTRestCenter defaultCenter].baseURL , URI];
    NSMutableURLRequest *request = [[UTRestCenter defaultCenter].requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:URLString parameters:newParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id key in dataParams.allKeys){
            [formData appendPartWithFileData:dataParams[key][@"data"] name:key fileName:@"upload" mimeType:dataParams[key][@"mimeType"]];
        }
    } error:nil];
    
    // 'PUT' and 'POST' convenience methods auto-run, but HTTPRequestOperationWithRequest just
    // sets up the request. you're responsible for firing it.
    AFHTTPRequestOperation *requestOperation = [[UTRestCenter defaultCenter] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
    
    // fire the request
    [requestOperation start];
}


- (void)UPDATE:(UTRestCallback)callback parameters:(NSDictionary *)params{
//    [[UTRestCenter defaultCenter] PUT:self.uniqueIdentifier parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if(callback){
//            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
//        if(callback)
//            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
//    }];
//    
    
    NSMutableDictionary *dataParams = [NSMutableDictionary dictionary];
    NSMutableDictionary *newParams = [NSMutableDictionary dictionary];
    for(NSString *key in params){
        id each = params[key];
        if([each isSubclassOfClass:[UIImage class]]){
            dataParams[key] = @{@"data": UIImageJPEGRepresentation(each, 1), @"mimeType": @"image/jpeg"};
        }
        else if ([each isSubclassOfClass:[NSData class]]){
            dataParams[key] = @{@"data": each, @"mimeType": @"text/txt"};
        }
        else{
            [newParams setObject:each forKey:key];
        }
    }
    
    // need to pass the full URLString instead of just a path like when using 'PUT' or 'POST' convenience methods
    NSString *URLString = [NSString stringWithFormat:@"%@%@", [UTRestCenter defaultCenter].baseURL , self.uniqueIdentifier];
    NSMutableURLRequest *request = [[UTRestCenter defaultCenter].requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:URLString parameters:newParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id key in dataParams.allKeys){
            [formData appendPartWithFileData:dataParams[key][@"data"] name:key fileName:@"upload" mimeType:dataParams[key][@"mimeType"]];
        }
    } error:nil];
    
    // 'PUT' and 'POST' convenience methods auto-run, but HTTPRequestOperationWithRequest just
    // sets up the request. you're responsible for firing it.
    AFHTTPRequestOperation *requestOperation = [[UTRestCenter defaultCenter] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
    
    // fire the request
    [requestOperation start];
    
}

- (void)DELETE:(UTRestCallback)callback{
    [self DELETE:callback parameters:nil];
}

+ (void)DELETE:(UTRestCallback)callback{
    [self DELETE:self.collectionIdentifier callback:callback];
}

+ (void)DELETE:(NSString *)URI callback:(UTRestCallback)callback{
    [self DELETE:URI callback:callback parameters:nil];
//    [[UTRestCenter defaultCenter] DELETE:URI parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if(callback){
//            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
//        if(callback)
//            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
//    }];
}

+ (void)DELETE:(NSString *)URI callback:(UTRestCallback)callback parameters:(NSDictionary *)params{
    [[UTRestCenter defaultCenter] DELETE:URI parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
}

- (void)DELETE:(UTRestCallback)callback parameters:(NSDictionary *)params{
    [[UTRestCenter defaultCenter] DELETE:self.uniqueIdentifier parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(callback){
            callback(operation.response.statusCode, [[self class] resource:responseObject], nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(callback)
            callback([operation.response statusCode], responseObject, [[self class] error:[operation.response statusCode] info:responseObject]);
    }];
}

+ (void)setBasePath:(NSString *)path{
    basePath = path;
}

+ (NSString *)basePath{
    return basePath? basePath: @"";
}

+ (NSString *)singular{
    return @"rest-resource";
}

+ (NSString *)plural{
    return [UTUtils pluralize:self.singular];
}

- (NSString *)uniqueIdentifier{
    return [NSString stringWithFormat:@"%@%@/%@", [self class].basePath, [self class].singular, self.pk];
}

+ (NSString *)collectionIdentifier{
    return [NSString stringWithFormat:@"%@%@", self.basePath, self.plural];
}

+ (NSString *)uniqueIdentifierWithPk:(id)pk{
    return [NSString stringWithFormat:@"%@%@/%@", self.basePath, self.singular, pk];
}

@end
