//
//  UTUILibary.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTObject.h"
#import <UIKit/UIKit.h>

#define UILOCK static BOOL __locked = NO; \
if(__locked) return;\
    __locked = YES; \

#define UIUNLOCK \
    __locked = NO;

#define UTAUTORESIZINGFULL (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin)

@interface UTUILibrary : UTObject

+ (void)setView:(UIView *)view shake:(BOOL)enabled;
+ (void)recursiveEnumerateSubviewsForView:(UIView*)view block:(void (^)(UIView *view, BOOL *stop))block;

+ (void)shakeView:(UIView *)view;


+ (BOOL)hasRecognizedValidGesture:(UIGestureRecognizer *)recognizer;
+ (void)gestureRecognizerEnd:(UIGestureRecognizer *)recognizer;
+ (void)setView:(UIView *)view blockJob:(void (^)())block;
+ (UIImage *)imageWithView:(UIView *)view;

+ (CGPoint)positionInLayer:(CALayer *)layer pointForView:(CGPoint)point;
@end
