//
//  UTImage.h
//  UTFramework
//
//  Created by Juyoung Lee on 12. 6. 27..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  UIImage (resizable)
- (UIImage *)resizableImageWithCapInsets:(UIEdgeInsets)capInsets isStetchable:(BOOL)isStetchable;
@end
