//
//  UTUILibary.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTUILibrary.h"
#import "UTDefine.h"
#import <QuartzCore/QuartzCore.h>
@implementation UTUILibrary

+ (UIImage *)imageWithView:(UIView *)view{
    
    CGSize screenShotSize = view.bounds.size;
//    UIImage *img;
    UIGraphicsBeginImageContext(screenShotSize);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *topViewImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    [view drawLayer:view.layer inContext:ctx];
//    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return topViewImage;
    
}

+ (void)recursiveEnumerateSubviewsForView:(UIView*)view block:(void (^)(UIView *view, BOOL *stop))block {
	if (view.subviews.count == 0) {
		return;
	}
	for (UIView *subview in [view subviews]) {
		BOOL stop = NO;
		block(subview, &stop);
		if (stop) {
			return;
		}
        [self recursiveEnumerateSubviewsForView:subview block:block];
	}
}

+ (void)setView:(UIView *)view shake:(BOOL)enabled{
    if (enabled)
    {
        CGFloat rotation = 0.03;
        
        CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"transform"];
        shake.duration = 0.13;
        shake.autoreverses = YES;
        shake.repeatCount  = MAXFLOAT;
        shake.removedOnCompletion = NO;
        shake.fromValue = [NSValue valueWithCATransform3D:CATransform3DRotate(view.layer.transform,-rotation, 0.0 ,0.0 ,1.0)];
        shake.toValue   = [NSValue valueWithCATransform3D:CATransform3DRotate(view.layer.transform, rotation, 0.0 ,0.0 ,1.0)];
        
        [view.layer addAnimation:shake forKey:@"shakeAnimation"];
    }
    else
    {
        [view.layer removeAnimationForKey:@"shakeAnimation"];
    }
    
}


+ (void)shakeView:(UIView *)view{
    CGFloat rotation = 0.03;
    
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"transform"];
    shake.duration = 0.1;
    shake.autoreverses = NO;
    shake.repeatCount  = 1;
    shake.removedOnCompletion = YES;
    shake.fromValue = [NSValue valueWithCATransform3D:CATransform3DRotate(view.layer.transform,-rotation, 0.0 ,0.0 ,1.0)];
    shake.toValue   = [NSValue valueWithCATransform3D:CATransform3DRotate(view.layer.transform, rotation, 0.0 ,0.0 ,1.0)];
    
    [view.layer addAnimation:shake forKey:@"shakeAnimation"];
    
}





+ (void)gestureRecognizerEnd:(UIGestureRecognizer *)recognizer
{
    BOOL currentStatus = recognizer.enabled;
    recognizer.enabled = NO;
    recognizer.enabled = currentStatus;
}

+ (BOOL)hasRecognizedValidGesture:(UIGestureRecognizer *)recognizer
{
    return (recognizer.state == UIGestureRecognizerStateChanged || recognizer.state == UIGestureRecognizerStateBegan);
}

+ (void)setView:(UIView *)view blockJob:(void (^)())block{
    UIView * _blockView = [[UIView alloc] initWithFrame:view.frame];
    _blockView.autoresizingMask = UT_AUTORESIZINGFULL;
    [_blockView setBackgroundColor:[UIColor blackColor]];
    [_blockView setAlpha:0.7];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    indicator.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin);
    indicator.center = CGPointMake(view.frame.size.width /2, view.frame.size.height /2);
    [_blockView addSubview:indicator];
    [view addSubview:_blockView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        block();
        dispatch_async(dispatch_get_main_queue(), ^{
            [_blockView removeFromSuperview];
        });
    });
}

+ (CGPoint)positionInLayer:(CALayer *)layer pointForView:(CGPoint)point{
    CGFloat scale = [layer contentsScale];
    CGPoint anchorPoint = layer.anchorPoint;
    CGSize size = layer.bounds.size;
    
    return CGPointMake(point.x * scale + (anchorPoint.x * size.width), point.y * scale + (anchorPoint.y * size.height));
}

@end
