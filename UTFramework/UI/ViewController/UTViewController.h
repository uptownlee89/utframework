//
//  UTViewController.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef USE_UTContainer
#import "UTControlButton.h"
#endif

@protocol UTViewController <NSObject>
@required
- (id)initWithURL:(NSURL *)url;
- (id)initWithURL:(NSURL *)url parameters:(NSDictionary*)params;
@optional
- (void)checkToSouldPopViewController:(void (^)(BOOL))callback;
- (void)receiveData:(id)data;
@end

@interface UTViewController : UIViewController<UIPopoverControllerDelegate, UTViewController>

@property (nonatomic, readonly) NSURL *url;
@property (nonatomic, strong) id parameters;
@property (nonatomic, readonly) UIView *blockView;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, assign) BOOL hidesBackBarButton;
@property (nonatomic, readonly) NSArray *controlButtons;


- (id)addObserverForName:(NSString *)name object:(id)obj queue:(NSOperationQueue *)queue usingBlock:(void (^)(NSNotification *))block;
- (void)setBlock:(BOOL)block showIndicator:(BOOL)showIndicator text:(NSString *)text toView:(UIView *)view;
- (void)setBlock:(BOOL)block showIndicator:(BOOL)showIndicator toView:(UIView *)view;
- (void)setBlock:(BOOL)block;

#ifdef USE_UTContainer
- (void)addControlButton:(UTControlButton *)controlButton;
- (void)removeControlButtonForIdentifier:(NSString *)identifier;
- (void)removeControlButton:(UTControlButton *)controlButton;
- (UTControlButton *)controlButtonForIdentifier:(NSString *)identifier;
- (BOOL)isPopover;
#endif

@end
