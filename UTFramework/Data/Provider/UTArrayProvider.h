//
//  UTTable.h
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTDataProvider.h"

@protocol UTArrayProviderProtocol <NSObject>
- (NSUInteger)count;
- (id)objectAtIndex:(NSUInteger)index;
- (id)objectAtIndexedSubscript:(NSUInteger)index;
- (NSUInteger)indexOfObject:(id)anObject;
@end

@interface UTArrayProvider : UTDataProvider<UTArrayProviderProtocol>

- (id)initWithArray:(NSArray *)array;
- (id)initWithContentsOfFile:(NSString *)file;
- (id)initWithContentsOfFile:(NSString *)file inBundle:(NSBundle *)bundle;
@end
