//
//  NSNumber+humanize.h
//  MyMusicTaste
//
//  Created by 주영 이 on 13. 3. 13..
//  Copyright (c) 2013년 JJS Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Humanize)

- (NSString *)getLocalizedTimeString;
- (NSString *)getDate;
- (NSString *)distance;
- (NSString *)getStringNumberWithComma;
- (NSString *)secToDuration;
@end
