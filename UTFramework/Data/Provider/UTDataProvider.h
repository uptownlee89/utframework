//
//  UTDataProvider.h
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 6..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTObject.h"

@class UTDataProvider;
typedef enum {
    UTDataProviderInit,
    UTDataProviderLoading,
    UTDataProviderLoaded,
    UTDataProviderRefeshing,
    UTDataProviderGettingMore,
    UTDataProviderNotAvailable,
    UTDataProviderFailed
} UTDataProviderState;

typedef void (^UTDataProviderCallback)(id data, NSError *error);

@protocol UTDataProviderDelegate <NSObject>

@optional
- (void)dataProvider:(UTDataProvider *)provider changeState:(UTDataProviderState)toState from:(UTDataProviderState)fromState;

@end

@protocol UTDataProviderProtocol <NSObject>


@end

@interface UTDataProvider : UTObject{
@protected
    UTDataProviderState _currentState;
}
@property (nonatomic, readonly) UTDataProviderState state;
@property (nonatomic, weak) id<UTDataProviderDelegate> delegate;
@property (nonatomic, readonly) NSDate *lastUpdatedDate;
@property (nonatomic, assign) BOOL canRefresh;
@property (nonatomic, strong) id params;
@property (nonatomic, readonly) NSError *lastError;



- (id)initWithParams:(id)params;

- (void)refresh;
- (void)refreshWithControl:(id)control;
- (void)forceRefresh;
- (void)load;
- (void)reservedLoad;
- (void)getMore;

- (void)loadWithData:(id)data;
- (void)getMoreWithData:(id)data;

- (BOOL)canGetMore;
- (void)setNotAvailable;
- (void)setFailed;

- (void)asyncGetMoreDataWithCallback:(UTDataProviderCallback)callback;
- (void)asyncLoadWithCallback:(UTDataProviderCallback)callback;
- (void)filter:(id)filter;
@end
