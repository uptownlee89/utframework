//
//  UTRootViewController.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTRootViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UTDefine.h"
#import "UTApplication.h"



@interface UTRootViewController (){
    UIViewController *_currentViewController;
    UIAlertView *_alertView;
}

@property (nonatomic, copy) void (^alertCallback)(BOOL);
//@property (nonatomic, strong) NSMutableDictionary *callbackStore;

@property (nonatomic, copy) void (^alertTextFieldCallback)(UITextField*,BOOL);
@property (nonatomic, copy) void (^alertDoubleTextFieldCallback)(UITextField*,UITextField*,BOOL);
@property (nonatomic, copy) void (^alertMultiChoiceCallback)(NSInteger);
@property (nonatomic, copy) void (^asCallback)(NSString *, NSInteger );
@property (nonatomic, assign) CGFloat keyboardHeight;
@property (nonatomic, strong) UIView *logView;
@end

@implementation UTRootViewController

- (CGRect)_getScreenFrameForCurrentOrientation {
    return [self _getScreenFrameForOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (CGRect)_getScreenFrameForOrientation:(UIInterfaceOrientation)orientation {
    
    UIScreen *screen = [UIScreen mainScreen];
    CGRect fullScreenRect = screen.bounds;
    BOOL statusBarHidden = [UIApplication sharedApplication].statusBarHidden;
    
    //implicitly in Portrait orientation.
    if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
        CGRect temp = CGRectZero;
        temp.size.width = fullScreenRect.size.height;
        temp.size.height = fullScreenRect.size.width;
        fullScreenRect = temp;
    }
    
    if(!statusBarHidden){
        CGFloat statusBarHeight = 20;//Needs a better solution, FYI statusBarFrame reports wrong in some cases..
        fullScreenRect.size.height -= statusBarHeight;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        fullScreenRect.size.height += 20;
    }
    return fullScreenRect;
}

- (void)loadView{
    self.view = [[UIView alloc] initWithFrame:[self _getScreenFrameForCurrentOrientation]];
    self.view.autoresizingMask = UT_AUTORESIZINGFULL;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.callbackStore = [NSMutableDictionary dictionary];
    __weak UTRootViewController * wself = self;
    [self addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *notification) {
        UTRootViewController *sself = wself;
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGFloat height = keyboardSize.height > keyboardSize.width ? keyboardSize.width : keyboardSize.height;
        sself.keyboardHeight = height;
        if(sself.logView){
            sself.logView.center = sself.view.center;
            CGRect frame =  sself.logView.frame;
            frame.origin.y = frame.origin.y - (sself.keyboardHeight /2);
            sself.logView.frame = frame;
        }
    }];
    [self addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *notification) {
        UTRootViewController *sself = wself;
        sself.keyboardHeight = 0.0f;
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setViewController:(UIViewController *)mainViewController{
    if(_currentViewController){
        [self switchViewControllerFrom:_currentViewController to:mainViewController];
    }
    else{
        [self setFirstViewController:mainViewController];
    }
}

- (void)setFirstViewController:(UIViewController *)mainViewController{

    [self addChildViewController:mainViewController];
    _currentViewController = mainViewController;
    CGRect frame = self.view.frame;
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !self.manageStatusBar){
        frame.origin.y = 20;
        frame.size.height -= 20;
    }
    mainViewController.view.frame = frame;
//    //NSLog(@"%@ %@",NSStringFromCGRect(mainViewController.view.frame), NSStringFromCGRect(self.view.frame));
    [self.view addSubview:mainViewController.view];
    [mainViewController didMoveToParentViewController:self];
}
- (void)switchViewControllerFrom:(UIViewController *)oldViewController to:(UIViewController *)newViewController{
    
    [oldViewController willMoveToParentViewController:nil];                        // 1
    [self addChildViewController:newViewController];
    
    newViewController.view.frame = oldViewController.view.frame;
    CGRect frame = self.view.frame;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !self.manageStatusBar){
        frame.origin.y = 20;
        frame.size.height -= 20;
    }
    oldViewController.view.frame = frame;
    [self transitionFromViewController: oldViewController toViewController: newViewController   // 3
                              duration: 0.4 options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{
                                oldViewController.view.frame = self.view.frame;
                                if(self.blockView){
                                    [self.view bringSubviewToFront:self.blockView];
                                }
                            }
                            completion:^(BOOL finished) {
//                                //NSLog(@"%@ %@",NSStringFromCGRect(newViewController.view.frame), NSStringFromCGRect(self.view.frame));
                                [oldViewController removeFromParentViewController];
                                [newViewController didMoveToParentViewController:self];
                                _currentViewController = newViewController;
                            }];
}


- (void)dealloc{
    _currentViewController = nil;
}



-(NSUInteger)supportedInterfaceOrientations{
    return [_currentViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate{
    return [_currentViewController shouldAutorotate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return [_currentViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}


- (UIViewController *)currentViewController{
    return _currentViewController;
}

- (UINavigationController *)navigationController{
    if([_currentViewController isKindOfClass:[UINavigationController class]])
        return (UINavigationController *)_currentViewController;
    return _currentViewController.navigationController;
}

- (void)confirmDialogWithTitle:(NSString *)title message:(NSString *)message confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle callback:( void (^)(BOOL))callback{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:confirmTitle, nil];
//    //NSLog(@"%@", alertView.);
//    [self.callbackStore setObject:[callback copy] forKey:[alertView]];
    self.alertCallback = callback;
    [alertView show];
}

- (void)textFieldDialogWithTitle:(NSString *)title message:(NSString*)message textFieldContent:(NSString*)content confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle type:(UIKeyboardType)keyboardType callback:( void (^)(UITextField*, BOOL))callback{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:confirmTitle, nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].text = content;
    [alertView textFieldAtIndex:0].keyboardType = keyboardType;
    [alertView textFieldAtIndex:0].delegate = self;
    [alertView textFieldAtIndex:0].returnKeyType = UIReturnKeyDone;
    self.alertTextFieldCallback = callback;
    
    _alertView = alertView;
    [alertView show];
}



- (void)doubleTextFieldDialogWithTitle:(NSString *)title message:(NSString*)message textFieldContent:(NSString*)content0 extraTextFieldContent:(NSString*)content1 confirmTitle:(NSString *)confirmTitle cancelTitle:(NSString *)cancelTitle type:(UIKeyboardType)keyboardType callback:( void (^)(UITextField*, UITextField*, BOOL))callback{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:confirmTitle, nil];
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [[alertView textFieldAtIndex:1] setSecureTextEntry:NO];
    [alertView textFieldAtIndex:0].placeholder = content0;
    [alertView textFieldAtIndex:0].keyboardType = keyboardType;
    [alertView textFieldAtIndex:0].delegate = self;
    [alertView textFieldAtIndex:0].returnKeyType = UIReturnKeyDone;
    
    [alertView textFieldAtIndex:1].placeholder = content1;
    [alertView textFieldAtIndex:1].keyboardType = keyboardType;
    [alertView textFieldAtIndex:1].delegate = self;
    [alertView textFieldAtIndex:1].returnKeyType = UIReturnKeyDone;
    self.alertDoubleTextFieldCallback = callback;
    
    _alertView = alertView;
    [alertView show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if(self.alertDoubleTextFieldCallback)
        return NO;
    if(_alertView){
        [_alertView dismissWithClickedButtonIndex:0 animated:YES];
        self.alertTextFieldCallback(textField, true);
    }
    
    _alertView = nil;
    self.alertTextFieldCallback = nil;
    
    return NO;
}

- (void)multiChoiceDialogWithTitle:(NSString *)title message:(NSString *)message titles:(NSArray*)titles cancelTitle:(NSString *)cancelTitle callback:( void (^)(NSInteger index))callback{

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:nil];
    
    for(int i = 0; i < [titles count]; i++){
        [alertView addButtonWithTitle:[titles objectAtIndex:i]];
    }
    
    self.alertMultiChoiceCallback = callback;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:NO];
    if(self.alertCallback){
        if(buttonIndex == 0){
            self.alertCallback(false);
        }else{
            self.alertCallback(true);
        }
        
        self.alertCallback = nil;
    }else if(self.alertTextFieldCallback){
        if(buttonIndex == 0){
            self.alertTextFieldCallback([alertView textFieldAtIndex:0], false);
        }else{
            self.alertTextFieldCallback([alertView textFieldAtIndex:0], true);
        }
        
        self.alertTextFieldCallback = nil;
    }else if(self.alertMultiChoiceCallback){
        self.alertMultiChoiceCallback(buttonIndex);
        self.alertMultiChoiceCallback = nil;
    }
    else if(self.alertDoubleTextFieldCallback){
        if(buttonIndex == 0){
            self.alertDoubleTextFieldCallback([alertView textFieldAtIndex:0], [alertView textFieldAtIndex:1], false);
        }else{
            self.alertDoubleTextFieldCallback([alertView textFieldAtIndex:0], [alertView textFieldAtIndex:1], true);
        }
        self.alertDoubleTextFieldCallback = nil;
    }

}
- (void)actionSheetWithTitle:(NSString *)title cancelTitle:(NSString *)cancelTitle destructiveTitle:(NSString *)destructiveTitle others:(NSArray *)others callback:( void (^)(NSString *title, NSInteger index))callback{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [as setTitle:title];
    int count = 0;
    if(destructiveTitle){
        [as addButtonWithTitle:destructiveTitle];
        [as setDestructiveButtonIndex:count];
        count ++;
    }
    for (NSString *aa in others){
        [as addButtonWithTitle:aa];
        count ++;
    }
    if(cancelTitle){
        [as addButtonWithTitle:cancelTitle];
        [as setCancelButtonIndex:count];
        count ++;
    }
    [as showInView:self.view];
    self.asCallback = callback;
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(self.asCallback){
        NSString * title = [actionSheet buttonTitleAtIndex:buttonIndex];
        self.asCallback(title, buttonIndex);
    }
    self.asCallback = nil;
    
}

- (void)showLogView:(NSString *)message withFont:(UIFont *)font{
    [self showLogView:message withFont:font duration:1.5];
}

- (void)forceHideLogView{
    [self.logView removeFromSuperview];
    self.logView = nil;
}

- (void)showLogView:(NSString *)message withFont:(UIFont *)font duration:(CGFloat)duration{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.numberOfLines = 0;
    [label setFont:font];
    [label setText:message];
    [label setTextColor:[UIColor whiteColor]];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    CGSize size = [label sizeThatFits:CGSizeMake(self.view.frame.size.width - 60, CGFLOAT_MAX)];
    label.frame = CGRectMake(0, 0, size.width, size.height);
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 50, size.height + 20)];
    [view setBackgroundColor:[UIColor clearColor]];
    UIView *backgroundView = [[UIView alloc] initWithFrame:view.frame];
    [backgroundView setBackgroundColor:[UIColor blackColor]];
    [backgroundView setAlpha:0.7];
    
    backgroundView.layer.cornerRadius = 8;
    view.autoresizesSubviews = UT_AUTORESIZINGFULL;
    [view addSubview:backgroundView];
    label.autoresizesSubviews = UIViewAnimationTransitionNone;
    label.center = view.center;
    [view addSubview:label];
    if(self.keyboardHeight == 0.0f){
        view.center = self.view.center;
    }
    else{
        view.center = self.view.center;
        CGRect frame =  view.frame;
        frame.origin.y = frame.origin.y - (self.keyboardHeight /2);
        view.frame = frame;
    }
    [self.view addSubview:view];
    self.logView = view;
    double delayInSeconds = duration;
    if(duration == -1){
    }
    else{
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [UIView animateWithDuration:0.5 animations:^{
                view.alpha = 0.0;
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
                self.logView = nil;
            }];
        });
    }
}

- (void)showLogView:(NSString *)message{
    [self showLogView:message withFont:[UIFont systemFontOfSize:15]];
}

- (void)showLogView:(NSString *)message duration:(CGFloat)duration{
    [self showLogView:message withFont:[UIFont systemFontOfSize:15] duration:duration];
}


@end
