//
//  UTMasonryViewController.m
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 16..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTMasonryViewController.h"
#import "UTMasonryViewCell.h"
#import "ODRefreshControl.h"
#import "UTUILibrary.h"

@interface UTMasonryViewControllerHelper : NSObject

- (id)initWithController:(UTMasonryViewController *)controller;
- (void)refresh;
@end

@implementation UTMasonryViewControllerHelper{
    __weak UTMasonryViewController *controller_;
}


- (id)initWithController:(UTMasonryViewController *)controller{
    if(self = [super init]){
        controller_ = controller;
    }
    return self;
}
- (void)refresh{
    [controller_.dataProvider refresh];
}

@end

@interface UTMasonryViewController ()

@property (nonatomic, strong) ODRefreshControl *refreshControl;
@end

@implementation UTMasonryViewController{
    UTMasonryViewControllerHelper *observer_;
}


- (id)initWithArrayProvider:(UTDataProvider<UTArrayProviderProtocol> *)dataProvider{
    self = [super initWithNibName:nil bundle:nil];
    if(self){
        self.dataProvider = dataProvider;
    }
    return self;
}


- (void)loadDefaultView{
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.autoresizingMask = UTAUTORESIZINGFULL;
    self.masonryView = [[UTMasonryView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.masonryView.autoresizingMask = UTAUTORESIZINGFULL;
    [self.view addSubview:self.masonryView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _masonryView.delegate = self;
    _masonryView.dataSource = self;
    
	// Do any additional setup after loading the view.
}

- (void)setDataProvider:(UTDataProvider<UTArrayProviderProtocol> *)dataProvider{
    _dataProvider = dataProvider;
    _dataProvider.delegate = self;
    
    if([_dataProvider canRefresh]){
        if(!observer_){
            observer_ = [[UTMasonryViewControllerHelper alloc] initWithController:self];
        }
        self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.masonryView];
        [self.refreshControl addTarget:observer_ action:@selector(refreshWithControl:) forControlEvents:UIControlEventValueChanged];
        [self.refreshControl setEnabled:NO];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload{
    [super viewDidUnload];
    [self.refreshControl removeTarget:observer_ action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSTimeInterval passed = [[NSDate date] timeIntervalSinceDate:self.dataProvider.lastUpdatedDate];
    if(passed > 300){
        [self.dataProvider refresh];
    }
    else{
        [self.masonryView reloadData];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)dataProvider:(UTDataProvider *)provider changeState:(UTDataProviderState)toState from:(UTDataProviderState)fromState{
    [self.refreshControl setEnabled:YES];
    
    if(toState == UTDataProviderLoaded){
        if(fromState == UTDataProviderGettingMore){
            [self.masonryView reloadDataAdded];
        }
        else{
            [self.masonryView reloadData];
        }
        [self setBlock:NO];
        if(fromState == UTDataProviderRefeshing){
            [self.refreshControl endRefreshing];
        }
    }
    else if(fromState == UTDataProviderInit ){
        [self setBlock:YES];
    }
    if(toState == UTDataProviderNotAvailable && self.notAvailableView){
        self.notAvailableView.frame = CGRectMake(0, -self.masonryView.contentInset.top + self.masonryView.scrollIndicatorInsets.top, self.masonryView.frame.size.width, self.masonryView.frame.size.height);
        self.masonryView.coverView = self.notAvailableView;
        [self setAvaliable:NO];
    }
    else if(toState == UTDataProviderFailed && self.failedView){
        self.failedView.frame = CGRectMake(0, -self.masonryView.contentInset.top + self.masonryView.scrollIndicatorInsets.top, self.masonryView.frame.size.width, self.masonryView.frame.size.height);
        [self.masonryView addSubview:self.failedView];
        [self setAvaliable:NO];
    }
    else if(toState != UTDataProviderRefeshing){
        [self.failedView removeFromSuperview];
        self.masonryView.coverView = nil;
        [self setAvaliable:YES];
    }
}

- (void)setAvaliable:(BOOL)isAvailable{
    
}


- (void)scrollViewDidScroll: (UIScrollView*)scroll {
    if([self.dataProvider canGetMore] && scroll == self.masonryView){
        // UITableView only moves in one direction, y axis
        NSInteger currentOffset = scroll.contentOffset.y;
        NSInteger maximumOffset = MIN(scroll.contentSize.height - scroll.frame.size.height,[(UTMasonryView*)scroll minHeight] - scroll.frame.size.height);
        
        // Change 10.0 to adjust the distance from bottom
        if (maximumOffset - currentOffset <= 320.0) {
            [self.dataProvider getMore];
        }
    }
}

- (NSUInteger)masonryViewNumberOfCells:(UTMasonryView *)masonryView{
    return [self.dataProvider count];
}

- (NSUInteger)masonryViewNumberOfColumns:(UTMasonryView *)masonryView{
    return 2;
}

- (UTMasonryViewCell *)masonryView:(UTMasonryView *)masonryView atIndex:(NSUInteger)index{
    
    UTMasonryViewCell *cell = [NSClassFromString([self masonryCellName:masonryView atIndex:index]) getReusedCellFromMasonryView:masonryView withReusedentifier:[self masonryCellName:masonryView atIndex:index]];
    [cell applyData:[self.dataProvider objectAtIndex:index]];
    return cell;
}

- (NSString *)masonryCellName:(UTMasonryView *)tableView atIndex:(NSUInteger)index{
    return @"UTMasonryViewCell";
}

- (CGFloat)masonryView:(UTMasonryView *)masonryView heightForCellAtIndex:(NSUInteger)index{
    return [NSClassFromString([self masonryCellName:masonryView atIndex:index]) getHeightWithWidth:masonryView.itemWidth forData:[self.dataProvider objectAtIndex:index]];
}

- (UIScrollView *)hiddenableNavigationTargetScrollView{
    return self.masonryView;
}
- (BOOL)useHiddenableNavigation{
    return YES;
}
@end
