//
//  UTContainerViewController.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTViewController.h"
#import "UTNavigationCenter.h"
#import "UTControlBar.h"
#import "UTUILibrary.h"


@interface UTContainerViewController : UTViewController
@property (nonatomic, weak) id<UTViewControllerFactory> datasource;
@property (nonatomic, readonly) UTViewController *topViewController;
@property (nonatomic, readonly) UTControlBar *controlBar;
@property (nonatomic, assign) BOOL hidesBackButton;
@property (nonatomic, assign) BOOL controlBarHidden;
@property (nonatomic, assign) CGFloat controlBarHiddenRate;
@property (nonatomic, readonly) NSArray *viewControllers;
@property (nonatomic, strong) UIView *titleView;

- (void)setControlBarHidden:(BOOL)hidden animated:(BOOL)animated;





/* 상속되어도 의미있음 */
- (id)initWithRootViewController:(UTViewController *)vc controlBarClass:(Class)theClass;
- (id)initWithRootViewController:(UTViewController *)vc;
- (void)setRootViewController:(UTViewController *)vc;
- (void)pushViewController:(UTViewController *)viewController animated:(BOOL)animated;
- (UTViewController *)popViewControllerAnimated:(BOOL)animated;
- (NSArray *)popToViewController:(UTViewController *)viewController animated:(BOOL)animated;
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated;
- (NSUInteger)numberOfPushedViewControllers;
- (void)setControlButtons:(NSArray *)items;
- (void)setControlButtons:(NSArray *)items animated:(BOOL)animated;
- (void)didTopViewControllerChanged;
@end
