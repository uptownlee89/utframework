//
//  UTMutableObject.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UTBoolean NSNumber*
#define UTInteger NSNumber*
#define UTDouble NSNumber*

@protocol UTMutableObject<NSObject>

- (NSUInteger)count;
- (id)objectForKey:(id)aKey;- (NSEnumerator *)keyEnumerator;
- (void)removeObjectForKey:(id)aKey;
- (void)setObject:(id)anObject forKey:(id)aKey;
- (void)realSetObject:(id)object forKey:(id)key;
- (void)addEntriesFromDictionary:(NSDictionary *)dictionary;

- (void)save;
- (void)load;
- (void)save:(NSString *)filename;
- (void)load:(NSString *)filename;
- (void)connectWithFile:(NSString*)filename;
@end


@interface UTMutableObject : NSMutableDictionary<UTMutableObject>
+ (instancetype)object;
+ (instancetype)objectFromDictionary:(NSDictionary*)dictionary;
+ (instancetype)copiedObjectFromDictionary:(NSDictionary*)jsonDictionary;
- (void)addOperator:(NSString *)opeartorCls;
- (void)removeOperator:(NSString *)opeartorCls;
- (id)filteredObject:(NSArray *)filter;
@end
