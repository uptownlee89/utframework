//
//  UTTableBasedViewController.h
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 6..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTViewController.h"
#import "UTTableProvider.h"
#import "UTTableView.h"

@interface UTTableViewController : UTViewController <UITableViewDataSource, UTTableViewDelegate, UTDataProviderDelegate>

@property (nonatomic, strong) IBOutlet UTTableView *tableView;
@property (nonatomic, strong) UTDataProvider<UTTableProviderProtocol> *dataProvider;
@property (nonatomic, strong) IBOutlet UIView *notAvailableView;
@property (nonatomic, strong) IBOutlet UIView *failedView;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
- (id)initWithDataProvider:(UTDataProvider<UTTableProviderProtocol> *)dataProvider;
- (void)setAvaliable:(BOOL)isAvailable;
- (UTDataProvider<UTTableProviderProtocol> *)dataProviderForTable:(UIScrollView *)tableView;
- (void)loadDefaultView;
@end
