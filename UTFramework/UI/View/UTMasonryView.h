//
//  UTMasonryView.h
//  UTFramework
//
//  Created by 주영 이 on 13. 2. 18..
//  Copyright (c) 2013년 Juyoung.me All rights reserved.
//

#import <UIKit/UIKit.h>
/* TODO : dequeueReusableCellWithReuseIdentifier */
@class UTMasonryViewCell;
@class UTMasonryView;

@protocol UTMasonryViewDataSource <NSObject>

- (NSUInteger)masonryViewNumberOfCells:(UTMasonryView *)masonryView;
- (UTMasonryViewCell *)masonryView:(UTMasonryView *)masonryView atIndex:(NSUInteger)index;
- (NSString *)masonryCellName:(UTMasonryView *)tableView atIndex:(NSUInteger)index;

@end

@protocol UTMasonryViewDelegate <UIScrollViewDelegate>

- (CGFloat)masonryView:(UTMasonryView *)masonryView heightForCellAtIndex:(NSUInteger)index;
- (NSUInteger)masonryViewNumberOfColumns:(UTMasonryView *)masonryView;
@optional
- (void)masonryView:(UTMasonryView *)masonryView didReceivedData:(id)data;
- (void)masonryView:(UTMasonryView *)masonryView didSelectCellAtIndex:(NSUInteger)index;

@end

@interface UTMasonryView : UIScrollView <UIGestureRecognizerDelegate>

@property (nonatomic, weak) id<UTMasonryViewDataSource> dataSource;
@property (nonatomic, weak) id<UTMasonryViewDelegate> delegate;
@property (nonatomic, assign) CGFloat widthSpacing;
@property (nonatomic, assign) CGFloat heightSpacing;
@property (nonatomic, assign) CGFloat firstColumnMargin;
@property (nonatomic, readonly) CGFloat itemWidth;
@property (nonatomic, readonly) CGFloat minHeight;
@property (nonatomic, strong) UIView *masonryTopView;
@property (nonatomic, strong) UIView *coverView;

- (UTMasonryViewCell *)cellForItemAtIndex:(NSInteger)position;
- (NSArray *)visibleCells;
- (NSArray *)visibleIndexes;
- (void)reloadData;
- (void)reloadDataAdded;
- (UTMasonryViewCell *)dequeueReusableCellWithReuseIdentifier:(NSString *)reuseIdentifier;


@end
