//
//  UTRestCredential.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 17..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTObject.h"

@class UTRestCredential;
@class AFHTTPRequestOperation;

typedef void (^UTRestCredentialCallback)(AFHTTPRequestOperation* operation, UTRestCredential *credential, NSError *error);
@interface UTRestCredential : UTObject
@property (nonatomic, readonly) NSString *access_token;
@property (nonatomic, readonly) NSString *refresh_token;
@property (nonatomic, readonly) NSString *token_type;
@property (nonatomic, readonly) NSDate *expire;
@property (nonatomic, readonly) BOOL is_expired;

- (id)initWithCredential:(NSDictionary *)credential;

+ (void)setDefaultCredential:(UTRestCredential *)credential;
+ (UTRestCredential *)defaultCredential;
+ (void)getCredential:(NSString *)tokenUrl parameters:(NSDictionary *)paramemters callback:(UTRestCredentialCallback)callback;
@end
