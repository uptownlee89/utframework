//
//  UTContainControlView.h
//  MMTFramework
//
//  Created by Juyoung Lee on 13. 9. 25..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTControlButton.h"
#import "UTViewController.h"


typedef enum {
    UTViewTransitionTypeNone,
    UTViewTransitionTypePush,
    UTViewTransitionTypePop,
} UTViewTransitionType;

@class UINavigationBar;
@class UTContainerViewController;
@interface UTControlBar : UIView
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, readonly) UIView *defaultView;
@property (nonatomic, readonly) UTControlButton *backButton;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL isBackButtonHidden;
@property (nonatomic, readonly) NSArray *controlButtons;
@property (nonatomic, readonly) UTContainerViewController* containerViewController;


- (id)initWithContainerViewController:(UTContainerViewController *)containerVC;
- (void)setControlButtons:(NSArray *)items animated:(BOOL)animated;



/* make friend with catagory */
- (void)willTransitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type;
- (void)transitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type;
- (void)didTransitionFrom:(UTViewController *)fromViewController to:(UTViewController *)toViewController transitionType:(UTViewTransitionType)type;

@end
