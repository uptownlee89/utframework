//
//  UIImage+UTFramework.h
//  UTFramework
//
//  Created by Lee, Juyoung on 2014. 11. 5..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (UTFramework)

+ (void)getImageFromURL:(NSURL *)url callback: (void (^)(UIImage*))callback;
@end
