//
//  UTDataProvider+Protected.h
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 4..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTDataProvider.h"

@interface UTDataProvider (Protected)
- (void)_setData:(id)data;
- (void)_getMoreData:(id)data;
- (void)_statusChangeTo:(UTDataProviderState)to;
@end
