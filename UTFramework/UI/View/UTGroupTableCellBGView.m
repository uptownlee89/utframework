//
//  MMTThreeColoredLineView.m
//  UTFramework
//
//  Created by 주영 이 on 13. 8. 22..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UTGroupTableCellBGView.h"
#import "UTDefine.h"

@implementation UTGroupTableCellBGView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void) drawRect: (CGRect) rect
{
    CGFloat factor = [[UIScreen mainScreen] scale] == 2.0 ? 0.5f : 1.0f;
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    
//    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
//    CGContextFillRect (context, CGRectMake(0,0, self.bounds.size.width, self.bounds.size.height));
    if(self.position == GROUPTABLECELL_TOP || self.position == GROUPTABLECELL_SINGLE){
        CGContextSetShouldAntialias(context, NO);
        CGContextSetLineWidth(context, factor);
        CGContextSetStrokeColorWithColor(context,  UIColorFromRGB(0xc8c8c8).CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 0, factor);
        CGContextAddLineToPoint(context, self.bounds.size.width, factor);
        CGContextStrokePath(context);
    }
    if(self.position == GROUPTABLECELL_BOTTOM || self.position == GROUPTABLECELL_SINGLE){
        CGContextSetShouldAntialias(context, NO);
        CGContextSetLineWidth(context, factor);
        CGContextSetStrokeColorWithColor(context, UIColorFromRGB(0xc8c8c8).CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 0, self.bounds.size.height-factor);
        CGContextAddLineToPoint(context, self.bounds.size.width, self.bounds.size.height-factor);
        CGContextStrokePath(context);
    }
    if(self.position == GROUPTABLECELL_BOTTOM || self.position == GROUPTABLECELL_MIDDLE){
        CGContextSetShouldAntialias(context, NO);
        CGContextSetLineWidth(context, factor);
        CGContextSetStrokeColorWithColor(context, UIColorFromRGB(0xc8c8c8).CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 15, factor);
        CGContextAddLineToPoint(context, self.bounds.size.width, factor);
        CGContextStrokePath(context);
    }
    CGContextRestoreGState(context);
}
- (void)setPosition:(GroupTableCellPosition)position{
    _position = position;
    [self setNeedsDisplay];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self setNeedsDisplay];
}
@end
