//
//  UTAPICenter.m
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 5..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTRestCenter.h"
//#import "AFJSONRequestOperation.h"
#import "UTApplication.h"
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import "UTMutableObject.h"
#import <objc/runtime.h>
#import <objc/objc.h>
#import "UTUtils.h"
#import "UTRestCredential.h"
#import "UTDefine.h"

@implementation UTJSONResponseSerializerWithData

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    if (![self validateResponse:(NSHTTPURLResponse *)response data:data error:error]) {
        if (*error != nil) {
            NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
            userInfo[JSONResponseSerializerWithDataKey] = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSError *newError = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
            (*error) = newError;
        }
        
        return (nil);
    }
    
    return ([super responseObjectForResponse:response data:data error:error]);
}

@end

@interface UTRestCenter ()
@property (nonatomic, strong) UTRestCredential *restCredential;

@end

@implementation UTRestCenter

+ (UTRestCenter *)defaultCenter{
    
    static UTRestCenter *defaultCenter = nil;
    if (nil != defaultCenter) {
        return defaultCenter;
    }
    
    static dispatch_once_t pred;        // Lock
    dispatch_once(&pred, ^{             // This code is called at most once per app
        defaultCenter = [[UTRestCenter alloc] initWithBaseURL:[NSURL URLWithString:app_string(@"API_BASE_PATH")]];
    });
    [defaultCenter setRestCredential:[UTRestCredential defaultCredential]];
    
    return defaultCenter;
}

- (id)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if(self){
        self.responseSerializer = [UTJSONResponseSerializerWithData serializer];
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    }
    return self;
}

- (void)setRestCredential:(UTRestCredential *)restCredential{
    _restCredential = restCredential;
    if(restCredential){
        [self.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@", restCredential.token_type, restCredential.access_token] forHTTPHeaderField:@"Authorization"];
    }
}


- (AFHTTPRequestOperation *)_HTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = self.responseSerializer;
    operation.shouldUseCredentialStorage = self.shouldUseCredentialStorage;
    operation.credential = self.credential;
    operation.securityPolicy = self.securityPolicy;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"%@, %@", operation, responseObject);
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"%@, %@", operation, error);
        failure(operation, error);
    }];
    
    return operation;
}

- (AFHTTPRequestOperation *)POSTUnsafe:(NSString *)URLString
                       parameters:(NSDictionary *)parameters
                          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self _HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)HTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = self.responseSerializer;
    operation.shouldUseCredentialStorage = self.shouldUseCredentialStorage;
    operation.credential = self.credential;
    operation.securityPolicy = self.securityPolicy;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation_, id responseObject) {
        success(operation_, responseObject);
//        [UTRestCredential getCredential:app_string(@"TOKEN_URL") parameters:@{@"client_id": app_string(@"CLIENT_ID"), @"client_secret": app_string(@"CLIENT_SECRET"), @"refresh_token": self.restCredential.refresh_token, @"grant_type": @"refresh_token"}  callback:^(AFHTTPRequestOperation *operation, UTRestCredential *credential, NSError *error) {
//            if(error){
//                failure(operation, error);
//            }
//            else{
//                [UTRestCredential setDefaultCredential:credential];
//                NSMutableURLRequest *newRequest = [[NSMutableURLRequest alloc] init];
//                [newRequest setURL:operation_.request.URL];
//                [newRequest setHTTPMethod:operation_.request.HTTPMethod];
//                [newRequest setHTTPBody:operation_.request.HTTPBody];
//                //NSLog(@"%@", operation_.request.HTTPBody);
//                [newRequest setAllHTTPHeaderFields:self.requestSerializer.HTTPRequestHeaders];
//                [self.operationQueue addOperation:[self _HTTPRequestOperationWithRequest:newRequest success:success failure:failure]];
//            }
//        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        id responseObject = [error.userInfo objectForKey:@"JSONResponseSerializerWithDataKey"];
        if(operation.response.statusCode == 401){
            if(self.restCredential.refresh_token){
                [UTRestCredential getCredential:app_string(@"TOKEN_URL") parameters:@{@"client_id": app_string(@"CLIENT_ID"), @"client_secret": app_string(@"CLIENT_SECRET"), @"refresh_token": self.restCredential.refresh_token, @"access_token": self.restCredential.access_token, @"grant_type": @"refresh_token"}  callback:^(AFHTTPRequestOperation *operation, UTRestCredential *credential, NSError *error) {
                    
                    if(error){
                        if(self.credentialInvalidCallback){
                            self.credentialInvalidCallback(error);
                        }
//                        if(operation.response.statusCode == 400){
//                            [UTRestCredential setDefaultCredential:nil];
////                            //NSLog(@"%@", @"asd");
//                        }
                        failure(operation, error);
                    }
                    else{
                        [UTRestCredential setDefaultCredential:credential];
                        NSMutableURLRequest *newRequest = [[NSMutableURLRequest alloc] init];
                        [newRequest setURL:request.URL];
                        [newRequest setHTTPMethod:request.HTTPMethod];
                        [newRequest setHTTPBody:request.HTTPBody];
                        [newRequest setHTTPBodyStream:request.HTTPBodyStream];
//                        //NSLog(@"%@", request.HTTPBody);
                        [newRequest setAllHTTPHeaderFields:self.requestSerializer.HTTPRequestHeaders];
                        
                        [self.operationQueue addOperation:[self _HTTPRequestOperationWithRequest:newRequest success:success failure:failure]];
                    }
                }];
            }
            else{
                failure(operation, error);
            }
        }
        else{
            failure(operation, error);
        }
    }];
    
    return operation;
}


@end
