//
//  UTObject.m
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTObject.h"
#import "UTApplication.h"
@interface UTObject () {
    NSMutableArray *_observers;
}
@end
@implementation UTObject


- (id)init{
    self = [super init];
    if(self){
        _observers = [NSMutableArray array];
    }
    return self;
}

- (void)dealloc{
    for (id observer in _observers){
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }
    _observers = nil;
}

+ (instancetype)singleton{
    return [[UTApplication sharedApplication] getSingletonedObjectForClass:[self class]];
}

- (id)addObserverForName:(NSString *)name object:(id)obj queue:(NSOperationQueue *)queue usingBlock:(void (^)(NSNotification *))block{
    if(queue == nil)
        queue = [NSOperationQueue mainQueue];
    id observer = [[NSNotificationCenter defaultCenter] addObserverForName:name object:obj queue:queue usingBlock:block];
    [_observers addObject:observer];
    return observer;
}
@end
