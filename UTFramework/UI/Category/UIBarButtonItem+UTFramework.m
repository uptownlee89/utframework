//
//  UIBarButtonItem+UTFramework.m
//  UTFramework
//
//  Created by Juyoung Lee on 2014. 1. 2..
//  Copyright (c) 2014년 Juyoung.me. All rights reserved.
//

#import "UIBarButtonItem+UTFramework.h"
#import "NSObject+UTFrameworkProtected.h"
#import <objc/runtime.h>

@implementation UIBarButtonItem (UTFramework)

- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem callback:(UTActionBlock)action {
    self = [self initWithBarButtonSystemItem:systemItem target:self action:@selector(_invokeBlock:)];
    [self copyAssociateValue:action withKey:kUTFrameworkBlockKey];
    return self;
}

- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action {
    self = [self initWithImage:image style:style target:self action:@selector(_invokeBlock:)];
    [self copyAssociateValue:action withKey:kUTFrameworkBlockKey];
    return self;
}

- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action {
    self = [self initWithTitle:title style:style target:self action:@selector(_invokeBlock:)];
    [self copyAssociateValue:action withKey:kUTFrameworkBlockKey];
    return self;
}

- (id)initWithImage:(UIImage *)image landscapeImagePhone:(UIImage *)landscapeImagePhone style:(UIBarButtonItemStyle)style callback:(UTActionBlock)action{
    self = [self initWithImage:image landscapeImagePhone:landscapeImagePhone style:style target:self action:@selector(_invokeBlock:)];
    [self copyAssociateValue:action withKey:kUTFrameworkBlockKey];
    return self;
}
@end
