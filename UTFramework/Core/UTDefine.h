//
//  UTDefine.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#ifndef UTFramework_UTDefine_h
#define UTFramework_UTDefine_h

// //NSLog is printed when Debug only
#ifdef DEBUG
#else
#define NSLog(fmt, ...)
#endif


#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define CURRENT_LOCALE2 [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
#define STR_SAME(str1, str2) [str1 isEqualToString str2]


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE4INCH ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0 && [UIScreen mainScreen].bounds.size.width == 320.0)
#define IS_IPHONE6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 667.0 && [UIScreen mainScreen].bounds.size.width == 375.0)
#define IS_IPHONE6P ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1104.0 && [UIScreen mainScreen].bounds.size.width == 621.0)




#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define UT_NAVIGATION_BAR_HEIGHT_IPHONE_PORTRAIT 44.0f
#define UT_NAVIGATION_BAR_HEIGHT_IPHONE_LANDSCAPE 32.0f

#ifdef __IPHONE_7_0
#define UT_STATUS_BAR_HEIGHT 20.0f
#else
#define UT_STATUS_BAR_HEIGHT 0.0f
#endif

#define UT_NAVIGATION_BAR_HEIGHT_IPAD_PORTRAIT 44.0f
#define UT_NAVIGATION_BAR_HEIGHT_IPAD_LANDSCAPE 44.0f

#define UT_AUTORESIZINGFULL (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin)
#define UT_AUTORESIZING_NOMARGIN (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin)


#define HEIGHT_FOR_NAVIGATIONBAR(isPortait) (isPortait?(IS_IPHONE ? UT_NAVIGATION_BAR_HEIGHT_IPHONE_PORTRAIT : UT_NAVIGATION_BAR_HEIGHT_IPAD_PORTRAIT):(IS_IPHONE ? UT_NAVIGATION_BAR_HEIGHT_IPHONE_LANDSCAPE : UT_NAVIGATION_BAR_HEIGHT_IPAD_LANDSCAPE))

#define Y_ENDPOINT_OF_NAVIGATIONBAR(isPortait) HEIGHT_FOR_NAVIGATIONBAR(isPortait)+(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")? UT_STATUS_BAR_HEIGHT : 0.0f)
#define Y_STARTPOINT_OF_NAVIGATIONBAR (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")? UT_STATUS_BAR_HEIGHT : 0.0f)

#define UT_MEMCACHED_COUNT 250
#define UT_USERDEFAULT_FILECACHESIZE @"UT_filecachesize"
#define UT_FILECACHE_RETENTION_DAYS 15

#define matched(type, str) ([[type lowercaseString] isEqualToString:[str lowercaseString]])
#define _(x) [[NSBundle mainBundle] localizedStringForKey:(x) value:@"" table:nil]

#define _int(x) [x intValue]
#define _str(x) [x stringValue]
#define _bool(x) [x boolValue]
#define _double(x) [x doubleValue]

#define UT_SYSTEMUSER_FILE @"UT_SYSTEMUSER_FILE.UTC"
#define UT_APPLICATION_FILE @"UT_APPLICATION_FILE.UTC"

#define SDK_VERSION 0.1

#define app_font(key) [[UTApplication sharedApplication] fontForKey:key]
#define app_image(key) [[UTApplication sharedApplication] imageForKey:key]
#define app_color(key) [[UTApplication sharedApplication] colorForKey:key]
#define app_string(key) _([[UTApplication sharedApplication] stringForKey:key])
#define app_double(key) [[UTApplication sharedApplication] doubleForKey:key]
#define app_integer(key) [[UTApplication sharedApplication] integerForKey:key]

#endif