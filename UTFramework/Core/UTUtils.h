//
//  UTUtils.h
//  UTFramework
//
//  Created by Juyoung Lee on 2013. 12. 28..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTUtils : NSObject

+ (void)shuffleWithFirstIndex:(NSMutableArray *)array  first:(NSInteger)first;
+ (void)shuffle:(NSMutableArray *)array;

+ (NSString *)stringWithUTF8PercentEscape:(NSString *)str;
+ (NSString*)decodeFromPercentEscapeString:(NSString *) str;
+ (NSString *)queryString:(NSDictionary *)dict;
+ (NSString *)machineName;
+ (NSString *)applicationHiddenDocumentsDirectory;
+ (NSString *)applicationHiddenCacheDocumentsDirectory;
+ (NSString *)applicationDocumentsDirectory;


+ (NSString *)deviceIdentifier;
+ (NSString *)bundleIdentifier;
+ (NSString *)localeIdenfier2;
+ (NSString *)systemVersion;
+ (NSString *)buildVersion;
+ (NSString *)shortVersionString;


+ (NSString *)stringFromData:(NSData *)data;
+ (id)jsonFromData:(NSData *)data error:(NSError **)error;
+ (UIImage *)imageFromData:(NSData *)data;


+ (NSDictionary *)getParametersFromURL:(NSURL *)url;
+ (NSDictionary *)getParametersFromQuery:(NSString *)query;
+ (NSString *)randomString:(NSInteger)length;


+ (NSString *)md5:(NSString *) input;
+ (NSString *)pluralize:(NSString *)singular;
@end
