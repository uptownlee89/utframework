//
//  UTTableProvider.m
//  UTFramework
//
//  Created by Juyoung Lee on 13. 2. 15..
//  Copyright (c) 2013년 Juyoung.me. All rights reserved.
//

#import "UTTableProvider.h"
#import "UTDataProvider+Protected.h"
@interface UTTableProvider ()

@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) NSMutableArray *sectionTitles;
@end


@implementation UTTableProvider

- (id)initWithArray:(NSArray *)array{
    self = [super init];
    if(self){
        [self.sections addObject:[NSArray arrayWithArray:array]];
    }
    return self;
}
- (id)initWithContentsOfFile:(NSString *)file{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"plist"];
    NSArray *data = [NSMutableArray arrayWithContentsOfFile:path];
    
    return[self initWithArray:data];
}
- (id)initWithContentsOfFile:(NSString *)file inBundle:(NSBundle *)bundle{
    
    NSString *path = [bundle pathForResource:file ofType:@"plist"];
    NSArray *data = [NSMutableArray arrayWithContentsOfFile:path];
    
    return[self initWithArray:data];
}


- (NSMutableArray *)sections{
    if(_sections)
        return _sections;
    _sections = [NSMutableArray array];
    return _sections;
}

- (NSUInteger)numberOfObjectForSection:(NSUInteger)section{
    return [self.sections[section] count];
}

- (NSUInteger)numberOfSection{
    return [self.sections count];
}

- (id)objectAtIndex:(NSUInteger)index forSection:(NSUInteger)section{
    return self.sections[section][index];
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath{
    return [self objectAtIndex:indexPath.row forSection:indexPath.section];
}

- (NSString *)titleForSection:(NSUInteger)section{
    return nil;
}

- (NSArray *)sectionIndexTitles{
    return nil;
}

- (NSInteger)sectionForSectionIndexTitle:(NSString *)title atIndex:(NSUInteger)index{
    return 0;
}

- (void)_setData:(id)data{
    NSUInteger index = 0;
    for( NSArray *section in data){
        self.sections[index++] = [NSArray arrayWithArray:section];
    }
}

- (void)_getMoreData:(id)data{
    NSUInteger index = 0;
    for( NSArray *section in data){
        [self.sections[index++] getMoreWithData:section];
    }
}

@end
